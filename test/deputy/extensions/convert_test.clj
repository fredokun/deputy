(ns deputy.extensions.convert-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [deputy.syntax :as s :refer [Π λ]]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :as t :refer [type-check]]
   [deputy.extensions.convert :refer :all]))

(deftest test-parsing
  (testing "parsing"
    (is (= (convert! nil from :unit to :unit)
           {:node :deputy.extensions.convert/convert!, :from :unit, :to :unit, :term nil}))

    (is (= (Π [T :type] (Π [S :type] (convert! nil from T to S)))
           '{:node :pi, :domain :type,
             :codomain {:node :bind, :name T,
                        :body {:node :pi, :domain :type,
                               :codomain {:node :bind, :name S,
                                          :body {:node :deputy.extensions.convert/convert!,
                                                 :from {:node :bound-var, :name T, :level 1},
                                                 :to {:node :bound-var, :name S, :level 0}, :term nil}}}}})))

  (testing "unparsing"
    (is (= (a/unparse (convert! nil :from :unit :to :unit))
           '(convert! nil :from :unit :to :unit)))))


(deftest test-evaluation
  (is (= (n/evaluate (convert! nil from :unit to :unit))
         nil))
  (is (= (n/evaluate (convert! ((s/fun [x] x) nil) from :unit to :unit))
         nil))
  (is (= (n/evaluate (convert! ((s/fun [x] x) nil) from :unit to :unit))
         nil)))

(deftest test-typing
  (testing "type-checking"
    (is (true? (t/type-check :type (convert! nil from :unit to :type))))
    (is (true? (t/type-check (Π [T :type] T) (λ [T] (convert! nil from :unit to T))))))

  (testing "type-synthesis"
    (is (= (t/type-synth (convert! ((s/the (s/=> :unit :unit) (s/fun [x] x)) nil) from :unit to :type))
           :type))))



           


