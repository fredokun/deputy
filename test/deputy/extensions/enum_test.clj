(ns deputy.extensions.enum-test
  (:require
   [clojure.test :refer :all]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.extensions.labels :as l :refer [tag ls cons-l]]
   [deputy.extensions.enum :as e :refer [switch struct enum index succ]]))

(defn fv-fixture [f]
  (binding [*allow-free-variables* true]
    (f)))

(use-fixtures :once fv-fixture)

(deftest test-parsing
  (testing "simple parses"
    (is (= (enum (ls (tag :a) (tag :b) (tag :c)))
           {:node :enum,
            :labels {:node :tags,
                     :label {:node :tag, :label :a},
                     :labels {:node :tags,
                              :label {:node :tag, :label :b},
                              :labels {:node :tags,
                                       :label {:node :tag, :label :c},
                                       :labels nil}}}}))

    (is (= (index 3)
           '{:node :succ, :idx {:node :succ, :idx {:node :succ, :idx zero}}}))

    (is (= (s/parse zero) 'zero))

    (is (= (succ zero) '{:node :succ, :idx zero})))

  (testing "simple unparse"
    (is (= (a/unparse (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f))))
           '(enum (ls :a :b :c :d :e :f))))

    (is (= (a/unparse (index 3))
           '(succ (succ (succ zero)))))))

(deftest test-tools
  (testing "alpha equivalence"
    (is (n/alpha-equiv (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e)))
                       (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e)))))

    (is (not (n/alpha-equiv (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e)))
                            (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :f))))))

    (is (not (n/alpha-equiv (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e)))
                            (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f))))))))

(deftest eval
  (testing "(struct (cons-l x xs) as v return (P v))
            ↝ (Σ [_ (P zero)] (struct xs as v return (P (succ v))))"
    (is (n/beta-equiv 
         (n/evaluate (struct (cons-l x xs) as v return (P v)))
         (Σ [_ (P zero)] (struct xs as v return (P (succ v))))))))
         
(deftest type-system
  (testing "[ls :labels] ⊢ :type ∋ enum ls"
    (is (true? (type-check {'ls :labels}
                           :type
                           (enum ls)))))

  (testing "[l :label][ls :labels] ⊢ enum (cons-l l ls) ∋ 0"
    (is (true? (type-check {'l :label
                            'ls :labels}
                           (enum (cons-l l ls))
                           'zero))))
  (testing "[l :label][ls :labels][k (enum ls) ⊢ enum (cons-l l ls) ∋ (suc k)"
    (is (true? (type-check {'l :label
                            'ls :labels
                            'k (enum ls)}
                           (enum (cons-l l ls))
                           (succ k)))))

  (testing "[xs :labels][P (=> (enum xs) :type)] ⊢ (struct xs as x return (P x)) ∈ :type"
    (is (n/beta-equiv
         :type
         (type-synth {'xs :labels
                      'P  (=> (enum xs) :type)}
                     (struct xs as x return (P x))))))

  (testing "[x :label][xs :labels]
            [P (=> (enum (cons-l x xs)) :type)]
            [v (P zero)]
            [vs (struct xs as x return (P (succ x)))] 
            ⊢ (struct xs as x return (P x)) ∋ [v vs]"
    (is (true? (type-check {'l :label
                            'ls :labels
                            'P  (=> (enum (cons-l l ls)) :type)
                            'v (app P zero)
                            'vs (struct ls as v return (P (succ v)))}
                           (n/evaluate (struct (cons-l l ls) as v return (P v)))
                           (pair v vs)))))

  (testing "[xs :labels][n (enum xs)][P (Π (enum xs) :type)][res (struct xs as x return (P x))] ⊢ (switch n as x return (P x) with res) ∈ (P n)"
    (is (n/beta-equiv
         (app P n)
         (type-synth {'xs :labels
                      'n (enum xs)
                      'P (=> (enum xs) :type)
                      'cases (struct xs as x return (P x))}
                     (switch n as x return (P x) with cases))))))

(deftest test-type-check
  (testing ":labels ∋ (:a :b :c)"
    (is (true? (type-check :labels (cons-l (tag :a) (cons-l (tag :b) (cons-l (tag :c) nil))))))
    (is (true? (type-check :labels (ls (tag :a) (tag :b) (tag :c))))))

  (testing "Π :labels :type ∋ enum"
    (is (true? (type-check (=> :labels :type)
                           (λ [x] (enum x))))))

  (testing "enum (:a :b :c :d :e :f) ∋ 0"
    (is (true? (type-check (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
                           (index 0)))))
  (testing "enum (:a :b :c :d :e :f) ∋ 3"
    (is (true? (type-check (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
                           (index 3)))))
  (testing "enum (:a :b :c :d :e :f) ∋ 5"
    (is (true? (type-check (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
                           (index 5)))))

  (testing "enum (cons-l :g (:a :b :c :d :e :f)) ∋ 6"
    (is (true? (type-check (enum (cons-l (tag :g) (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f))))
                           (index 6)))))

  (testing "Π [x [enum (:left :right)]] :type ∋ (λ [y] (switch y as x return :type with [(enum :left) [(enum :right) nil]]))"
    (is (true? (type-check (Π [x (enum (ls (tag :left) (tag :right)))] :type)
                            (λ [y] (switch y as _ return :type with
                                           (pair (enum (ls (tag :left)))
                                                 (enum (ls (tag :right)))
                                                 nil))))))))

(deftest negtest-type-check
  (testing "Index out of range"
    (is (ko-expr? (type-check (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
                              (index 6))))
    (is (ko-expr? (type-check (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
                              (index 7))))))

;;; XXX : not a test

(def enum-ab (enum (ls (tag :a) (tag :b))))

(deftest issue-7
  (type-check
   (Π [P (=> (clj enum-ab) :type)]
      (Π [p1 (P (index 0))]
         (Π [p2 (P (index 1))]
            (P (index 0)))))
   (fun [P p1 p2]
        ((switch (the (clj enum-ab) (index 0))
                 as x
                 return (Π [xs :unit] (P x))
                 with [(fun [xs] p1)
                       (fun [xs] p2)
                       nil]) nil))))
