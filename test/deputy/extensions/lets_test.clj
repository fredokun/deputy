(ns deputy.extensions.lets-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [deputy.syntax :as s]
   [deputy.core :refer [defterm]]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.stdlib.boolean :as boolean]
   [deputy.typing :as t :refer [type-check]]
   [deputy.extensions.lets :refer :all]))

(deftest test-syntax
  (testing "simple parses"
    (is (= (s/parse (lets [[x :type] nil
                           [y :type] x]
                          [x y]))
           '{:node :let, 
             :params [[[x :type] nil] 
                      [[y :type] {:node :bound-var, :name x, :level 0}]], 
             :body {:node :pair, 
                    :first {:node :bound-var, :name x, :level 1}, 
                    :second {:node :bound-var, :name y, :level 0}}}))

    (is (= (s/parse (lets a b c d))
           '[:ko> "Wrong arity for `let` form, expecting 3." 
             {:term (:deputy.extensions.lets/let a b c d), :arity 5}]))

    (is (= (s/parse (lets a b))
           '[:ko> "`let` form variable definitions must be a vector." {:vars a}]))

    (is (= (s/parse (lets [] nil))
           '{:node :let, :params [], :body nil}))

    (is (= (s/parse (lets [1] nil))
           '[:ko> "Wrong variable definition in `let` form." {:var 1}]))

    (is (= (s/parse (lets [[x :type]] nil))
           '[:ko> "Missing let-variable expression." {:var [x :type]}])))


  (testing "unparses"
    (is (= (a/unparse '{:node :let, 
                        :params [[[x :type] nil] 
                                 [[y :type] {:node :bound-var, :name x, :level 1}]], 
                        :body {:node :pair, :first {:node :bound-var, :name x, :level 1}, 
                               :second {:node :bound-var, :name y, :level 0}}})
           '(let [[x :type] nil 
                  [y :type] x] 
              [x y])))

    (is (= (a/unparse (s/parse (lets [[x :type] nil
                                      [y :type] x]
                                     [x y])))
           '(let [[x :type] nil 
                  [y :type] x] 
              [x y])))))



(deftest test-methods
  (testing "freevars"
    (is (= (a/freevars (binding [s/*allow-free-variables* true]
                         (s/parse (lets [[x :type] y
                                         [y z] x
                                         [u :type] [x z]]
                                        [a b x y z u]))))
           '#{a y z b})))

  (testing "subst"
    (is (= (a/unparse 
            (a/subst nil (:body (:content 
                                 (s/fun [x] 
                                        [x (lets [[y :type] [x :type]
                                                  [z :type] [x y]]
                                                 [x z])])))))
           '[nil (let [[y :type] [nil :type] [z :type] [nil y]] [nil z])])))

  (testing "bind"
    (is (= (a/unparse 
            :index
            (a/bind 'x (binding [s/*allow-free-variables* true]
                         (s/parse [x (lets [[y :type] [x :type]
                                            [z :type] [x y]]
                                           [x z])]))))
           '[#{0} (let [[y :type] [#{0} :type] 
                        [z :type] [#{1} #{0}]] 
                    [#{2} #{0}])])))


  (testing "evaluate"
    (is (= (n/evaluate (lets [[f (s/=> :type :type)] (s/fun [x] x)
                              [y :type] nil]
                             (f y)))
           nil))))


#_(defterm [bug-subst :unit]
  (lets [[x boolean/Bool] boolean/ttrue
         [y (boolean/ifte x (s/fun [_] :type) :unit boolean/Bool)] nil]
        y))

(deftest test-typing
  (testing "type-synth"
    (is (= (t/type-synth 
            '{z :type} 
            (binding [s/*allow-free-variables* true]
              (lets [[f (s/=> :type :type)] (s/fun [x] x)
                     [y :type] z]
                    (f y))))
           :type))

    (is (= (t/type-synth 
            {}
            (lets [[x boolean/Bool] boolean/ttrue
                   [y (boolean/ifte x (s/fun [_] :type) :unit boolean/Bool)] nil]
                  y))
           :unit))))


