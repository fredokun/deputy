(ns deputy.extensions.labels-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [deputy.syntax :as s]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :as t :refer [type-check]]
   [deputy.extensions.labels :as l :refer [tag ls cons-l vls]]))

(deftest test-parsing
  (testing "simple parses"
    (is (= (tag :a)
           '{:node :tag, :label :a}))
    (is (= (l/label :a)
           '{:node :tag, :label :a}))
    (is (= (l/l :a)
           '{:node :tag, :label :a}))

    (is (= (ls (tag :a) (tag :b) (tag :c))
           {:node :tags,
            :label {:node :tag, :label :a},
            :labels {:node :tags,
                     :label {:node :tag, :label :b},
                     :labels {:node :tags,
                              :label {:node :tag, :label :c},
                              :labels nil}}})))

  (testing "simple unparse"
    (is (= (a/unparse (tag :a)) :a))
    
    (is (= (a/unparse (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
           '(ls :a :b :c :d :e :f))))
  )

(deftest test-tools
  (testing "alpha equivalence"
    (is (n/alpha-equiv (tag :a) (tag :a)))
    (is (not (n/alpha-equiv (tag :a) (tag :b))))

    (is (n/alpha-equiv (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e))
                       (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e))))

    (is (not (n/alpha-equiv (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e))
                            (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :f)))))

    (is (not (n/alpha-equiv (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e))
                            (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))))))

(deftest type-system
  (testing ":type ∋ :label"
    (is (true? (type-check :type :label))))
  (testing ":label ∋ :a"
    (is (true? (type-check :label (tag :a))))
    (is (true? (type-check :label (tag :foo))))
    (is (true? (type-check :label (tag :bar)))))

  (testing ":type ∋ :labels"
    (is (true? (type-check :type :labels))))
  (testing ":labels ∋ nil"
    (is (true? (type-check :labels nil))))
  (testing "[t :label][ls :labels] ⊢ :labels ∋ (cons-l t ls)"
    (is (true? (type-check {'t :label
                            'u :labels}
                           :labels
                           (cons-l (clj (a/mk-free 't)) 
                                   (clj (a/mk-free 'u)))))))

  (testing ":labels ∋ (:foo :bar :baz)"
    (is (true? (type-check :labels (vls :foo :bar :baz))))))

(deftest test-type-check
  (testing ":labels ∋ ((tag :a) (tag :b) (tag :c))"
    (is (true? (type-check :labels (cons-l (tag :a) (cons-l (tag :b) (cons-l (tag :c) nil))))))
    (is (true? (type-check :labels (ls (tag :a) (tag :b) (tag :c)))))))
