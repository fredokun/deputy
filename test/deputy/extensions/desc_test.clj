(ns deputy.extensions.desc-test
  (:require
   [clojure.test :refer :all]
   [deputy.syntax :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :as t :refer [type-check]]
   [deputy.extensions.labels :as l :refer [ls tag]]
   [deputy.extensions.enum :as e :refer [succ struct switch]]
   [deputy.extensions.desc :refer :all]
   ))

(defn fv-fixture [f]
  (binding [*allow-free-variables* true]
    (f)))

(use-fixtures :once fv-fixture)


(deftest type-system
  (testing ":type ∋ descD"
    (is (true? (type-check (mu descD-ctor) descD-ctor)))
    
    (is (true? (type-check :type
                           (mu descD-ctor)))))

  (testing "[l :labels] ⊢ :type ∋ (struct-desc l)"
    (is (true? (type-check {'l  :labels} :type (struct-desc l)))))

  (testing "[l :labels] ⊢ descD ∋ (switch-desc e cs)"
    (is (true? (type-check {'l  :labels
                            'e  (e/enum l)
                            'cs (struct-desc l)}
                           descD
                           (switch-desc e with cs)))))

  (testing "[T :type] ⊢ descD ∋ :k T"
    (is (true? (type-check {'T :type}
                           descD
                           (desc-k T)))))
  
  (testing "[S :type][T (Π S descD)] ⊢ descD ∋ (:sig S T)"
    (is (true? (type-check {'S :type
                            'T (=> S descD)}
                           descD
                           (desc-sig S T)))))
  
  (testing "[D1 descD][D2 descD)] ⊢ descD ∋ (:prod D1 D2)"
    (is (true? (type-check {'D1 descD
                            'D2 descD}
                           descD
                           (desc-prod D1 D2)))))
  
  (testing "[S :type][T (Π S descD)] ⊢ descD ∋ (:pi S T)"
    (is (true? (type-check {'S :type
                            'T (=> S descD)}
                           descD
                           (desc-pi S T)))))
  
  (testing "[xs :labels][cs (struct xs as _ return descD)] ⊢ descD ∋ (:struct xs cs)"
    (is (true? (type-check {'xs :labels
                            'cs (struct-desc xs)}
                           descD
                           (desc-sig-struct xs cs)))))

  (testing "[xs :labels][cs (struct xs as _ return descD)] ⊢ descD ∋ (:struct xs cs)"
    (is (true? (type-check {'xs :labels
                            'cs (struct-desc xs)}
                           descD
                           (desc-pi-struct xs cs)))))

  (testing "[D descD][X :type] ⊢ :type ∋ (:interp D X)"
    (is (true? (type-check {'D descD
                            'X :type}
                           :type
                           (interp D X)))))

  (testing "[D descD] ⊢ :type ∋ (:μ D)"
    (is (true? (type-check {'D descD}
                           :type
                           (μ D)))))
  
  (testing "[D descD][xs (:interp D (:μ D))] ⊢ (:μ D) ∋ (:ctor xs)"
    (is (true? (type-check {'D descD
                            'xs (interp D (μ D))}
                           (μ D)
                           (ctor xs)))))
  (testing "[D descD][v (:μ D)] ⊢ (:interp D (:μ D)) ∋ (:attr v)"
    (is (true? (type-check {'D descD
                            'v (μ D)}
                           (interp D (μ D))
                           (inner v))))))

(deftest test-type-check
  (testing "descD ∋ (:k :unit)"
    (is (true? (type-check descD (desc-k :unit)))))

  (testing "descD ∋ (:sig (:k :unit) (λ [_] :x))"
    (is (true? (type-check descD (desc-sig :unit (λ [_] desc-x))))))

  (testing "descD ∋ (:prod (:k :unit) :x)"
    (is (true? (type-check descD (desc-prod (desc-k :unit) desc-x)))))

  (testing "descD ∋ (:pi (:k :unit) (λ [_] :x))"
    (is (true? (type-check descD (desc-pi :unit (λ [_] desc-x))))))

  (testing "descD ∋ (:struct (:cell :end) [:x [(:k :unit) nil]])"
    (is (true? (type-check descD (desc-sig-struct (ls (tag :cell)
                                                      (tag :end))
                                                  (pair desc-x
                                                        (desc-k :unit)
                                                        nil))))))

  (testing "(:interp (:sig :unit (λ [x] (:k :unit))) :unit) ∋ :attr (the (:mu (:sig :unit (λ [x] (:k :unit)))) (:ctor [nil nil]))"
    (is (true? (type-check (n/evaluate (interp (desc-sig :unit (λ [_] (desc-k :unit))) :unit))
                           (inner (the (mu (desc-sig :unit (λ [_] (desc-k :unit)))) (ctor [nil nil])))))))

  (testing "(sig :unit :unit) ∋  (π2 (((the (pi [desc descD] (pi (:mu desc) (:interp desc (:mu desc))))
                                    (λ [desc ctor] (:attr ctor)))) (:struct (:a :b :c :d)
                                                                                   [(:sig :unit (λ [x] (:k :unit)))
                                                                                    [(:k :unit)
                                                                                     [(:k :unit)
                                                                                      [(:k :unit) nil]]]]))
                              (:ctor [:zero [nil nil]])))"
    (is (true? (type-check (sig :unit :unit)
                           (π2 (((the (Π [desc descD] (Π [_ (mu desc)] (interp desc (mu desc))))
                                      (λ [desc] (λ [ctor] (inner ctor))))
                                 (desc-sig-struct (ls (tag :a) (tag :b) (tag :c) (tag :d))
                                                  (pair (desc-sig :unit (λ [_] (desc-k :unit)))
                                                        (desc-k :unit)
                                                        (desc-k :unit)
                                                        (desc-k :unit)
                                                        nil)))
                                (ctor (pair zero nil nil)))))))))

(deftest test-bug-switch-desc
  ;; Bug: this forces me to do a `convert!` over cs
  (is (true? (type-check (Π [ls :labels]
                            (Π [cs (struct-desc ls)]
                               (Π [e (e/enum ls)] descD)))
                         (fun [ls cs e] (switch-desc e with cs))))))

 ;;equivalence ? XXX: what is this testing?
 ;;  (t/type-check (n (parse (sig [e (enum (:a :b :c :d))] (case e as x return :type with [:label [:unit [:labels [:enum nil]]]]))))
 ;;                (parse (:attr (the (:mu (:struct (:a :b :c :d) [(:k :label) [(:k :unit) [(:k :labels) [(:k :enum) nil]]]])) (:ctor [(index 1) nil])))))
 ;;  => true

 ;; XXX: what is this testing???
 ;; (t/type-check :type (n (parse ((the (pi descD :type) (fn [d] (:interp d (:mu d)))) (:k :unit)))))
 ;; => true)
 ;; (t/type-check :type (n (parse ((the (pi descD :type) (fn [d] (:interp d (:mu d)))) (:sig :unit (fn [x] :x))))))
 ;; => true)
 ;; (t/type-check :type (n (parse ((the (pi descD :type) (fn [d] (:interp d (:mu d)))) (:prod (:k :unit) :x)))))
 ;; => true)
 ;; (t/type-check :type (n (parse ((the (pi descD :type) (fn [d] (:interp d (:mu d)))) (:pi :unit (fn [x] :x))))))
 ;; => true)
 ;; (t/type-check :type (n (parse ((the (pi descD :type) (fn [d] (:interp d (:mu d)))) (:struct (:cell :end) [:x [(:k :unit) nil]])))))
;; => true
