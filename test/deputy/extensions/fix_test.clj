(ns deputy.extensions.fix-test
  (:require
   [clojure.test :refer :all]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :as t]
   [deputy.extensions.fix :as e :refer :all]))

(defn fv-fixture [f]
  (binding [*allow-free-variables* true]
    (f)))

(use-fixtures :once fv-fixture)


(deftest test-parsing
  (testing "simple parses"
    (is (= (rec a b c)
           '{:node :rec,
             :domain {:node :free-var, :name a},
             :codomain {:node :free-var, :name b},
             :return {:node :free-var, :name c}}))
    (is (=  (ret a)
            '{:node :ret,
              :value {:node :free-var, :name a}}))
    (is (= (rec-call a b)
           '{:node :rec-call,
             :args {:node :free-var, :name a}
             :kont {:node :free-var, :name b}}))
    (is (= (red a b c d e)
           '{:node :red,
             :domain {:node :free-var, :name a},
             :codomain {:node :free-var, :name b},
             :return {:node :free-var, :name c},
             :code {:node :free-var, :name d},
             :rec {:node :free-var, :name e}}))
    (is (= (fix a b c d)
           '{:node :fix,
             :domain {:node :free-var, :name a},
             :codomain {:node :free-var, :name b},
             :init {:node :free-var, :name c},
             :rec {:node :free-var, :name d}}))))


(deftest type-system
  (testing "[A :type][B (=> A :type)][X :type] ⊢ :type ∋ rec A B X"
    (is (true? (t/type-check {'A :type
                              'B (=> A :type)
                              'X :type}
                             :type
                             (rec A B X)))))

  (testing "[A :type][B (=> A :type)][X :type][v X] ⊢ rec A B X ∋ ret v"
    (is (true? (t/type-check {'A :type
                              'B  (=> A :type)
                              'X :type
                              'v (s/parse X)}
                             (rec A B X)
                             (ret v)))))

  (testing "[A :type][B (=> A :type)][X :type][a A][k (=> (B a) (rec A B X))] ⊢ rec A B X ∋ rec-call a k"
    (is (true? (t/type-check {'A :type
                              'B (=> A :type)
                              'X :type
                              'a (s/parse A)
                              'k (=> (B a) (rec A B X))}
                             (rec A B X)
                             (rec-call a k)))))

  (testing "[A :type][B (=> A :type)][X :type][a (rec A B X)][r (Π [a A] (B a))] ⊢ (red A B X a r) ∈ X"
    (is (n/beta-equiv
         (t/type-synth {'A :type
                        'B (=> A :type)
                        'X :type
                        'a (rec A B X)
                        'r (Π [a A] (B a))}
                       (red A B X a r))
         (s/parse X))))

  (testing "[A :type][B (=> A :type)][a A][r (Π [a A] (rec A B (B a)))] ⊢ (fix A B a r) ∈ (B a)"
    (is (n/beta-equiv
         (t/type-synth {'A :type
                        'B (=> A :type)
                        'a (s/parse A)
                        'r (Π [a A] (rec A B (B a)))}
                       (fix A B a r))
         (app B a)))

    (is (true? (t/type-check {'A :type
                              'B (=> A :type)
                              'a (s/parse A)
                              'r (Π [a A] (rec A B (B a)))}
                             (app B a)
                             (fix A B a r))))))


(deftest test-eval
  (testing "rec"
    (is (= (n/evaluate (rec (app (λ [x] x) a) (app (λ [x] x) b) (app (λ [x] x) x)))
           (rec a b x))))

  (testing "ret"
    (is (= (n/evaluate (ret (app (λ [x] x) v)))
           (ret v))))

  (testing "rec-call"
    (is (= (n/evaluate (rec-call (app (λ [x] x) a) (app (λ [x] x) k)))
           (rec-call a k))))

  (testing "red"
    (is (= (n/evaluate (red (app (λ [y] y) dom)
                            (app (λ [y] y) cod)
                            (app (λ [y] y) ret)
                            (app (λ [y] y) code)
                            (app (λ [y] y) rec)))
           (red dom cod ret code rec)))
    (is (= (n/evaluate (red (app (λ [y] y) dom)
                            (app (λ [y] y) cod)
                            (app (λ [y] y) ret)
                            (app (λ [y] y) (ret bc))
                            (app (λ [y] y) rec)))
           (s/parse bc)))

    (is (= (n/evaluate (red (app (λ [y] y) dom)
                            (app (λ [y] y) cod)
                            (app (λ [y] y) ret)
                            (app (λ [y] y) (rec-call a k))
                            (app (λ [y] y) r)))
           (red dom cod ret (k (r a)) r)))

    (is (= (n/evaluate (red (app (λ [y] y) dom)
                            (app (λ [y] y) cod)
                            (app (λ [y] y) ret)
                            (app (λ [y] y) (rec-call a (λ [x] (ret x))))
                            (app (λ [y] y) r)))
           (app r a))))

  (testing "fix"
    (is (= (n/evaluate (fix (app (λ [y] y) dom)
                            (app (λ [y] y) cod)
                            (app (λ [y] y) init)
                            (app (λ [y] y) r)))
           (fix dom cod init r)))

    (is (n/beta-equiv
         (fix (app (λ [y] y) dom)
              (app (λ [y] y) cod)
              (app (λ [y] y) nil)
              (app (λ [y] y) r))
         (red dom cod (cod nil) (r nil) (λ [x] (fix dom cod x r)))))

    (is (= (n/evaluate (fix (app (λ [y] y) dom)
                            (app (λ [y] y) cod)
                            (app (λ [y] y) nil)
                            (λ [y] (ret bc))))
           (s/parse bc)))

    (is (n/beta-equiv
         (fix (app (λ [y] y) dom)
              (app (λ [y] y) cod)
              (app (λ [y] y) nil)
              (λ [y] (rec-call a k)))
         (red dom cod (app cod nil)
              (app k (fix dom cod a (λ [y] (rec-call a k))))
              (λ[x] (fix dom cod x (λ [y] (rec-call a k)))))))))


(binding [*allow-free-variables* true]
  (a/unparse (n/evaluate (λ [x y] (fix S T (fun [z] x) (fun [z] x))))))
