(ns deputy.typing-test
  (:require
   [clojure.test :refer :all]
   [deputy.syntax :as s :refer :all]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.norm :refer [beta-equiv]]))

(defn fv-fixture [f]
  (binding [*allow-free-variables* true]
    (f)))

(use-fixtures :once fv-fixture)

(deftest type-system-core
  ;; Type
  (testing ":type ∋ :type."
    (is (true? (type-check :type :type))))

  ;; Unit
  (testing ":type ∋ :unit"
    (is (true? (type-check :type :unit))))
  
  (testing ":unit ∋ nil"
    (is (true? (type-check :unit nil))))

  ;; Pi-type
  (testing "[A :type] [B (=> A :type)] ⊢ :type ∋ (Π [a A] (B a))"
    (is (true? (type-check {'A :type, 'B (=> A :type)} :type (Π [a A] (B a))))))
  
  (testing "[A :type] [B (=> A :type)] [f (Π [a A] (B a))] ⊢ (Π [a A] (B a)) ∋ (λ [a] (f a))"
    (is (true? (type-check {'A :type
                            'B (=> A :type)
                            'f (Π [a A] (B a))}
                           (Π [a A] (B a))
                           (λ [x] (f x))))))

  (testing "[A :type] [B (=> A :type)] [f (Π [a A] (B a))] [a A] ⊢ (f a) ∈ (B a)"
    (is (beta-equiv
         (app B a)
         (type-synth {'A :type
                      'B (=> A :type)
                      'f (Π [a A] (B a))
                      'a (dvar A)}
                     (app f a)))))

  ;; Variable
  (testing "[A :type] [a A] ⊢ a ∈ A"
    (is (beta-equiv
         (dvar A)
         (type-synth {'A :type
                      'a (dvar A)}
                     (dvar a)))))

  ;; Sigma-type
  (testing "[A :type] [B (=> A :type)] ⊢ :type ∋ (Σ [a A] (B a))"
    (is (true? (type-check {'A :type, 'B (=> A :type)}
                           :type (Σ [a A] (B a))))))
  
  (testing "[A :type] [B (=> A :type)] [a A] [b (B a)] ⊢ (Σ [a A] (B a)) ∋ [a b]"
    (is (true? (type-check {'A :type
                            'B (=> A :type)
                            'a (dvar A)
                            'b (app B a)}
                           (Σ [x A] (B x))
                           (pair a b)))))

  (testing "[A :type] [B (=> A :type)] [p (Σ [a A] (B a))] ⊢ (π1 p) ∈ A"
    (is (beta-equiv
         (dvar A)
         (type-synth {'A :type
                      'B (=> A :type)
                      'p (Σ [a A] (B a))}
                     (π1 p)))))
  
  (testing "[A :type] [B (=> A :type)] [p (Σ [a A] (B a))] ⊢ B (π1 p) ∋ (π2 p)"
    (is (beta-equiv
         (app B (π1 p))
         (type-synth {'A :type
                      'B (=> A :type)
                      'p (Σ [a A] (B a))}
                     (π2 p)))))

 ;; Annotation
 (testing "[A :type] [a A] ⊢ (the A a) ∈ A"
   (is (beta-equiv
        (dvar A)
        (type-synth {'A :type
                     'a (dvar A)}
                    (the A a)))))

  ;; Type conversion
  (testing "[A :type] [a A] ⊢ A ∋ a"
    (is (true? (type-check {'A :type
                            'a (dvar A)}
                           (dvar A)
                           (dvar a))))))

(deftest test-type-check-impl
  (testing ":type ∋ (=> :unit :unit)"
    (is (true? (type-check :type (=> :unit :unit)))))
  
  (testing "(=> :type :type) ∋ (λ [x] :unit)"
    (is (true? (type-check (=> :type :type) (λ [x] :unit)))))

  (testing "(=> :unit :type) ∋ (λ [x] :unit)"
    (is (true? (type-check (=> :unit :type) (λ [x] :unit)))))

  (testing "(=> :unit :unit) ∋ (λ [x] x)"
    (is (true? (type-check (=> :unit :unit) (λ [x] x)))))

  (testing "(=> :type :unit) ∋ (λ [x] nil)"
    (is (true? (type-check (=> :type :unit) (λ [x] nil)))))

  (testing "(Π [x :type] (=> x x)) ∋ (λ [x y] y)"
    (is (true? (type-check (Π [x :type] (=> x x)) (λ [x y] y)))))

  (testing ":type ∋ Σ :unit :unit"
    (is (true? (type-check :type (Σ :unit :unit)))))

  (testing "(=> :type :type :type) ∋ (λ [x y] (the :type :unit))"
    (is (true? (type-check (=> :type :type :type)
                           (λ [x y] (the :type :unit))))))
  
  (testing "(Π [a :type] (=> a :type)) ∋ (λ [x y] (the x y))"
    (is (true? (type-check (Π [a :type]  (=> a a))
                           (λ [x y] (the x y))))))
  )

(deftest negtest-type-check-impl
  (testing "Variable in empty context"
    (is (ko-expr? (type-check :type (dvar A)))))
  
  (testing "Missing variable"
    (is (ko-expr? (type-check {'A :type
                               'a (dvar A)}
                              (dvar A)
                              (dvar b)))))

  (testing "Dig into Π"
    (is (ko-expr? (type-check :type (=> nil :unit))))
    (is (ko-expr? (type-check :type (=> :unit nil)))))
  
  (testing "Mismatch of domain does not type-check"
    (is (ko-expr? (type-check {'A :type 'B (=> :unit :type)}
                              :type (Π [a A] (B a))))))

  (testing "Dig into λ"
    (is (ko-expr? (type-check (=> :unit :type) (λ [x] nil))))
    (is (ko-expr? (type-check (=> :unit :type) (λ [x] x)))))

  (testing "Dig into Σ"
    (is (ko-expr? (type-check :type (Σ :unit nil)))))

  (testing "Check type of annotation"
    (is (ko-expr? (type-check :unit (the :unit [nil nil]))))
    (is (ko-expr? (type-check :unit (the nil nil))))

    (is (ko-expr? (type-check {'A :type 'B :type
                               'a (dvar A)}
                                (dvar A)
                                (the B a))))
    
    (is (ko-expr? (type-check {'A :type 'B :type
                               'a (dvar A)}
                              (dvar B)
                              (the A a))))))
