(ns deputy.data-oriented-deputy
  (:require
   [deputy.syntax :refer :all]
   [deputy.core :refer [defterm]]
   [deputy.extensions.labels :as l :refer [tag ls cons-l]]
   [deputy.extensions.enum :as e :refer [switch struct enum index succ]]
   [deputy.extensions.lets :refer :all]
   [deputy.stdlib.boolean :as boolean]
   [deputy.stdlib.nat :as nat]
   [deputy.stdlib.list :as list]
   [deputy.stdlib.map :as map]))

;; Code taken from Chapter 3 ("Manipulate the whole system data with generic functions") of
;; 'Data-oriented programming' by Y. Sharvit

;; XXX: no type for strings as this stage, I've used `:unit` instead(!)

;; ****************************************************************
;; Various sets and indices

(defterm [catalog-isbns :labels]
  (ls (tag :isbn-978-1779501127)))

(defterm [the-catalog-isbn-978-1779501127 (enum catalog-isbns)]
  zero)

;; ****************

(defterm [catalog-authors :labels]
  (ls (tag :author-alan-moore)
      (tag :author-dave-gibbons)))

(defterm [the-catalog-author-alan-moore (enum catalog-authors)]
  zero)

(defterm [the-catalog-author-dave-gibbons (enum catalog-authors)]
  (succ zero))

;; ****************

(defterm [catalog-racks :labels]
  (ls (tag :rack-17)))

(defterm [the-catalog-rack-17 (enum catalog-racks)]
  zero)

;; ****************

(defterm [catalog-bookItem-id :labels]
  (ls (tag :book-item-1)
      (tag :book-item-2)))

(defterm [the-catalog-book-item-1 (enum catalog-bookItem-id)]
  zero)

(defterm [the-catalog-book-item-2 (enum catalog-bookItem-id)]
  (succ zero))

;; ****************

(defterm [userManagement-librarians :labels]
  (ls (tag :franck.gmail.com)))

(defterm [the-librarian-franck (enum userManagement-librarians)]
  zero)

;; ****************

(defterm [userManagement-members :labels]
  (ls (tag :samantha.gmail.com)))

(defterm [the-member-samantha (enum userManagement-members)]
  zero)

;; ****************************************************************
;; bookItem record

(defterm [bookItem-keys :labels]
  (ls
   (tag :id)
   (tag :rackId)
   (tag :isLent)))

(defterm [bookItem-types
          [k (enum bookItem-keys)]
          :type]
  (switch k as _ return :type with
          [(enum catalog-bookItem-id)
            [(enum catalog-racks)
             [boolean/Bool
              nil]]]))

(defterm [bookItem :type]
        (struct bookItem-keys as k return (bookItem-types k)))

;; ****************************************************************
;; book record

(defterm [book-keys :labels]
  (ls
   (tag :isbn)
   (tag :title)
   (tag :publicationYear)
   (tag :authors)
   (tag :bookItems)))

(defterm [book-types
          [k (enum book-keys)]
          :type]
  (switch k as x return :type with
          [(enum catalog-isbns)
           [:unit
            [nat/Nat
             [(list/List (enum catalog-authors))
              [(list/List bookItem)
               nil]]]]]))

(defterm [book :type]
  (struct book-keys as k return (book-types k)))

(defterm [the-book-isbn (enum book-keys)]
  zero)

(defterm [the-book-title (enum book-keys)]
  (succ zero))

(defterm [the-book-authors (enum book-keys)]
  (succ (succ (succ zero))))

;; ****************************************************************
;; example: book

;; Original (p.82):
;;
;; var watchmenBook = {
;;     "isbn": "978-1779501127",
;;     "title": "Watchmen",
;;     "publicationYear": 1987,
;;     "authors": ["alan-moore", "dave-gibbons"],
;;     "bookItems": [
;;         {
;;             "id": "book-item-1",
;;             "rackId": "rack-17",
;;             "isLent": true
;;         },
;;         {
;;             "id": "book-item-2",
;;             "rackId": "rack-17",
;;             "isLent": false
;;         }
;;     ]
;; }


(defterm [eightyseven nat/Nat]
  nat/ze)

(defterm [watchmenBook book]
  [the-catalog-isbn-978-1779501127
   [nil
    [eightyseven
     [(list/lcons (enum catalog-authors) the-catalog-author-dave-gibbons
                  (list/lcons (enum catalog-authors) the-catalog-author-alan-moore
                              (list/lnil (enum catalog-authors))))
      [(list/lcons
        bookItem
        [the-catalog-book-item-1 [the-catalog-rack-17 [boolean/ttrue nil]]]
        (list/lcons
         bookItem
         [the-catalog-book-item-2 [the-catalog-rack-17 [boolean/ffalse nil]]]
         (list/lnil bookItem)))
       nil]]]]])

;; TODO: generic JSON_stringify

;; Original (p.93):
;;
;; function searchBooksByTitleJSON(libraryData, query) {
;;     var results = searchBooksByTitle(_.get(libraryData, "catalog"), query);
;;     var resultsJSON = JSON.stringify(results);
;;     return resultsJSON;
;; }


;; ****************************************************************
;; author record

(defterm [author-keys :labels]
  (ls (tag :name)
      (tag :bookIsbns)))

(defterm [author-types
          [k (enum author-keys)]
          :type]
  (switch k as x return :type with
          [:unit
           [:labels
            nil]]))

(defterm [author :type]
  (struct author-keys as k return (author-types k)))

(defterm [the-author-name (enum author-keys)]
  zero)

;; ****************************************************************
;; example: authors

(defterm [alan-mooreAuthor author]
  [nil
   [(ls (tag :isbn-978-1779501127))
    nil]])

(defterm [dave-gibbonsAuthor author]
  [nil
   [(ls (tag :isbn-978-1779501127))
    nil]])

;; ****************************************************************
;; record catalog

(defterm [catalog-keys :labels]
  (ls
   (tag :booksByIsbn)
   (tag :authorsById)))

(defterm [catalog-types
          [k (enum catalog-keys)]
          :type]
  (switch k as x return :type with
          [(map/smap catalog-isbns book)
           [(map/smap catalog-authors author)
            nil]]))

(defterm [catalog :type]
  (struct catalog-keys as k return (catalog-types k)))

(defterm [the-catalog-booksByIsbn (enum catalog-keys)]
  zero)

(defterm [the-catalog-authorsById (enum catalog-keys)]
  (succ zero))

;; ****************************************************************
;; example: catalog

;; Original (p.87):
;;
;; var catalogData = {
;;     "booksByIsbn": {
;;         "978-1779501127": {
;;             "isbn": "978-1779501127",
;;             "title": "Watchmen",
;;             "publicationYear": 1987,
;;             "authorIds": ["alan-moore", "dave-gibbons"],
;;             "bookItems": [
;;                 {
;;                      "id": "book-item-1",
;;                      "rackId": "rack-17",
;;                      "isLent": true
;;                 },
;;                 {
;;                      "id": "book-item-2",
;;                      "rackId": "rack-17",
;;                      "isLent": false
;;                 }
;;             ]
;;         }
;;     },
;;     "authorsById": {
;;         "alan-moore": {
;;             "name": "Alan Moore",
;;             "bookIsbns": ["978-1779501127"]
;;         },
;;         "dave-gibbons": {
;;             "name": "Dave Gibbons",
;;             "bookIsbns": ["978-1779501127"]
;;         }
;;     }
;; }


(defterm [catalogData catalog]
  [[watchmenBook nil]
   [[alan-mooreAuthor [dave-gibbonsAuthor nil]]
    nil]])

;; ****************************************************************
;; example: projections


;; _.get(catalogData, ["booksByIsbn"]
(defterm [example0 (map/smap catalog-isbns book)]
  (switch (the (enum catalog-keys) zero) as x
          return (catalog-types x) with
          catalogData))

;; _.get(catalogData, ["booksByIsbn", "978-1779501127"]
(defterm [example1 book]
  (switch (the (enum catalog-isbns) zero) as x
          return book with
          (switch (the (enum catalog-keys) zero) as x
                  return (catalog-types x) with
                  catalogData)))

;; Original (p.88):
;; _.get(catalogData, ["booksByIsbn", "978-1779501127", "title"]

(defterm [example2 :unit]
  (switch (the (enum book-keys) (e/succ zero)) as x
          return (book-types x) with
          (switch (the (enum catalog-isbns) zero) as x
                  return book with
                  (switch (the (enum catalog-keys) zero) as x
                          return (catalog-types x) with
                          catalogData))))

;; ****************************************************************
;; record bookInfo

(defterm [bookInfo-keys :labels]
  (ls (tag :title)
      (tag :isbn)
      (tag :authorNames)))

(defterm [bookInfo-types
          [k (enum bookInfo-keys)]
          :type]
  (switch k as x return :type with
          [:unit
           [(enum catalog-isbns)
            [(list/List :unit)
             nil]]]))

(defterm [bookInfo :type]
  (struct bookInfo-keys as x return (bookInfo-types x)))

;; ****************************************************************
;; example: bookInfo

;; Original (p.91):
;;
;; {
;;   "title": "Watchmen",
;;   "isbn": "978-1779501127",
;;   "authorNames": [
;;       "Alan Moore",
;;       "Dave Gibbons",
;;   ]
;; }

(defterm [watchmenInfo bookInfo]
  [nil
   [the-catalog-isbn-978-1779501127
    [(list/lcons
      :unit nil
      (list/lcons
       :unit nil
       (list/lnil :unit)))
     nil]]])

;; ****************************************************************
;; example: operations

(defterm [catalog-get-name
          [catalogData catalog]
          [authorId (enum catalog-authors)]
          :unit]
  (switch the-author-name as x return (author-types x) with
          (switch authorId as x return author with
                  (switch the-catalog-authorsById as x return (catalog-types x) with catalogData))))

;; Original (p.91):
;;
;; function authorNames(catalogData, book) {
;;     var authorIds = _.get(book, "authorIds");
;;     var names = _.map(authorIds, function(authorId) {
;;         return _.get(catalogData, ["authorsById", authorId, "name"]);
;;     });
;;     return names;
;; }

(defterm [authorNames
          [catalogData catalog]
          [bookData book]
          (list/List :unit)]
  (list/lmap (enum catalog-authors) :unit
             (catalog-get-name catalogData)
             (switch the-book-authors as x return (book-types x) with bookData)))

;; Original (p.91):
;;
;; function bookInfo(catalogData, book) {
;;     var bookInfo = {
;;         "title": _.get(book, "title"),
;;         "isbn": _.get(book, "isbn"),
;;         "authorNames": authorNames(catalogData, book)
;;     };
;;     return bookInfo;
;; }

(defterm [mkBookInfo
          [catalogData catalog]
          [bookData book]
          bookInfo]
  (lets [[title :unit]
         (switch the-book-title as x return (book-types x) with bookData)
         [isbn (enum catalog-isbns)]
         (switch the-book-isbn as x return (book-types x) with bookData)]
        (the bookInfo
             [title
              [isbn
               [(authorNames catalogData bookData)
                nil]]])))

(defterm [book-get-title
          [bookData book]
          :unit]
  (switch the-book-title as x return (book-types x) with bookData))

;; XXX: this should be a generic program!
;; generalize catalog-isbns to any [keys :labels]
(defterm [smap-list
          [T :type]
          [ts (map/smap catalog-isbns T)]
          (list/List T)]
  (list/lcons T (switch the-catalog-isbn-978-1779501127 as x return T with ts)
              (list/lnil T)))

(defterm [includes
          [s1 :unit]
          [s2 :unit]
          boolean/Bool]
  ;; replace by equality/inclusion of strings
  boolean/ttrue)

;; Original (p.92):
;;
;; function searchBooksByTitle(catalogData, query) {
;;     var allBooks = _.get(catalogData, "booksByIsbn");
;;     var matchingBooks = _.filter(allBooks, function(book) {
;;         return _.get(book, "title").includes(query);
;;     });
;;     var bookInfos = _.map(matchingBooks, function(book) {
;;         return bookInfo(catalogData, book);
;;     });
;;     return bookInfos;
;; }
;; searchBooksByTitle(catalogData, "Watchmen");


(defterm [searchBookByTitle
          [catalogData catalog]
          [query :unit]
          (list/List bookInfo)]
  (lets [[allBooks (map/smap catalog-isbns book)]
         (switch the-catalog-booksByIsbn as x return (catalog-types x) with catalogData)
         [book-includes
          (=> :unit book boolean/Bool)]
         (fun [title bookData]
              (includes title
                        (switch the-book-title as x return (book-types x) with bookData)))
        [matchingBooks (list/List book)]
        (list/filter book (book-includes query) (smap-list book allBooks))]
        (list/lmap book bookInfo (mkBookInfo catalogData) matchingBooks)))

;; ****************************************************************
;; record bookLending

(defterm [bookLending-keys :labels]
  (ls (tag :bookItemId)
      (tag :bookIsbn)
      (tag :lendingDate)))

(defterm [bookLending-types
          [k (enum bookLending-keys)]
          :type]
  (switch k as x return :type with
          [(enum catalog-bookItem-id)
           [(enum catalog-isbns)
            [:unit
             nil]]]))

(defterm [bookLending :type]
  (struct bookLending-keys as x return (bookLending-types x)))

;; ****************************************************************
;; record librarian

(defterm [librarian-keys :labels]
  (ls (tag :email)
      (tag :encryptedPassword)))

(defterm [librarian-types
          [k (enum librarian-keys)]
          :type]
  (switch k as x return :type with
          [(enum userManagement-librarians)
           [:unit
            nil]]))

(defterm [librarian :type]
  (struct librarian-keys as x return (librarian-types x)))

;; ****************************************************************
;; record members

(defterm [members-keys :labels]
  (ls (tag :email)
      (tag :encryptedPassword)
      (tag :isBlocked)
      (tag :bookLendings)))

(defterm [members-types
          [k (enum members-keys)]
          :type]
  (switch k as x return :type with
          [(enum userManagement-members)
           [:unit
            [boolean/Bool
             [(list/List bookLending)
              nil]]]]))

(defterm [member :type]
  (struct members-keys as x return (members-types x)))

;; ****************************************************************
;; record userManagement

(defterm [userManagement-keys :labels]
  (ls (tag :librarians)
      (tag :members)))

(defterm [userManagement-types
          [k (enum userManagement-keys)]
          :type]
  (switch k as x return :type with
          [(map/smap userManagement-librarians librarian)
           [(map/smap userManagement-members member)
            nil]]))

(defterm [userManagement :type]
  (struct userManagement-keys as x return (userManagement-types x)))

;; ****************************************************************
;; example: userManagement

;; Original (p.97):
;;
;; var userManagementData = {
;;     "librarians": {
;;         "franck@gmail.com" : {
;;             "email": "franck@gmail.com",
;;             "encryptedPassword": "bXlwYXNzd29yZA=="
;;         }
;;     },
;;     "members": {
;;         "samantha@gmail.com": {
;;             "email": "samantha@gmail.com",
;;             "encryptedPassword": "c2VjcmV0",
;;             "isBlocked": false,
;;             "bookLendings": [
;;                  {
;;                     "bookItemId": "book-item-1",
;;                     "bookIsbn": "978-1779501127",
;;                     "lendingDate": "2020-04-23"
;;                  }
;;             ]
;;         }
;;     }
;; }


(defterm [userManagementData userManagement]
  [[[the-librarian-franck
     [nil
      nil]]
    nil]
   [[[the-member-samantha
      [nil
       [boolean/ffalse
        [(list/lcons bookLending
                     [the-catalog-book-item-1
                      [the-catalog-isbn-978-1779501127
                       [nil
                        nil]]]
                     (list/lnil bookLending))
         nil]]]]
     nil]
    nil]])

;; ****************************************************************
;; example: operations

;; XXX: cannot do that for the moment.
;; Need to cast from string to tag and check if it belongs to a list
;; equality and membership is decidable so no reason it couldn't work
;;
;; Original (p.97):
;;
;; function isLibrarian(userManagement, email) {
;;    return _.has(_.get(userManagement, "librariansByEmail"), email);
;; }

(defterm [isLibrarian
          [userManagementData userManagement]
          [email :unit]
          boolean/Bool]
  ?hole)

;; Original (p.98):
;;
;; function isVIPMember(userManagement, email) {
;;     return _.get(userManagement, ["membersByEmail", email, "isVIP"]) == true
;; }

(defterm [isVIP
          [userManagementData userManagement]
          [email :unit]
          boolean/Bool]
  ?hole)

;; Original (p.98):
;;
;; isSuperMember(userManagement, email) {
;;     return _.get(userManagement, ["membersByEmail", email, "isSuper"]) == true;
;; }

(defterm [isSuper
          [userManagementData userManagement]
          [email :unit]
          boolean/Bool]
  ?hole)
