(ns deputy.stdlib.defdata-test
  "Tests of Simpler creation of inductives through description"
  (:require
   [clojure.test :refer [deftest testing is]]
   [deputy.syntax :refer :all]
   [deputy.stdlib.defdata :refer :all]
   [deputy.core :refer [defterm]]))

(def Nat-def '("The inductive type of natural numbers."
               []
               (ze)
               (su [n <rec>])))

(def Tree-def '("The inductive type of binary trees with node labelled with B's
   and leaves labelled with A's"
                [A :type, B :type]
                (leaf [x A])
                (node [l <rec>] [y B] [r <rec>])))


(deftest test-parse
  (testing "Parsing defdata"

    (is (= (parse-defdata 'Tree Tree-def)
           '{:name Tree
             :dostring "The inductive type of binary trees with node labelled with B's\n   and leaves labelled with A's"
             :params [[A :type] [B :type]]
             :constructors [{:name leaf, :params [[x A]]} 
                            {:name node, :params [[l <rec>] [y B] [r <rec>]]}]}))


    (is (= (parse-defdata 'Tree
                          '([A :type, B :type]
                            (leaf [x A])
                            (node [l <rec>] [y B] [r <rec>])))
           '{:name Tree, :dostring ""
             :params [[A :type] [B :type]]
             :constructors [{:name leaf, :params [[x A]]}
                            {:name node, :params [[l <rec>] [y B] [r <rec>]]}]}))))



(deftest test-expand
  (testing "Expand constructors"
    
    (is (= (expand-cstr-codes '[<rec> B <rec>])
           '(deputy.extensions.desc/dprod deputy.extensions.desc/dx (deputy.extensions.desc/dprod (deputy.extensions.desc/dk B) (deputy.extensions.desc/dprod deputy.extensions.desc/dx (deputy.extensions.desc/dk :unit))))))

    (is (= (expand-smarts (parse-defdata 'Nat Nat-def))
           '[(deputy.core/defterm [ze Nat] 
               (deputy.extensions.desc/ctor [zero nil])) 
             (deputy.core/defterm [su [n Nat] Nat] 
               (deputy.extensions.desc/ctor [(deputy.extensions.enum/succ zero) [n nil]]))])))

  (testing "Expand case"

    (is (= (expand-case-constr '[[A :type] [B :type]] [0 {:name 'leaf, :params '[[a A]]}])
           '(λ [a tt] (deputy.extensions.convert/convert! 
                       (case-leaf a) 
                       from (P (leaf A B a)) 
                       to (P (deputy.extensions.desc/ctor [zero [a tt]]))))))

    (is (= (expand-case-constr '[[A :type] [B :type]] [1 {:name 'node, :params '[[l <rec>] [b B] [r <rec>]]}])
           '(λ [l b r tt] (deputy.extensions.convert/convert! 
                           (case-node l b r) 
                           from (P (node A B l b r)) 
                           to (P (deputy.extensions.desc/ctor [(deputy.extensions.enum/succ zero) [l [b [r tt]]]]))))))))
