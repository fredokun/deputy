(ns deputy.stdlib.list-test
  (:require
   [deputy.core :refer [defterm]]
   [clojure.test :refer [deftest testing is]]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.stdlib.nat :as nat]
   [deputy.stdlib.list :refer :all]
   [deputy.extensions.desc :as d]))

(comment
  ;; this is the underlying encoding
  ;; auto-generated through defdata

  (defterm [constructors :labels]
    (vls :lnil :lcons))

  (defterm [codes
            [A :type]
            (d/struct-desc constructors)]
    [(d/dk :unit)
     (d/dprod (d/dk A) (d/dprod d/dx (d/dk :unit)))
     nil])

  (defterm [llistD
            [A :type]
            d/descD]
    (d/dsig-struct constructors (codes A)))

  (defterm [llist (=> :type :type)]
    (λ [A] (d/mu (llistD A))))

  (defterm [lnil (Π [A :type] (llist A))]
    (λ [A] (d/ctor [zero nil])))

  (defterm [lcons (Π [A :type] (=> A (=> (llist A) (llist A))))]
    (fun [A a as] (d/ctor [(succ zero) [a [as nil]]])))

  ;; Superseded by generic case
  (defterm [lcase (Π [A :type]
                     (Π [as (llist A)]
                        (Π [P (=> (llist A) :type)]
                           (=> (P (lnil A))
                               (=> (Π [a A]
                                      (Π [as (llist A)]
                                         (P (lcons A a as))))
                                   (P as))))))]
    (fun [A as P cn cc]
         (c/convert!
          ((switch (π1 (d/inner as))
                   as x
                   return (Π [xs (d/interp (d/switch-desc x with
                                                          [(d/dk :unit)
                                                           (d/dprod (d/dk A) (d/dprod d/dx (d/dk :unit)))
                                                           nil])
                                           (llist A))]
                             (P (d/ctor [x xs])))
                   with
                   [(fun [tt] (c/convert! cn
                                          from (P (d/ctor [zero nil]))
                                          to (P (d/ctor [zero tt]))))
                    (fun [nk] (c/convert! (cc (π1 nk) (π1 (π2 nk)))
                                          from (P (lcons A (π1 nk) (π1 (π2 nk))))
                                          to (P (d/ctor [(succ zero) nk]))))
                    nil])
           (π2 (d/inner as)))
          from (P (d/ctor [(π1 (d/inner as)) (π2 (d/inner as))]))
          to (P as))))

  (defterm [lcase
            [A :type]
            [as (llist A)]
            [P (=> (llist A) :type)]
            [case-nil (P (lnil A))]
            [case-cons (Π [a A]
                          (Π [as (llist A)]
                             (P (lcons A a as))))]
            (P as)]
    (g/gcase constructors (codes A) P as
             [(λ [tt] (c/convert! case-nil
                                  from (P (lnil A))
                                  to (P (d/ctor [zero tt]))))
              [(λ [a as tt] (c/convert! (case-cons a as)
                                        from (P (lcons A a as))
                                        to (P (d/ctor [(succ zero) [a [as tt]]]))))
               nil]]))
)

(defterm [t0 (List nat/Nat)]
  (lnil nat/Nat))

(defterm [t1 (List nat/Nat)]
  (lcons nat/Nat nat/one t0))

(defterm [t2 (List nat/Nat)]
  (lcons nat/Nat nat/two t1))

(defterm [t3 (List nat/Nat)]
  (lcons nat/Nat nat/three t2))

(deftest eval
  (testing "case"
    (is (n/beta-equiv
         (app List-case nat/Nat t0 (λ [_] nat/Nat) nat/three (λ [x y] nat/four))
         nat/three))
    (is (n/beta-equiv
         (app List-case nat/Nat t1  (λ [_] nat/Nat) nat/three (λ [x y] nat/four))
         nat/four))
    (is (n/beta-equiv
         (app List-case nat/Nat t2 (λ [_] nat/Nat) nat/three (λ [x y] nat/four))
         nat/four))
    (is (n/beta-equiv
         (app List-case nat/Nat t3 (λ [_] nat/Nat) nat/three (λ [x y] nat/four))
         nat/four)))

  (testing "lmap"
    (is (n/beta-equiv
         (app lmap nat/Nat nat/Nat nat/su t0)
         t0))
    (is (n/beta-equiv
         (app lmap nat/Nat nat/Nat nat/su t1)
         (app lcons nat/Nat nat/two t0)))
    (is (n/beta-equiv
         (app lmap nat/Nat nat/Nat nat/su t2)
         (app lcons nat/Nat nat/three (lcons nat/Nat nat/two t0)))))

  (testing "append"
    (is (n/beta-equiv
         (app append nat/Nat t0 t0)
         t0))
    (is (n/beta-equiv
         (app append nat/Nat t0 t1)
         t1))
    (is (n/beta-equiv
         (app append nat/Nat t1 t0)
         t1))
    (is (n/beta-equiv
         (app append nat/Nat t3 t0)
         t3))
    (is (n/beta-equiv
         (app append nat/Nat t0 t3)
         t3))
    (is (n/beta-equiv
         (app append nat/Nat t2 t3)
         (app lcons 
          nat/Nat nat/two 
          (lcons 
           nat/Nat nat/one
           (lcons 
            nat/Nat nat/three
            (lcons
             nat/Nat nat/two
             (lcons
              nat/Nat nat/one 
              (lnil nat/Nat))))))))))
