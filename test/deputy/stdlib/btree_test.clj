(ns deputy.stdlib.btree-test
  (:require
   [deputy.core :refer [defterm]]
   [clojure.test :refer [deftest testing is]]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.stdlib.nat :as nat]
   [deputy.stdlib.list :as list]
   [deputy.stdlib.btree :refer :all]
   [deputy.extensions.desc :as d]))

(comment
  ;; this is the underlying encoding
  ;; auto-generated through defdata
  (defterm [constructors :labels]
    (vls :leaf :node))

  (defterm [codes
            [A :type]
            [B :type]
            (d/struct-desc constructors)]
    [(d/dprod (d/dk A) (d/dk :unit))
     (d/dprod d/dx (d/dprod (d/dk B) (d/dprod d/dx (d/dk :unit))))
     nil])

  (defterm [treeD (=> :type (=> :type d/descD))]
    (λ [A B]
       (d/dsig-struct constructors (codes A B))))

  (defterm [tree (=> :type (=> :type :type))]
    (λ [A B] (d/mu (treeD A B))))

  (defterm [leaf (Π [A B :type] (=> A (tree A B)))]
    (λ [A B a] (d/ctor [zero [a nil]])))

  (defterm [node (Π [A B :type] (=> (tree A B) (=> B (=> (tree A B) (tree A B)))))]
    (fun [A B ls b rs] (d/ctor [(succ zero) [ls [b [rs nil]]]])))

  ;; Superseded by generic case
  (defterm [tcase
            [A :type]
            [B :type]
            [t (tree A B)]
            [P (=> (tree A B) :type)]
            [case-leaf (Π [a A] (P (leaf A B a)))]
            [case-node (Π [ls (tree A B)]
                          (Π [b B]
                             (Π [rs (tree A B)]
                                (P (node A B ls b rs)))))]
            (P t)]
    (c/convert!
     ((switch (π1 (d/inner t))
              as x
              return (Π [xs (d/interp (d/switch-desc x with
                                                     [(d/dprod (d/dk A) (d/dk :unit))
                                                      (d/dprod d/dx (d/dprod (d/dk B) (d/dprod d/dx (d/dk :unit))))
                                                      nil])
                                      (tree A B))]
                        (P (d/ctor [x xs])))
              with
              [(fun [args] (c/convert! (case-leaf (π1 args))
                                       from (P (leaf A B (π1 args)))
                                       to (P (d/ctor [zero args]))))
               (fun [args] (c/convert! (case-node (π1 args) (π1 (π2 args)) (π1 (π2 (π2 args))))
                                       from (P (node A B (π1 args) (π1 (π2 args)) (π1 (π2 (π2 args)))))
                                       to (P (d/ctor [(succ zero) args]))))
               nil])
      (π2 (d/inner t)))
     from (P (d/ctor [(π1 (d/inner t)) (π2 (d/inner t))]))
     to (P t)))

  (defterm [tcase 
            [A :type]
            [B :type]
            [t (tree A B)]
            [P (=> (tree A B) :type)]
            [case-leaf (Π [a A] (P (leaf A B a)))]
            [case-node (Π [ls (tree A B)]
                          (Π [b B]
                             (Π [rs (tree A B)]
                                (P (node A B ls b rs)))))]
            (P t)]
    (g/gcase constructors (codes A B) P t
             [(λ [a tt] (c/convert! (case-leaf a)
                                    from (P (leaf A B a))
                                    to (P (d/ctor [zero [a tt]]))))
              [(λ [l b r tt] (c/convert! (case-node l b r)
                                         from (P (node A B l b r))
                                         to (P (d/ctor [(succ zero) [l [b [r tt]]]]))))
               nil]]))
)

(defterm [t0 (Tree nat/Nat nat/Nat)]
  (leaf nat/Nat nat/Nat nat/ze))

(defterm [t1 (Tree nat/Nat nat/Nat)]
  (leaf nat/Nat nat/Nat nat/one))

(defterm [t2 (Tree nat/Nat nat/Nat)]
  (node nat/Nat nat/Nat t0 nat/two t1))

(defterm [t3 (Tree nat/Nat nat/Nat)]
  (leaf nat/Nat nat/Nat nat/three))

(defterm [t4 (Tree nat/Nat nat/Nat)]
  (node nat/Nat nat/Nat t2 nat/four t3))

(deftest eval
  (testing "case"
    (is (n/beta-equiv
         (app Tree-case nat/Nat nat/Nat t0 (λ [_] nat/Nat) (fun [_] nat/three) (λ [x y z] nat/four))
         nat/three))
    (is (n/beta-equiv
         (app Tree-case nat/Nat nat/Nat t0 (λ [_] nat/Nat) (fun [x] x) (λ [x y z] nat/four))
         nat/ze))
    (is (n/beta-equiv
         (app Tree-case nat/Nat nat/Nat t1  (λ [_] nat/Nat) (fun [_] nat/three) (λ [x y z] nat/four))
         nat/three))
    (is (n/beta-equiv
         (app Tree-case nat/Nat nat/Nat t1  (λ [_] nat/Nat) (fun [x] x) (λ [x y z] nat/four))
         nat/one))
    (is (n/beta-equiv
         (app Tree-case nat/Nat nat/Nat t2 (λ [_] nat/Nat) (fun [_] nat/three) (λ [x y z] nat/four))
         nat/four))
    (is (n/beta-equiv
         (app Tree-case nat/Nat nat/Nat t2 (λ [_] nat/Nat) (fun [_] nat/three) (λ [x y z] x))
         t0))
    (is (n/beta-equiv
         (app Tree-case nat/Nat nat/Nat t2 (λ [_] nat/Nat) (fun [_] nat/three) (λ [x y z] z))
         t1))
    (is (n/beta-equiv
         (app Tree-case nat/Nat nat/Nat t2 (λ [_] nat/Nat) (fun [_] nat/three) (λ [x y z] y))
         nat/two))
    (is (n/beta-equiv
         (app Tree-case nat/Nat nat/Nat t3 (λ [_] nat/Nat) (fun [_] nat/three) (λ [x y z] nat/four))
         nat/three))
    (is (n/beta-equiv
         (app Tree-case nat/Nat nat/Nat t4 (λ [_] nat/Nat) (fun [_] nat/three) (λ [x y z] nat/four))
         nat/four)))

  (testing "flatten"
    (is (n/beta-equiv
         (app flatten nat/Nat nat/Nat t0)
         (app list/lcons nat/Nat nat/ze (list/lnil nat/Nat))))
    (is (n/beta-equiv
         (app flatten nat/Nat nat/Nat t1)
         (app list/lcons nat/Nat nat/one (list/lnil nat/Nat))))
    (is (n/beta-equiv
         (app flatten nat/Nat nat/Nat t2)
         (app list/lcons nat/Nat nat/ze (list/lcons nat/Nat nat/one (list/lnil nat/Nat)))))
    (is (n/beta-equiv
         (app flatten nat/Nat nat/Nat t3)
         (app list/lcons nat/Nat nat/three (list/lnil nat/Nat))))
    (is (n/beta-equiv
         (app flatten nat/Nat nat/Nat t4)
         (app list/lcons
              nat/Nat nat/ze
              (list/lcons 
               nat/Nat nat/one
               (list/lcons nat/Nat nat/three
                           (list/lnil nat/Nat))))))))
