(ns deputy.stdlib.nat-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.stdlib.nat :refer :all]
   [deputy.extensions.desc :as d]))

(testing "unparse"
  (is (= (a/unparse Nat) 'Nat))
  ;; this is not good:
  (is (= (a/unparse (n/evaluate Nat))
         :deputy.stdlib.nat/Nat))
  (is (= (a/unparse ze)
      'ze))
  (is (= (a/unparse
       (n/evaluate ze))
      0))
  (is (= (a/unparse su)
         'su))
  (is (= (a/unparse (app su ze))
         '(su ze)))

  (is (= (a/unparse (n/evaluate (app su ze)))
         1))
  (is (= (a/unparse (n/evaluate (app su (su (su (su (su ze)))))))
         5))

  (is (= (a/unparse six)
         'six))
  (is (= (a/unparse (n/evaluate six))
         6))
  (is (= (a/unparse (n/evaluate (app fact three)))
         6)))

(comment
  
  ;; the underlying encoding
  ;; (generated through defdata)
  
  (defterm [constructors :labels]
    (vls :ze :su))
  
  (defterm [codes (d/struct-desc constructors)]
    [(d/dk :unit)
     (d/dprod d/dx (d/dk :unit))
     nil])
  
  (defterm [natD d/descD]
    (d/dsig-struct constructors codes))
  
  (example
   (a/unparse natD) 
   => 'natD)
  
  (defterm [nat :type]
    (d/mu natD))
  
  (defterm [ze nat]
    (d/ctor [zero nil]))
  
  (defterm [su [n nat] nat]
    (d/ctor [(succ zero) [n nil]]))
  
  ;; Superseded by defdata and generic case
  (defterm [ncase [n nat] [P (=> nat :type)]
            [cz (P ze)]
            [cs (Π [k nat]
                   (P (su k)))]
            (P n)]
    (c/convert!
     ((switch (π1 (d/inner n))
              as x
              return (Π [xs (d/interp (d/switch-desc x with
                                                     [(d/dk :unit)
                                                      (d/dprod d/desc-x (d/dk :unit))
                                                      nil])
                                      nat)]
                        (P (d/ctor [x xs])))
              with
              [(fun [tt] (c/convert! cz
                                     from (P (d/ctor [zero nil]))
                                     to (P (d/ctor [zero tt]))))
               (fun [nk] (c/convert! (cs (π1 nk))
                                     from (P (su (π1 nk)))
                                     to (P (d/ctor [(succ zero) nk]))))
               nil])
      (π2 (d/inner n)))
     from (P (d/ctor [(π1 (d/inner n)) (π2 (d/inner n))]))
     to (P n)))

  ;; auto-generated through defdata
  (defterm [ncase [n nat] [P (=> nat :type)]
            [cz (P ze)]
            [cs (Π [k nat]
                   (P (su k)))]
            (P n)]
    (g/gcase constructors codes P n
             [(λ [tt]
                 (c/convert! cz
                             from (P ze)
                             to (P (d/ctor [zero tt]))))
              [(λ [n tt] (c/convert! (cs n)
                                     from (P (su n))
                                     to (P (d/ctor [(succ zero) [n tt]]))))
               nil]]))

  )


(deftest typing
  (testing "Nat-desc : desc"
    (is (n/beta-equiv 
         d/descD
         (type-synth Nat-desc))))

  (testing "Nat : :type"
    (is (n/beta-equiv 
         :type
         (type-synth Nat))))

  (testing "ze : Nat"
    (is (n/beta-equiv 
         Nat
         (type-synth ze))))

  (testing "su : (=> Nat Nat)"
    (is (n/beta-equiv 
         (=> Nat Nat)
         (type-synth su))))

  (testing "two : Nat"
    (is (n/beta-equiv
         Nat
         (type-synth two))))

  (testing "three : Nat"
    (is (n/beta-equiv
         Nat
         (type-synth three))))

  (testing "four : Nat"
    (is (n/beta-equiv
         Nat
         (type-synth four))))

  (testing "five : Nat"
    (is (n/beta-equiv
         Nat
         (type-synth five))))

  (testing "six : Nat"
    (is (n/beta-equiv
         Nat
         (type-synth six))))

  (testing "pred : (=> Nat Nat)"
    (is (n/beta-equiv
         (=> Nat Nat)
         (type-synth pred))))

  (testing "ind : (...)"
    (is (n/beta-equiv
         (Π [n Nat]
            (Π [P (=> Nat :type)]
               (=> (P ze)
                   (=> (Π [n Nat]
                          (=> (P n)
                              (P (su n))))
                       (P n)))))
         (type-synth ind))))

  (testing "add : (=> Nat (=> Nat Nat))"
    (is (n/beta-equiv
         (=> Nat (=> Nat Nat))
         (type-synth add)))))

(deftest eval
  (testing "case"
    (is (n/beta-equiv
         (app Nat-case ze (λ [_] Nat) three (λ [_] four))
         three))
    (is (n/beta-equiv
         (app Nat-case one (λ [_] Nat) three (λ [_] four))
         four))
    (is (n/beta-equiv
         (app Nat-case two (λ [_] Nat) three (λ [_] four))
         four))
    (is (n/beta-equiv
         (app Nat-case three (λ [_] Nat) three (λ [_] four))
         four)))

  (testing "pred"
    (is (n/beta-equiv
         (app pred ze)
         ze))
    (is (n/beta-equiv
         (app pred (su ze))
         ze))
    (is (n/beta-equiv
         (app pred (su (su ze)))
         (app su ze)))
    (is (n/beta-equiv
         (app pred (su (su (su ze))))
         (app su (su ze)))))

  (testing "add"
    (is (n/beta-equiv
         (app add two three)
         five))
    (is (n/beta-equiv
         (app add three two)
         five))
    (is (n/beta-equiv
         (app add two two)
         four))
    (is (n/beta-equiv
         (app add three three)
         six))

    (is (n/beta-equiv
        (n/evaluate (app (fun [k x] (add k x)) three one))
        four))
    
    (is (n/beta-equiv
         (n/evaluate (app (fun [k x] (add x k)) three one))
         four)))

  (testing "mult"
    (is (n/beta-equiv
         (app mult ze ze)
         ze))
    (is (n/beta-equiv
         (app mult ze two)
         ze))
    (is (n/beta-equiv
         (app mult two ze)
         ze))
    (is (n/beta-equiv
         (app mult one two)
         two))
    (is (n/beta-equiv
         (app mult two one)
         two))
    (is (n/beta-equiv
         (app mult two three)
         six))
    (is (n/beta-equiv
         (app mult three two)
         six)))

  (testing "fact"
    (is (n/beta-equiv
         (n/evaluate (app fact two))
         two))

    (is (n/beta-equiv
         (n/evaluate (app fact three))
         six))))





;; (require '[clj-async-profiler.core :as prof])
;; (prof/profile (n/evaluate (app fact two)))
