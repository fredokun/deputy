(ns deputy.stdlib.boolean-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.stdlib.boolean :refer :all]
))

(deftest typing
  (testing "Bool : :type"
    (is (n/beta-equiv 
         :type
         (type-synth Bool))))

  (testing "ttrue : Bool"
    (is (n/beta-equiv
         Bool
         (type-synth ttrue))))

  (testing "ffalse : Bool"
    (is (n/beta-equiv
         Bool
         (type-synth ffalse))))
  
  (testing "ifte : Π [b bool][P (=> bool :type)] (=> (P ttrue) (P ffalse) (P b))"
    (is (n/beta-equiv
         (Π [b (clj Bool)]
            (Π [P (=> (clj Bool) :type)]
               (=> (P (clj ttrue))
                   (=> (P (clj ffalse))
                       (P b)))))
         (type-synth ifte)))))

(deftest eval
  (testing "ifte ttrue P e1 e2 ↝ e1"
    (is (binding [s/*allow-free-variables* true]
          (n/beta-equiv
           (app (clj ifte) (clj ttrue) P e1 e2)
           (dvar e1)))))
  (testing "ifte ffalse P e1 e2 ↝ e2"
    (is (binding [s/*allow-free-variables* true]
          (n/beta-equiv
           (app (clj ifte) (clj ffalse) P e1 e2)
           (dvar e2))))))

