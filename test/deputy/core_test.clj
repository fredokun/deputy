(ns deputy.core-test
  (:require
   [clojure.test :refer :all]
   [deputy.core :refer :all]))

(defterm [toto :type]
  :unit)

(defterm [x toto]
  nil)

(deftest test-defterm
  (testing "Basic defterms: toto"
    (is (= (:node @#'deputy.core-test/toto) :certified))
    (is (= (:term @#'deputy.core-test/toto) :unit))
    (is (= (:vtype @#'deputy.core-test/toto) :type)))

  (testing "Basic defterms: x"
    (is (= (:node @#'deputy.core-test/x) :certified))
    (is (= (:term @#'deputy.core-test/x) nil))
    (is (= (:vtype @#'deputy.core-test/x) :unit))))

