
(ns deputy.ast-test
  (:require
   [clojure.test :refer :all]
   [deputy.syntax :refer :all]
   [deputy.ast :refer :all]))

(defn fv-fixture [f]
  (binding [*allow-free-variables* true]
    (f)))

(use-fixtures :once fv-fixture)

(deftest test-mk
  (testing "Selection of ast constructors"

    (is (=  (mk-annot (mk-const :unit) (mk-const nil))
            '{:node :annotation, :type :unit, :term nil}))

    (is (true? (:deputy (meta (mk-annot (mk-const :unit) (mk-const nil))))))

    (is (true? (:test (meta (mk-annot (mk-const :unit) (mk-const nil) {:test true})))))
            
    (is (true? (:deputy (meta (mk-annot (mk-const :unit) (mk-const nil) {:test true})))))

    (is (= (mk-app (mk-free 'a) (mk-free 'b))
           '{:node :application
             :rator {:node :free-var, :name a}
             :rand {:node :free-var, :name b}}))
    
    (is (= (mk-lam (mk-bind 'x (mk-bound 'x 0)))
           '{:node :lambda, :content {:node :bind, :name x,
                                      :body {:node :bound-var, :name x, :level 0}}}))
    
    (is (= (mk-pair (mk-free 'a) (mk-free 'b))
           '{:node :pair, 
             :first {:node :free-var, :name a}, 
             :second {:node :free-var, :name b}}))
    
    (is (= (mk-proj :right (mk-pair (mk-free 'a) (mk-free 'b)))
           '{:node :proj, :take :right, 
             :pair {:node :pair, 
                    :first {:node :free-var, :name a}, 
                    :second {:node :free-var, :name b}}}))
    
    (is (= (mk-sig (mk-const :type) (mk-bind 'x  (mk-bound 'x 0)))
           '{:node :sig, :first :type,
             :second {:node :bind, :name x,
                      :body {:node :bound-var, :name x, :level 0}}}))
    
    (is (= (mk-pi (mk-const :type) (mk-bind 'x (mk-bound 'x 0))
           '{:node :pi, :domain :type,
             :codomain {:node :bind, :name x,
                        :body {:node :bound-var, :name x, :level 0}}})))))

(deftest test-unparse
  (testing "Unparsing of differents ast"
    (is (= (unparse (lambda [x] [x, x]))
           '(λ [x] [x x])))
    
    (is (= (unparse (pi [x (sig [a :type] a)] (p2 x)))
           '(Π [x (Σ [a :type] a)] (π2 x))))
    
    (is (= (unparse (the :unit nil))
           '(the :unit nil)))
    
    (is (= (unparse :index (λ [x] x))
           '(λ #{0})))
    
    (is (= (unparse :index (λ [x] a))
           '(λ a)))
    
    (is (= (unparse (=> :type :type))
           '(Π [_ :type] :type)))))

(deftest test-freevars
  (testing "Getting free vars from different terms"
    (is (= (freevars (λ [x] x))
           '#{}))

    (is (= (freevars (pi [x (sig [a :type] a)] (p2 x)))
           '#{}))

    (is (= (freevars (p1 [a b]))
           '#{a b}))

    (is (= (freevars (app (lambda [a] a) b))
           '#{b}))

    (is (= (freevars (the :unit nil))
           '#{}))

    (is (= (freevars (lambda [x] a))
           '#{a}))))

(deftest test-normal-form-canonical
  (testing "Normal form canonical on different terms"
    (is (normal-form-canonical? (λ [x] x)))

    (is (normal-form-canonical? (pi [x (sig [a :type] a)] (p2 x))))

    (is (not (normal-form-canonical? (p1 [a b]))))

    (is (not (normal-form-canonical? (app (lambda [a] a) b))))

    (is (normal-form-canonical? (parse b)))

    (is (not (normal-form-canonical? (the :unit nil))))

    (is (normal-form-canonical? nil))

    (is (normal-form-canonical? (lambda [x] a)))))

(deftest test-normal-form-canonical
  (testing "Neutral form canonical on different terms"
    (is (not (normal-form-neutral? (λ [x] x))))

    (is (not (normal-form-neutral? (pi [x (sig [a :type] a)] (p2 x)))))

    (is (not (normal-form-neutral? (p1 [a b]))))

    (is (not (normal-form-neutral? (app (lambda [a] a) b))))

    (is (normal-form-neutral? (parse b)))

    (is (not (normal-form-neutral? (the :unit nil))))

    (is (not (normal-form-neutral? nil)))

    (is (not (normal-form-neutral? (lambda [x] a))))))

(deftest test-bind
  (testing "Bind on different terms"
    
    ))

(deftest test-subst
  (testing "Subst on different terms"
    
    ))

