(ns deputy.norm-test
  (:require
   [clojure.test :refer :all]
   [deputy.syntax :refer :all]
   [deputy.ast :as a]
   [deputy.norm :refer :all]
   [deputy.extensions.boolean :as b]))

(deftest test-evaluate
  (testing "beta-reduction"
    (is (= (a/unparse :index (parse ((fun [x] (fun [y] (x y))) (fun [x] x))))
           '((λ (λ (#{1} #{0}))) (λ #{0}))))

    (is (= (a/unparse :index (evaluate (parse ((fun [x] (fun [y] (x y))) (fun [x] x)))))
           '(λ #{0}))))

  (testing "projections"
    (is (= (a/unparse (evaluate (parse (π1 [:unit nil nil]))))
           :unit))))

(deftest test-beta-equiv
  (testing "lambdas"
    (is (beta-equiv (fun [x] x) (parse ((fun [x] (fun [y] (x y))) (fun [z] z)))))

    (is (beta-equiv (fun [x] (fun [y] (x y))) (fun [y] (fun [x] (y x)))))

    (is (not (beta-equiv (fun [x] (fun [y] (x y))) (fun [y] (fun [x] (x y))))))))

;;; Migrated tests from "old"

(deftest eval-simples
  (testing "constants"
    (is (= (evaluate :type) :type))
    (is (= (evaluate :bool) :bool))
    (is (= (evaluate true) true))
    (is (= (evaluate false) false))
    (is (= (evaluate :unit) :unit))
    (is (= (evaluate nil) nil)))

  (testing "with annotation (same as without)"
    (is (= (evaluate (the :type :type)) :type))
    (is (= (evaluate (the :bool true)) true))))

(deftest eval-vars
  (testing "Free vars"
    (is (= (evaluate (a/mk-free 'x)) '{:node :free-var, :name x})))

  (testing "Bound vars"
    (is (= (evaluate-impl (parse ((λ [x] x) true)))
           true))))

(deftest eval-pis
  (is (let [m (evaluate (Π [x :bool] (λ [y] :bool)))]
        (and (= (:node m) :pi)
             (= (:domain m) :bool)
             (= (evaluate (a/mk-app (:body (:codomain m)) nil)) :bool))))

  (is (let [m (evaluate (Π [x :bool] (λ [y] y)))]
        (and (= (:domain m) :bool)
             (= (evaluate (a/mk-app (:body (:codomain m)) true)) true))))

  (is (let [m1 (evaluate (Π [x :type] (λ [y] (Π [z y] (λ [t] t)))))
            m2 (evaluate (parse ((clj (:body (:codomain m1))) :unit)))]
        (and (= (:domain m1) :type)
             (= (:domain m2) :unit)
             (= (evaluate (parse ((clj (:body (:codomain m2))) nil))) nil)))))

(deftest eval-lambdas
  (is (let [f (evaluate (λ [x] x))]
        (= (evaluate (a/mk-app f true)) true)))

  (is (let [f (evaluate (λ [x] (λ [y] y)))]
        (= (evaluate (a/mk-app (a/mk-app f 42) 17)) 17)))

  (is (let [f (evaluate (λ [x] (λ [y] x)))]
        (= (evaluate (a/mk-app (a/mk-app f 42) 17)) 42)))

  (is (let [f (evaluate (λ [x] (Π [y x] (λ [z] z))))
            m (evaluate (a/mk-app f :bool))]
        (and (= (:domain m) :bool)
             (= (evaluate (a/mk-app (:body (:codomain m)) false)) false))))

  (is (= (let [f (evaluate (the (Π [x :bool] (λ [y] :bool)) (λ [x] x)))]
           (evaluate (a/mk-app f (a/mk-free 'x))))
         (a/mk-free 'x))))

(deftest eval-applications
  (is (= (evaluate (app (λ [x] x) :bool)) :bool))

  (is (= (evaluate (a/mk-app (a/mk-free 'x) (a/mk-free 'y)))
         '{:node :application, :rator {:node :free-var, :name x}, :rand {:node :free-var, :name y}}))

  (is (= (evaluate
          (app (the (Π [x :bool] (λ [y] :bool)) (λ [x] x)) (clj (a/mk-free 'y))))
         '{:node :free-var, :name y}))

  (is (= (evaluate (parse (((λ [x] (λ [y] x)) true) false)))
         true))

  (is (= (evaluate (parse (((λ [x] (λ [y] y)) true) false)))
         false)))


(comment
;;; XXX : no quotation feature (yet)

(deftest quot-simples
  (is (= (quot :type :type) :type))
  (is (= (quot :type :bool) :bool))
  (is (= (quot :bool true) true))
  (is (= (quot :bool false) false))
  (is (= (quot :type :unit) :unit))
  (is (= (quot :unit nil) nil)))

(deftest quot-pi
  (is (let [pi (evaluate '(Π :bool (λ :bool)))]
        (= (quot :type pi)
           '(Π :bool (λ :bool))))))

(deftest quot-lambdas
  (is (let [f (evaluate '(λ #{0}))
            fty (evaluate '(Π :bool (λ :bool)))]
        (= (quot fty f)
           '(λ #{0}))))

  (is (let [fty (evaluate '(Π :bool (λ :bool)))]
        (= (quot fty (fn [x] x))
           '(λ #{0}))))

  (is (let [f (evaluate '(λ (λ #{0})))
            fty (evaluate '(Π :bool (λ (Π :bool (λ :bool)))))]
        (= (quot fty f)
           '(λ (λ #{0})))))

  (is (let [fty (evaluate '(Π :bool (λ (Π :bool (λ :bool)))))]
        (= (quot fty (fn [x] (fn [y] y)))
           '(λ (λ #{0})))))

  (is (let [f (evaluate '(λ (λ #{1})))
            fty (evaluate '(Π :bool (λ (Π :bool (λ :bool)))))]
        (= (quot fty f)
           '(λ (λ #{1})))))

  (is (let [fty (evaluate '(Π :bool (λ (Π :bool (λ :bool)))))]
        (= (quot fty (fn [x] (fn [y] x)))
           '(λ (λ #{1}))))))
)
