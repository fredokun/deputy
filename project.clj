(defproject deputy "0.4.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "MIT License"
            :url "https://opensource.org/licenses/MIT"}
  :main ^:skip-aot deputy.core
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [clj-time "0.15.2"]
                 [fipp "0.6.23"]
                 [mvxcvi/cljstyle "0.15.0"]
                 [com.clojure-goes-fast/clj-async-profiler "1.5.1"]]
  :jvm-opts ["-Djdk.attach.allowAttachSelf"]
  :repl-options {:init-ns deputy.core}
  :plugins [[lein-auto "0.1.3"]]
  :cljfmt {:indents
           {around [[:inner 0]]
            context [[:inner 0]]
            describe [[:inner 0]]
            it [[:inner 0]]}})

