\documentclass{styles/sig-alternate-05-2015}

\usepackage{mathpartir}
\usepackage{stmaryrd}
\usepackage{proof}
\usepackage{fancyvrb}
\usepackage{url}
\urlstyle{sf}

\usepackage{lplfitch}

\newcommand{\typeterm}{\star}
\newcommand{\kindterm}{\square}

\newcommand{\fimplies}{\Longrightarrow}

\newcommand{\norm}[1]{\widetilde{#1}}

\DefineVerbatimEnvironment%
{program}{Verbatim}
{%
commandchars=\\\{\},fontsize=\small,fontfamily=courier,%
codes={\catcode`$=3\catcode`_=8}} %$

\newcommand{\kw}[1]{\textbf{#1}}
\newcommand{\cmt}[1]{\textit{#1}}
\newcommand{\code}[1]{\begin{sffamily}{\small #1}\end{sffamily}}
\newcommand{\mcode}[1]{\textrm{\code{#1}}}
\newcommand{\mkw}[1]{\textrm{\kw{#1}}}
\newcommand{\pindent}{\hspace{8pt}\=}

\begin{document}

% Copyright
%\setcopyright{styles/acmcopyright}

% DOI
%\doi{10.475/123_4}

% ISBN
%\isbn{123-4567-24-567/08/06}

%Conference
\conferenceinfo{ELS '20}{April 27--28, 2020, Zürich, Switzerland}

%\acmPrice{\$15.00}

\title{Dependent Types? Elaborate!}
%\subtitle{(Submitted January 28 2017)}

\numberofauthors{2} %  in this sample file, there are a *total*
% of EIGHT authors. SIX appear on the 'first-page' (for formatting
% reasons) and the remaining two appear in the \additionalauthors section.
%
\author{
% You can go ahead and credit any number of authors here,
% e.g. one 'row of three' or two rows (consisting of one row of three
% and a second row of one, two or three).
%
% The command \alignauthor (no curly braces needed) should
% precede each author name, affiliation/snail-mail address and
% e-mail address. Additionally, tag each line of
% affiliation/address with \affaddr, and tag the
% e-mail address with \email.
%
% 1st. author
\alignauthor
Pierre-Evariste Dagand\\
       \affaddr{Sorbonne University -- LIP6}\\
       \affaddr{4 place Jussieu}\\
       \affaddr{Paris, France}\\
       \email{pierre-evariste.dagand@lip6.fr}
\\
\alignauthor
Frederic Peschanski\\
       \affaddr{Sorbonne University -- LIP6}\\
       \affaddr{4 place Jussieu}\\
       \affaddr{Paris, France}\\
       \email{frederic.peschanski@lip6.fr}
}
% There's nothing stopping you putting the seventh, eighth, etc.
% author on the opening page (as the 'third row') but we ask,
% for aesthetic reasons that you place these 'additional authors'
% in the \additional authors block, viz.
\date{xx April 2020}
% Just remember to make sure that the TOTAL number of authors
% is the number that will appear on the first page PLUS the
% number that will appear in the \additionalauthors section.

\maketitle
\begin{abstract}
Dependent types (DT) combine standard (term-level) computations with
type-level computations. DT have been successfuly applied in the realm of proof assistants but beyond some early experiments, their interest for practical programming remains an open debate. It is most common to consider DT as a (natural?) evolution of statically-typed (functional) programming languages. In this paper, we take a completely opposed point of view of investigating DT within Lisp (precisely Clojure), the epitome of dynamic typing. The objective is not just to implement ``yet another type theory'' in Lisp (which is still a by-product of the effort) but more to reflect upon the possible interesting use of dependent types in this setting. We study the very basic constructions of a dependently-typed lambda-calculus, but we go way beyond that by exploring the elaboration of complex inductive definitions. Most importantly, everything written is the paper is backed-up by a complete implementation using the Clojure programming language, with an affirmed pedagogical bias. It is the belief of (at least one of) the authors that Lisp is the perfect host for DT. One reason is the ``staging'' issue of DT: it is not easy to grasp wether type checking should happen an compile-time or run-time (or boss), which is close (but not identical) to questions raised by macro-level computations. Another reason is that Lisp was the host of early statically-typed functional languages. They since departed (often criticizing their friendly host!), maybe now is the good time for (explicit) types to come back home?
\end{abstract}

 \begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10003752.10003790</concept_id>
<concept_desc>Theory of computation~Logic</concept_desc>
<concept_significance>500</concept_significance>
</concept>
<concept>
<concept_id>10003752.10003790.10011740</concept_id>
<concept_desc>Theory of computation~Type theory</concept_desc>
<concept_significance>500</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Theory of computation~Logic}
\ccsdesc[500]{Theory of computation~Type theory}

%  Use this command to print the description
%
\printccsdesc

% We no longer use \terms command
%\terms{Theory}

\keywords{Dependent Types; Inductive definitions;Functional programming; Clojure}


\section{Introduction}

Dependent types (DT) combine standard (term-level) computations with
type-level computations. DT have been successfuly applied in the realm of proof assistants such as Coq or Agda. Another example is LaTTe, a proof assistant implemented as a Clojure DSL (and developped by one co-author of this paper)\footnote{Shameless plug: \url{https://github.com/latte-central/LaTTe}}. However, beyond some early experiments, their interest for practical programming remains an open debate. The closest example of a dependently typed programming language is Idris~\cite{DBLP:journals/jfp/Brady13}, and while it is usable it is still considered largely an experimental language rather than a practical one.

 It is most common to consider DT as a (natural?) evolution of statically-typed (functional) programming languages. In this paper, we take a completely opposed point of view of investigating DT within Lisp (precisely Clojure), the epitome of dynamic typing. The objective is not just to implement ``yet another type theory'' in Lisp (which is still a by-product of the effort) but more to reflect upon the possible interesting use of dependent types in this setting. We study the very basic constructions of a dependently-typed lambda-calculus, but we go way beyond that by exploring the elaboration of complex inductive definitions. Most importantly, everything written is the paper is backed-up by a complete implementation using the Clojure programming language, with an affirmed pedagogical bias. It is the belief of (at least one of) the authors that Lisp is the perfect host for DT. One reason is the ``staging'' issue of DT: it is not easy to grasp wether type checking should happen an compile-time or run-time (or boss), which is close (but not identical) to questions raised by macro-level computations. Another reason is that Lisp was the host of early statically-typed functional languages. They since departed (often criticizing their friendly host!), maybe now is the good time for (explicit) types to come back home?

\section{A dependently-typed lambda calculus from the ground up}

TODO

\section{A Lisp way to Inductive definitions}

\newcommand{\DCheck}[3]{#1 \vdash #2 \ni #3}
\newcommand{\DSynth}[3]{#1 \vdash #2 \in #3}

\newcommand{\Dsubst}[3]{#1[#2/#3]}

\newcommand{\Dkw}[1]{\mathsf{#1}}
\newcommand{\Das}{\mathop{\Dkw{as}}}
\newcommand{\Dret}{\mathop{\Dkw{return}}}
\newcommand{\Dwith}{\mathop{\Dkw{with}}}

\newcommand{\Dtype}{\Dkw{type}}

\newcommand{\Dunit}{\Dkw{unit}}
\newcommand{\Dnil}{\Dkw{nil}}

\newcommand{\DSigma}[2]{\Sigma\: (#1 \mathop{:} #2)}
\newcommand{\Dtimes}{\times}
\newcommand{\Dpair}[2]{[ #1 \: #2 ]}
\newcommand{\Dfst}[1]{\pi_1\: #1}
\newcommand{\Dsnd}[1]{\pi_2\: #1}

\newcommand{\DPi}[2]{\Pi\: (#1 \mathop{:} #2)}
\newcommand{\Dto}{\to}
\newcommand{\Dlam}[1]{\lambda\: #1.}

\newcommand{\Dlabel}{\Dkw{label}}
\newcommand{\Dquote}[1]{` #1}

\newcommand{\Dlabels}{\Dkw{labels}}

\newcommand{\Denum}{\Dkw{enum}}
\newcommand{\Dzero}{\Dkw{0}}
\newcommand{\Dsuc}{\Dkw{suc}}

\newcommand{\Drecord}{\Dkw{record}}
\newcommand{\Dcase}{\Dkw{case}}

\begin{figure*}
\begin{mathpar}
\inferrule
    { }
    {\DCheck \Gamma \Dtype \Dtype}
\\
\inferrule
    {{}}
    {\DCheck \Gamma \Dtype \Dunit}
\quad
\inferrule
    {{}}
    {\DCheck \Gamma \Dunit \Dnil}
\\
\inferrule
    {\DCheck \Gamma \Dtype A \\
     \DCheck {\Gamma, x : A} \Dtype B}
    {\DCheck \Gamma \Dtype \DPi{x}{A}{B}}
\quad
\inferrule
    {\DCheck {\Gamma, x : A} B b}
    {\DCheck \Gamma {\DPi{x}{A}{B}} {\Dlam x b}}
\quad
\inferrule
    {\DSynth \Gamma f {\DPi{x}{A}{B}}\\
     \DCheck \Gamma A s}
    {\DSynth \Gamma {f\: s} {\Dsubst B s x}}
\\
\inferrule
    {\DCheck \Gamma \Dtype A \\
     \DCheck {\Gamma, x : A} \Dtype B}
    {\DCheck \Gamma \Dtype \DSigma{x}{A}{B}}
\quad
\inferrule
    {\DCheck \Gamma A a\\
     \DCheck \Gamma {\Dsubst B x a} b}
    {\DCheck \Gamma {\DSigma x A B} {\Dpair a b}}
\quad
\inferrule
    {\DSynth \Gamma p {\DSigma x A B}}
    {\DSynth \Gamma {\Dfst p} A}
\quad
\inferrule
    {\DSynth \Gamma p {\DSigma x A B}}
    {\DSynth \Gamma {\Dsnd p} {\Dsubst B x {\Dfst p}}}
\end{mathpar}

\begin{align*}
  (\Dlam x b)\: s &\leadsto \Dsubst b x s\\
  (\Dfst {\Dpair{a}{b}}) &\leadsto a \\
  (\Dsnd {\Dpair{a}{b}}) &\leadsto b
\end{align*}

\caption{Core type theory}
\end{figure*}

\begin{figure*}
\begin{mathpar}
\inferrule
    { }
    {\DCheck \Gamma \Dtype \Dlabel}
\quad
\inferrule
    { }
    {\DCheck \Gamma \Dlabel {\Dquote t}}
\\
\inferrule
    { }
    {\DCheck \Gamma \Dtype \Dlabels}
\quad
\inferrule
    { }
    {\DCheck \Gamma \Dlabels \Dnil}
\quad
\inferrule
    {\DCheck \Gamma \Dlabel t \\
     \DCheck \Gamma \Dlabels l}
    {\DCheck \Gamma \Dlabels {\Dpair t l}}
\\
\inferrule
    {\DCheck \Gamma \Dlabels l}
    {\DCheck \Gamma \Dtype {\Denum\: l}}
\quad
\inferrule
    { }
    {\DCheck \Gamma {\Denum\: {\Dpair t l}} \Dzero}
\quad
\inferrule
    {\DCheck \Gamma {\Denum\: l} n}
    {\DCheck \Gamma {\Denum\: {\Dpair t l}} {\Dsuc\: n}}
\\
\inferrule
    {\DCheck \Gamma \Dlabels l \\
     \DCheck {\Gamma, x : \Denum\: l} \Dtype B}
    {\DSynth \Gamma {\Drecord\: l \Das x \Dret B} \Dtype}
\\
\inferrule
    {\DSynth \Gamma e {\Denum\: l} \\
     \DCheck {\Gamma, x : \Denum\: l} \Dtype B \\
     \DCheck \Gamma {\Drecord\: l \Das x \Dret B} cs}
    {\DSynth \Gamma {\Dcase\: e \Das x \Dret B \Dwith cs} {\Dsubst B e x}}
\end{mathpar}

\begin{align*}
  \Drecord\: \Dnil \Das x \Dret B 
  &\leadsto \Dunit \\
  \Drecord\: {\Dpair t l} \Das x \Dret B
  &\leadsto {\Dsubst B \Dzero x} \Dtimes 
             \Drecord\: l \Das x \Dret (\Dsubst B {\Dsuc\: x} x) \\
\\
  \Dcase\: \Dzero \Das x \Dret B \Dwith {\Dpair b bs}
  &\leadsto b \\
  \Dcase\: (\Dsuc\: n) \Das x \Dret B \Dwith {\Dpair b bs}
  &\leadsto \Dcase\: n \Das x \Dret B \Dwith bs
\end{align*}

\caption{Enumerations}
\end{figure*}

\newcommand{\Ddesc}{\Dkw{Desc}}
\newcommand{\Ddunit}{\Dquote{\Dunit}}
\newcommand{\Ddvar}{\Dquote{\Dkw{X}}}
\newcommand{\Ddrecord}{\Dquote{\Drecord}}
\newcommand{\Ddsigma}{\Dquote{\Sigma}}
\newcommand{\Ddprod}{\mathrel{\Dquote{\!\times}}}
\newcommand{\Ddpi}{\Dquote{\Pi}}

\newcommand{\Dinterp}[1]{\llbracket #1 \rrbracket}
\newcommand{\Dmu}{\mu}
\newcommand{\Dcon}{\Dkw{con}}
\newcommand{\DAll}{\Dkw{All}}
\newcommand{\Dall}{\Dkw{all}}
\newcommand{\Dind}{\Dkw{ind}}

\begin{figure*}

\begin{mathpar}
\inferrule
    { }
    {\DCheck \Gamma \Dtype \Ddesc} 
\quad
\inferrule
    { }
    {\DCheck \Gamma \Ddesc \Ddunit} 
\quad
\inferrule
    { }
    {\DCheck \Gamma \Ddesc \Ddvar} 
\\
\inferrule
    {\DCheck \Gamma \Dlabels l \\\\
      \DCheck \Gamma {\Drecord\: l \Das \_ \Dret \Ddesc} Ds}
    {\DCheck \Gamma \Ddesc {\Ddrecord\: l\: Ds}} 
\quad
\inferrule
    {\DCheck \Gamma \Dtype S \\\\
      \DCheck \Gamma {S \Dto \Ddesc} D}
    {\DCheck \Gamma \Ddesc {\Ddsigma\: S\: D}} 
\quad
\inferrule
    {\DCheck \Gamma \Ddesc D_1 \\\\
      \DCheck \Gamma \Ddesc D_2}
    {\DCheck \Gamma \Ddesc {D_1 \Ddprod D_2}}
\quad
\inferrule
    {\DCheck \Gamma \Dtype S \\\\
      \DCheck \Gamma {S \Dto \Ddesc} D}
    {\DCheck \Gamma \Ddesc {\Ddpi\: S\: D}}
\\
\inferrule
    {\DCheck \Gamma \Ddesc D \\
     \DCheck \Gamma \Dtype X}
    {\DSynth \Gamma {\Dinterp{D}\: X} \Dtype}
\\
\inferrule
    {\DCheck \Gamma \Ddesc D}
    {\DCheck \Gamma \Dtype {\Dmu\: D}}
\quad
\inferrule
    {\DCheck \Gamma {\Dinterp{D}\: (\Dmu\: D)} xs}
    {\DCheck \Gamma {\Dmu\: D} {\Dcon\: xs}}
\\
\inferrule
    {\DCheck \Gamma \Ddesc D\\
      \DCheck \Gamma \Dtype X\\
      \DCheck {\Gamma, x : X} \Dtype B\\
     \DCheck \Gamma {\Dinterp{D}\: X} xs}
    {\DSynth \Gamma {\DAll_{D,X}\: xs \Das x \Dret B} \Dtype}
\\
\inferrule
    {\DCheck \Gamma \Ddesc D\\
     \DCheck \Gamma {\Dmu\: D} xs\\
     \DCheck {\Gamma, x : \Dmu\: D} \Dtype B\\\\
     \DCheck \Gamma {\DPi{xs}{\Dinterp{D}{\Dmu\: D}}\:
                     \DAll_{D,(\Dmu\: D)}\: xs \Das x \Dret B \Dto
                     {\Dsubst B x {\Dcon\: xs}}} rec}
    {\DSynth \Gamma {\Dind_{D}\: t \Das x \Dret B\: \Dwith rec} {\Dsubst B x t}}
\end{mathpar}

\begin{align*}
  \Dinterp{\Ddunit}\: X &\leadsto \Dunit \\
  \Dinterp{\Ddvar}\: X &\leadsto X \\
  \Dinterp{\Ddrecord\: l\: Ds}\: X &\leadsto
    \DSigma{e}{\Denum\: l}\:
    (\Dcase\: e \Das \_ \Dret \Dtype \Dwith Ds) \\
  \Dinterp{\Ddsigma\: S\: D}\: X &\leadsto \DSigma{s}{S}\: \Dinterp{D\: s}\: X \\
\Dinterp{D_1 \Ddprod D_2}\: X &\leadsto \DSigma{\_}{\Dinterp{D_1}\: X}\: \Dinterp{D_2}\: X \\
  \Dinterp{\Ddpi\: S\: D}\: X &\leadsto \DPi{s}{S}\: \Dinterp{D\: s}\: X \\
\end{align*}

\caption{Descriptions}
\end{figure*}

TODO

\section{Related work}
  \label{sec:related}

TODO

\cite{leroy:compiled-strong-reduction}:
\begin{itemize}
\item key idea: a compiler allows computing weak $\beta$-normal forms
\item compile / execute / decompile
\item to get strong $\beta$-normal forms: iterate under binders
\item need to support reduction with free variables (``symbolic reduction'')
\item extends the ZAM with an \texttt{ACCU} instruction that captures the stack: make suspensions 
\end{itemize}

\cite{berger:inverse-eval,berger:nbe,berger:nbe-term-rewriting}:
\begin{itemize}
\item MinLog: a type theory in Scheme, using \texttt{eval} for normalizing
\item \cite{berger:inverse-eval}
\begin{itemize}
\item foundational, introduce the idea of NbE
\item relate normalization of a closed term to that of compiling it (Section 5, p.10)
\end{itemize}
\item \cite{berger:nbe,berger:nbe-term-rewriting}
\begin{itemize}
\item the latter is the journal version of the former, focused on (theoretical) expressive power
\item use of \texttt{eval} and benchmark only given in \cite{berger:nbe} (Section 5.2)
\end{itemize}

\end{itemize}

\section{Conclusion and future work}

TODO

%\newpage
 
\bibliographystyle{abbrv}
\bibliography{biblio}


\end{document}
