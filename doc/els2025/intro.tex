%% Intro

Dynamic versus static typing is perhaps the longest unresolved programming debate, revived almost daily on the \emph{Hacker news}\footnote{Actually, Lispers know that the debate is somewhat pointless,  considering e.g. \href{https://docs.racket-lang.org/ts-guide/index.html}{Typed Racket}, \href{https://github.com/typedclojure/typedclojure}{Typed Clojure} or \href{https://coalton-lang.github.io/}{Coalton} for instance.}.
Static typing helps to find (some) bugs early, undoubtedly. But, the other way around, types
often go in the way of just thinking about solving computation problems.
Consider for example \emph{data-oriented programming}~\citep{sharvit:data-oriented}
 that involves complex pipelines along which semi-structured data pass through chains of transformations.
With classical algebraic datatypes, the specification of such systems ofen leads to significant redundancy 
(trying to exactly capture the specifics of
each step) or loss of accuracy (merging multiple, distinct
representations in a coarser one). Compilation chains provide another similar situation, with the
implementation of multiple compilation passes, each one characterized by an input and an output intermediate
language representation. Of course, more powerful type
systems have been proposed to improve the situation: polymorphic variants~\citep{garrigue:polymorphic-variants}, refinement
types~\citep{pfenning:refinement}, set-theoretic
types~\citep{castagna:set-types}, \etc{} to name but a few.
However, if the confort improves, we still
face the situation that types remain mostly static and overall distinct from \emph{actual} computations

From a totally different perspective, \emph{dependent types}~\citep{martin-lof:tt} put into question the overly \emph{static} nature of 
classical type systems. Pioneered by Epigram~\citep{mcbride:view-from-the-left}, languages based on dependent-type theory provide an integrated development environment \emph{assisting} programmers in writing code that compute \emph{types}.
In data transformation pipelines or compilation chains, 
 each node of a data-transforming
 pipeline can be specified at the type level by a transformation \emph{performed}
 at the type-level. Most interestingly, type-level computations
 may \emph{depend} on ``normal'' value-level computations, \ie{} taking the
output of a data-transforming node to adapt the \emph{type} of the next node
 in the chain. A typical down-to-earth example is that of type-checking
\texttt{format} calls by first parsing the format strings, and then checking
the argument types. One can also think about modern web frameworks hosted in typed languages,
performing an extraction of the database schema at boot time so as to
validate the type safety of database queries~\citep{garnaes:graphql-ocaml}
ahead of any execution. Dependent
types natively support these patterns, and much
more~\citep{oury:power-of-pi}. Moreover, even though the system is
dynamic, type-checking is still performed purely at compile-time, which 
makes the approach quite different from runtime validation frameworks
such as
\texttt{clojure.spec}~\footnote{\url{https://clojure.org/guides/spec}}
or Malli~\footnote{\url{https://github.com/metosin/malli}}.

%% *** LISP remake of Gentle Art

The goal of the Deputy project\footnote{\url{https://gitlab.com/fredokun/deputy}} is to
support an integrated experience of \emph{programming with dependent-types} from \emph{within} a Lisp host
 -- actually the Clojure programming language --
in a typical \emph{domain-specific language} approach. The choice of Clojure is mostly pragmatic: the essence
of dependently-typed programming being purely functional. Another less fundamental
 but still enjoyable characteristic of Clojure is its support for vectors and maps beyond
 lists and S-expressions. The flip side of the coin is that computing with dependent
types can be quite demanding performance-wise, involving non-trivial memory usage patterns\footnote{cf. \url{https://github.com/AndrasKovacs/smalltt}}.

Technically-speaking, Deputy is a Lisp remake of the work of~\citet{dagand:gentle-art}, which describes an implementation of inductive families, the dependently-typed generalization
of algebraic datatypes~\citep{burstall:hope}. Drawing on the work
of~\citet{benke:generic-universe} and~\citet{morris:spf}, this work devised a
tiny yet powerful presentation of inductive types and inductive
families. A fascinating property of this
implementation was its reflexive nature: the formalization of
inductive types was carried (and in fact implemented!) in terms of
itself: the \emph{grammar} of inductive types was bootstrapped,
encoded as an inductive type similar to any other inductive
definition. To achieve this feat, the entire type theory was designed
to natively support a (restricted) from of homoiconicity, heavily
inspired by Lisp representation of lists as cons-cells. As a
consequence, native and reflected terms are syntactically identical
and the bootstrap goes unnoticed for the user. The present work reiterates this bootstrap process\footnote{Also helped by a talented student: thank you, Teo Bernier !}, taking full advantage of the host language and programming environment.

The presentations being done, we now provide the outline of the remainder
of the papier. In Section~\ref{sec:symbolic}, we illustrate one of the main
 \emph{tangible} features of the Deputy system: the fact that the user of
the system can interact in a typical REPL-based workflow with the main
and most complex component of the sytem: its type-checker.
In Section~\ref{sec:core-tt}, we introduce dependent types through the Curry-Howard
  correspondance adopting dependent types
  as a means of specifying our programs.
 We next develop statically-typed counterparts to several idiomatic
  Clojure idioms (Section~\ref{sec:presentation}), namely keywords and
  maps. This enables us to transfer the burden of validating basic
  schemas from run-time to compile-time~;
 We ultimately extend our type theory with inductive types
  (Section~\ref{sec:inductives}), thus giving ourselves the ability to
  program with recursive (well-founded) data-structures. As a
  consequence, recursive schemas can thus be checked at compile-time
  too.

%% ** General take-away: no type-checker? keep the discipline!
%% *** Imperative programmers: ANSI/ISO C Specification Langage (ACSL), JML, etc.
%% *** claim: Data-driven functional programmer, "just" dependent types

%% General purpose spec language => as powerful as "all" of (constructive) mathematics
%% with logical constructs (pi, sigma) that reflects actual computations (lambdas, tuples)
%% ==> and usable for day-to-day Lisp programming

%% *** lacking a type checker? documentation!  (==> like in spec/malli  ==> Spec'ing functions)
%% **** resonated with us: MrScheme experiment
%% ***** Types in doc

We do expect some readers to remain unmoved by the possibility of
embedding (yet) another type checker in their favorite Lisp. Having ourselves endured quite
a few hours pondering over inscrutably complex type errors, we sympathize with
this stance: aside from the academic exercise, why subject oneself to
the ordeal of arguing with a type-checker? The general take-away of the
present work remains that dependent types offer a language as powerful
as constructive mathematics to specify computation of software artifacts. 
With great power \ldots

\section{An appertizer: type-level symbolic debugging}\label{sec:symbolic}

The primary goals of the Clojure implementation 
 are two-fold. First, we want to exploit the full
potential of the homoiconoicity requirements by having the object
language (our dependently-typed language,
Deputy), live as an
embedded language into its host language. This is the easy part.
Second, and it is a little bit more tricky, we want to explore the implications of the
Lisp-based REPL-driven interactive development workflow \emph{during type-checking}.
While typed programs do not fail (in the sense that they are immune to
 runtime type errors), type-level programs can (and do!) fail. Indeed, the
dynamic nature of dependent types make them quite hard to follow in one's head, and type errors may be tricky to fix. Programmers need assistance during type-checking. To the point were having a
 type-level debugger is far from being a silly idea. 
 So, our reasoning went, if we cannot pay for dedicated
infrastructure, how about riding on the one already provided by our
host language? This, we believe, may be the most innovative aspect
 of our implementation: the fact that the type-checker is completely
reflected into the host.

%%  Question : How to debug type-level symbolic programs ?
%% *** Type error: debugger
%% **** context = stack trace
%% **** error = breakpoint on failed assert
%% *** Type-driven programming: in situ programming in REPL
%% **** context = stack
%% **** hole

Indeed, dependent types are ``nothing but'' abstraction of programs,
with the type-checker playing the role of an interpreter, reducing the
pair of a program \emph{applied to its type} to \texttt{true} only if the program is
provably \emph{not going wrong} over any concrete value inhabiting that type. In the field of abstract
interpretation, a similar observation lead to ``abstract
debugging''~\citep{bourdoncle:abstract-debugging, monat:maintenance,
  lam:abstract-debugger}: rather than debug a program with a single
concrete, run-time value (such as an integer), one can just as well
debug the abstract program (obtained by abstract interpretation of the
concrete one) using an abstract, symbolic representation of a
potentially-infinite set of values (such as open-ended intervals of
integers).

There are therefore two distinct execution phases in the life of a
Deputy program. First, type-checking runs the program symbolically (in
an open context), following its top-level typing annotation. If that
step succeeds, the program is well-defined thus allowing us to, in a
second phase, run the actual program on concrete values. In case of an
error during the execution of the actual program, we would fire up the
debugger; in our case, the one shipped with the \textsc{Cider} mode of
Emacs~\footnote{\url{https://docs.cider.mx/cider}}.

Similarly, in case of an error during type-checking, we got into the
habit of adding a breakpoint in the offending typing rule. For
example, the following ill-typed definition
%
\begin{clj}
(defterm [wrong [f (=> :unit :unit)]
                [x :type] :unit]
  (f x))
\end{clj}
%
will fail during the type conversion rule
%
\begin{clj}
(defmethod type-check-impl
  :computation
  type-check-impl-computation
  [ctx vtype term]
  (ok> (type-synth-impl ctx term) :as synth-vtype
       [:ko> (#dbg "Cannot synthesize type.")
         {:term (a/unparse term)}]
       (if (n/alpha-equiv vtype synth-vtype)
         true
         (#dbg [:ko "Term `term` is not of type `vtype`."
           {:term (a/unparse term)
             :synth-type (a/unparse synth-vtype)
             :vtype (a/unparse vtype)}]))))
\end{clj}

Being suitably instrumented with judiciously-placed \lstinline!#dbg! breapoints (a very convenient debugging
aid provided by \textsc{Cider}), the
type-checker gets suspended at the origin of the type error we intend to debug,
bringing \textsc{Cider} into the state depicted in Figure~\ref{fig:debug}.

\begin{figure*}[htb]
  \centering
  \includegraphics[width=.8\textwidth]{debug.png}
  \caption{(Symbolic) debugging}
  \label{fig:debug}
\end{figure*}

From there, one can query the stack trace (using the command ``s'' in
\textsc{Cider}) of the type-checker, which implicitly represent the collection
of typing rules involved so far:
%
{\footnotesize
\begin{lstlisting}
        (...)
        REPL:  131  deputy.typing/eval15258/type-check-impl-computation
MultiFn.java:  239  clojure.lang.MultiFn/invoke
  typing.clj:   64  deputy.typing/type-check
  typing.clj:   52  deputy.typing/type-check
  typing.clj:  220  deputy.typing/eval1422/type-synth-impl-application
MultiFn.java:  234  clojure.lang.MultiFn/invoke
        REPL:  127  deputy.typing/eval15258/type-check-impl-computation
       (...)
\end{lstlisting}}
%
or we can query the local variables (using the command ``l'' in \textsc{Cider})
involved in that particular typing rule:
%
{\footnotesize
\begin{lstlisting}
Class: clojure.lang.PersistentArrayMap
Count: 6

--- Contents:
  type-check-impl-computation = #function[(...)]
  ctx = {f {:node :pi, :domain :unit, 
            :codomain {:node :bind, :name _, :body :unit}}, x :type}
  vtype = :unit
  term = {:node :free-var, :name x}
  res15256 = :type
  synth-vtype = :type
\end{lstlisting}}

\begin{figure*}[htb]
  \centering
  \includegraphics[width=.8\textwidth]{debug_ctx.png}
  \caption{Accessing the context by \emph{e}valuation in the debugger}
  \label{fig:debug-ctx}
\end{figure*}

More conveniently, we can also use local evaluation (using the command
``e'' in \textsc{Cider}) to call arbitrary functions defined in the Deputy
library. For example, calling \lstinline!(debug-ctx ctx)! will display
a pretty-printed representation of the \lstinline!ctx! variable
(Figure~\ref{fig:debug-ctx}).
Of course, most of the required machinery is made available thanks to
\textsc{Cider}, Clojure itself and the underlying runtime (in general, the JVM but
 JS in another possibility through Clojurescript). The contribution on our
  part is the architecture of the type-checker that provide several entry
  points for reflecting its behaviour.

%% ** Contributions
%% *** [1] Dependent type theory for the working programmer
%% %  Contributions [FP]
%% %% ===> a working implementation of a (hopefully) practical dependent-type theory
%% %% ===> implemented in (a) lisp without breaking the host language experience (Lisp UX)
%% %%      you are still in clojure and you can take advantage of dependent types (or not)
%% %%      -> it's easy to switch the language level
%% %% Ultimately it's a clojure library, part of the Clojure ecosystem 

%% %% [2] We keep the Lisp/Clojure UX =>  workflow "repl-driven"  (??? => trouver la bonne ref)

