(ns deputy.norm
  (:require
   [clojure.set :as set]
   [deputy.syntax :as s :refer [parse fun]]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [ok>]]))


;; temporary assert macro
(defmacro ensure [cond & body]
  `(do (assert ~cond)
       ~@body))

;; ************************************************************************************
;; *********************************** METHODS ********************************************
;; ************************************************************************************


(defmulti evaluate-impl
  "Evaluate term `node` in environment `env` by piggy-backing on Clojure's β-reduction."
  (fn [node] (:node node)))

(defn evaluate
  "Evaluate term `node` in environment `env` by piggy-backing on Clojure's β-reduction.
   Uses empty environment by default."
  [term]
  {:pre [(a/locally-closed? term)]}
  (evaluate-impl term))

(defmulti alpha-equiv-impl
  "Checks if `node1` and `node2` are α-equivalent. 
   Expressions have to be evaluated before the check !"
  (fn [node1 node2] [(:node node1) (:node node2)]))

(defn alpha-equiv [node1 node2]
  "Checks if `node1` and `node2` are α-equivalent. 
   Expressions have to be evaluated before the check !"
  (cond
    ;; left is reference?
    (= (u/node-type node1) :deputy.ast/ref)
    (recur (a/unref node1) node2)
    ;; right is reference?
    (= (u/node-type node2) :deputy.ast/ref)
    (recur node1 (a/unref node2))
    ;; left is certified ?
    (= (u/node-type node1) :certified)
    (recur (:term node1) node2)
    ;; right is certified ?
    (= (u/node-type node2) :certified)
    (recur node1 (:term node2))
    :else
    (alpha-equiv-impl node1 node2)))

(defn beta-equiv
  [node1 node2]
  (alpha-equiv (evaluate node1) (evaluate node2)))


;; ***********************************************************************************
;; *********************************** EVALUATE **************************************
;; ***********************************************************************************

;;; XXX : this, or let the multi fail? (what's idiomatic?)
(defmethod evaluate-impl :default evaluate-impl-default
  [_ node]
  (throw (ex-info "Cannot normalize term `node`." {:term (a/unparse node)})))

(defmethod evaluate-impl :annotation evaluate-impl-annotation
  [node]
  (evaluate-impl (:term node)))

(defmethod evaluate-impl :application evaluate-impl-application
  [node]
  (let [rator (evaluate-impl (:rator node))
        rand  (evaluate-impl (:rand node))]
    ;;(println "[evaluate] application")
    ;;(println "  => rator = " (a/unparse rator))
    ;;(println "  => rand = " (a/unparse rand))
    (if (= (u/node-type rator) :lambda)
      (evaluate-impl (a/subst rand (:body (:content rator))))
      (ensure (and (a/normal-form-neutral? rator)
                   (a/normal-form-canonical? rand))
              (a/mk-app rator rand (meta node))))))

(defmethod evaluate-impl :bound-var evaluate-impl-bound-var
  [node]
  (throw (ex-info "Cannot evaluate bound variables" {:var node})))

(defmethod evaluate-impl :free-var evaluate-impl-free-var
  [node]
  node)

(def ^:dynamic *cache-ref-normal-forms*
  true)

(defmethod evaluate-impl :deputy.ast/ref evaluate-impl-ref
  [node]
  (let [nf (get (meta (:ref node)) :normal-form)]
    (if (and *cache-ref-normal-forms* nf)
      ;; if the normal form has already been recorded, then we're done
      nf
      ;; otherwise, we need to compute it
      (let [nf (evaluate-impl (a/unref node))]
        (when *cache-ref-normal-forms*
          (alter-meta! (:ref node) #(assoc % :normal-form nf)))
        nf))))

(defmethod evaluate-impl :deputy.ast/hole evaluate-impl-hole
  [node]
  ;;(throw (ex-info "Cannot (yet) evaluate holes" {:hole (:name node)})))
  node)

(defmethod evaluate-impl nil evaluate-impl-nil
  [node]
  node)

(defmethod evaluate-impl :lambda evaluate-impl-lambda
  [node]
  (let [name (s/gen-free-name (:name (:content node)) (a/freevars (:content node)))
        body-norm (evaluate-impl (a/subst (a/mk-free name) (:body (:content node))))]
    (a/mk-lam (a/mk-bind name (a/bind name body-norm)) (meta node))))

(defmethod evaluate-impl :pair evaluate-impl-pair
  [node]
  (let [first (evaluate-impl (:first node))
        second (evaluate-impl (:second node))]
   (a/mk-pair first second (meta node))))

(defmethod evaluate-impl :sig evaluate-impl-sig
  [node]
  (let [first (evaluate-impl (:first node))
        name (s/gen-free-name (:name (:second node)) (a/freevars (:body (:second node))))
        second (evaluate-impl (a/subst (a/mk-free name) (:body (:second node))))]
    (a/mk-sig first (a/mk-bind name (a/bind name second)) (meta node))))

(defmethod evaluate-impl :proj evaluate-impl-proj
  [node]
  (let [pair (evaluate-impl (:pair node))]
    (case (u/node-type pair)
      :pair (case (:take node)
              :left (evaluate-impl (:first pair))
              :right (evaluate-impl (:second pair)))
      (ensure (a/normal-form-neutral? pair)
              (a/mk-proj (:take node) pair (meta node))))))

(defmethod evaluate-impl :pi evaluate-impl-pi
  [node]
  (let [domain (evaluate-impl (:domain node))
        name (s/gen-free-name (:name (:codomain node)) (a/freevars (:body (:codomain node))))
        codomain (evaluate-impl (a/subst (a/mk-free name) (:body (:codomain node))))]
   (a/mk-pi domain (a/mk-bind name (a/bind name codomain)) (meta node))))


;; ***********************************************************************************
;; *********************************** ALPHA-EQUIV ***********************************
;; ***********************************************************************************

(defmethod alpha-equiv-impl :default alpha-equiv-impl-default
  [node1 node2] (= node1 node2))

;; XXX : ???
;; (defmethod alpha-equiv :false [_ _] false)

(defmethod alpha-equiv-impl [:application :application] alpha-equiv-impl-application-application
  [node1 node2]
  (and (alpha-equiv (:rator node1) (:rator node2))
       (alpha-equiv (:rand node1) (:rand node2))))

(defmethod alpha-equiv-impl [:bound-var :bound-var] alpha-equiv-impl-bound-var-bound-var
  [node1 node2]
  (= (:level node1) (:level node2)))

(defmethod alpha-equiv-impl [:free-var :free-var] alpha-equiv-impl-free-var-free-var
  [node1 node2]
  (= (:name node1) (:name node2)))

(defmethod alpha-equiv-impl [:bind :bind] alpha-equiv-impl-bind-bind
  [node1 node2]
  (alpha-equiv (:body node1) (:body node2)))

(defmethod alpha-equiv-impl [:lambda :lambda] alpha-equiv-impl-lambda-lambda
  [node1 node2]
  (alpha-equiv (:content node1) (:content node2)))

(defmethod alpha-equiv-impl [:pair :pair] alpha-equiv-impl-pair-pair
  [node1 node2]
  (and (alpha-equiv (:first node1) (:first node2))
       (alpha-equiv (:second node1) (:second node2))))

(defmethod alpha-equiv-impl [:sig :sig] alpha-equiv-impl-sig-sig
  [node1 node2]
  (and (alpha-equiv (:first node1) (:first node2))
       (alpha-equiv (:second node1) (:second node2))))

(defmethod alpha-equiv-impl [:proj :proj] alpha-equiv-impl-proj-proj
  [node1 node2]
  (and (alpha-equiv (:take node1) (:take node2))
       (alpha-equiv (:pair node1) (:pair node2))))

(defmethod alpha-equiv-impl [:pi :pi] alpha-equiv-impl-pi-pi
  [node1 node2]
  (and (alpha-equiv (:domain node1) (:domain node2))
       (alpha-equiv (:codomain node1) (:codomain node2))))

