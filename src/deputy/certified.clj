(ns deputy.certified
  "Management of certified terms,
 as introduced by `defterm` or similar forms."

  (:require [deputy.utils :as u]
            [deputy.ast :as a]
            [deputy.norm :as n]
            [deputy.typing :as t])
  
  )

;; *******************************************************************************
;; ******************************* BASIC METHODS *********************************
;; *******************************************************************************

(defn mk-certified
  "A certified `term` of (evaluated) type `vtype`."
  ([term vtype] (mk-certified term vtype {}))
  ([term vtype met]
   (a/mk-meta {:node :certified
               :term term
               :vtype vtype}
              met)))

(defmethod a/freevars :certified freevars-certified
  [node]
  (a/freevars (:term node)))

(defmethod a/locally-closed-impl? :certified locally-closed-impl?-certified
  [idx node]
  (a/locally-closed-impl? idx (:term node)))

(defmethod a/normal-form-neutral? :certified normal-form-neutral?-certified
  [node]
  (a/normal-form-neutral? (:term node)))

(defmethod a/normal-form-canonical? :certified normal-form-canonical?-certified
  [node]
  (a/normal-form-canonical? (:term node)))

(defmethod a/subst-impl :certified subst-impl-certified
  [expr idx node]
  ;; Note: term is not certified anymore after substitution
  (a/mk-meta (a/subst-impl expr idx (:term node))
             (meta node)))

(defmethod a/bind-impl :certified bind-impl-certified
  [name idx node]
  ;; Note: term is not certified anymore after binding
  (a/mk-meta (a/bind-impl name idx (:term node))
             (meta node)))


(defmethod n/evaluate-impl :certified evaluate-impl-certified
  [node]
  (a/mk-meta (n/evaluate-impl (:term node))
             (if-let [rid (get node :def-id)]
               (assoc (meta node)
                      :deputy.certified/nf-def-id rid)
               (meta node))))

(t/set-computation! :certified)

(defmethod t/type-synth-impl :certified type-synth-impl-certified
  [_ term]
  (:vtype term))

(defmethod @#'a/unparse-impl :certified unparse-impl-certified
  [mode node]
  (let [has-ref (get (meta node) :ref-id)
        def-name (get node :def-id)]
    (if (and (not has-ref) def-name)
      (u/strip-ns def-name)
      (a/unparse mode (:term node)))))




