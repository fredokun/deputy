(ns deputy.stdlib.nat
  (:require
   [deputy.syntax :refer :all]
   [deputy.unparse :as up]
   [deputy.core :refer [defterm]]
   [deputy.extensions.fix :as r]
   [deputy.stdlib.defdata :refer [defdata]]))

(defdata Nat
   "The inductive type of natural numbers."
   []
   (ze)
   (su [n <rec>]))

;; Unparsers from ze/suc to native integers
(up/register-unparse-pattern! ::ze '(ctor [zero nil])
                              (fn [_ _] 0))

(up/register-unparse-pattern! ::su '(ctor [(succ zero) [?n nil]])
                              (fn [subst _] (let [arg (get subst '?n)]
                                              (if (int? arg)
                                                (inc arg)
                                                (list 'su arg)))))

(defterm [one Nat]
  (su ze))

(defterm [two Nat]
  (su one))

(defterm [three Nat]
  (su two))

(defterm [four Nat]
  (su three))

(defterm [five Nat]
  (su four))

(defterm [six Nat]
  (su five))

(defterm [pred [n Nat] Nat]
  (Nat-case n (λ [_] Nat) ze (fun [nk] nk)))

(defterm [ind
          [n Nat]
          [P (=> Nat :type)]
          [bc (P ze)]
          [ih (Π [k Nat]
                 (=> (P k)
                     (P (su k))))]
          (P n)]
  (r/fix Nat P n
         (fun [n]
              (Nat-case
               n
               (λ [n] (r/rec Nat P (P n)))
               (r/ret bc)
               (fun [nk]
                    (r/rec-call nk (fun [Pnk] (r/ret (ih nk Pnk)))))))))

(defterm [add [m Nat] [n Nat] Nat]
  (ind
   n
   (fun [n] Nat)
   m
   (fun [n mn] (su mn))))

(defterm [mult [m Nat] [n Nat] Nat]
  (ind n (fun [_] Nat)
       ze
       (fun [n multmn] (add m multmn))))

(defterm [double [k Nat] Nat]
  (add k k))

(defterm [fact [n Nat] Nat]
  (ind n (fun [_] Nat)
       one
       (fun [n factn] (mult factn (su n)))))
