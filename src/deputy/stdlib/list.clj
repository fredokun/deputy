(ns deputy.stdlib.list
  (:require
   [deputy.syntax :refer :all]
   [deputy.core :refer [defterm]]
   [deputy.extensions.fix :as r]
   [deputy.stdlib.defdata :refer [defdata]]
   [deputy.stdlib.boolean :refer [Bool ifte]]))

(defdata List
  "Inductive lists"
  [A :type]
  (lnil)
  (lcons [hd A] [tl <rec>]))

(defterm [ind
          [A :type]
          [as (List A)]
          [P (=> (List A) :type)]
          [bc (P (lnil A))]
          [ih (Π [a A]
                 (Π [as (List A)]
                    (=> (P as)
                        (P (lcons A a as)))))]
          (P as)]
  (r/fix (List A)
         P
         as
         (fun [as]
              (List-case A as
                         (λ [as] (r/rec (List A) P (P as)))
                         (r/ret bc)
                         (fun [a ask]
                              (r/rec-call ask (fun [Pask]
                              (r/ret (ih a ask Pask)))))))))

(defterm [lmap
          [A :type]
          [B :type]
          [f (=> A B)]
          [xs (List A)]
          (List B)]
  (ind A xs
       (fun [_] (List B))
       (lnil B)
       (fun [a _ bs] (lcons B (f a) bs))))

(defterm [filter
          [A :type]
          [pred (=> A Bool)]
          [xs (List A)]
          (List A)]
  (ind A xs
       (fun [_] (List A))
       (lnil A)
       (fun [a _ as] (ifte (pred a) (fun [_] (List A)) (lcons A a as) as))))

(defterm [append
          [A :type]
          [xs (List A)]
          [ys (List A)]
          (List A)]
  (ind A xs
       (fun [_] (List A))
       ys
       (fun [a _ xsys] (lcons A a xsys))))
