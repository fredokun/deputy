(ns deputy.stdlib.map
  (:require
   [deputy.core :refer [defterm]]
   [deputy.syntax :refer :all]
   [deputy.extensions.labels :as l]
   [deputy.extensions.enum :as e :refer [struct]]))

(defterm [smap
          [keys :labels]
          [T :type]
          :type]
  (struct keys as x return T))
