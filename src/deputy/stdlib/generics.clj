(ns deputy.stdlib.generics
  (:require
   [deputy.syntax :refer :all :exclude [+examples-enabled+]]
   [deputy.core :refer [defterm]]
   [deputy.extensions.enum :as e]
   [deputy.extensions.fix :as r]
   [deputy.extensions.desc :as d]
   [deputy.extensions.convert :as c]
   [deputy.stdlib.bottom :as b]))

(defterm [case-desc
          [D d/descD]
          [P (=> d/descD :type)]
          [case-k
            (Π [T :type] (P (d/desc-k T)))]
          [case-x
           (P d/desc-x)]
          [case-sig
            (Π [S :type]
               (Π [T (=> S d/descD)]
                  (P (d/desc-sig S T))))]
          [case-pi
            (Π [S :type]
               (Π [T (=> S d/descD)]
                  (P (d/desc-pi S T))))]
          [case-prod
            (Π [D1 d/descD]
               (Π [D2 d/descD]
                  (P (d/desc-prod D1 D2))))]
          [case-sig-struct
            (Π [ls :labels]
               (Π [cs (d/struct-desc ls)]
                  (P (d/desc-sig-struct ls cs))))]
          [case-pi-struct
            (Π [ls :labels]
               (Π [cs (d/struct-desc ls)]
                  (P (d/desc-pi-struct ls cs))))]
          (P D)]
  (c/convert!
   ((e/switch (π1 (d/inner D))
            as x
            return (Π [xs (d/interp (d/switch-desc x with
                                               [(d/dk :type)
                                                (d/dk :unit)
                                                (d/dsig :type (λ [t] (d/dpi t (λ [y] d/desc-x))))
                                                (d/dsig :type (λ [t] (d/dpi t (λ [y] d/desc-x))))
                                                (d/dprod d/desc-x d/desc-x)
                                                (d/dsig :labels (λ [t] (d/dk (d/struct-desc t))))
                                                (d/dsig :labels (λ [t] (d/dk (d/struct-desc t))))
                                                nil])
                                  d/descD)]
                      (P (d/ctor [x xs])))
            with
            [;; (vls :k :x :sig :pi :prod :sig-struct :pi-struct)
             ;; (dk :type)
             (λ [args] (case-k args))
             ;; (dk :unit)
             (λ [args] (c/convert!
                        case-x
                        from (P d/desc-x)
                        to (P (d/ctor [(e/succ zero) args]))))
             ;; (dsig :type (λ [t] (dpi t (λ [y] desc-x))))
             (λ [args] (c/convert!
                        (case-sig (π1 args) (π2 args))
                        from (P (d/ctor [(e/succ (e/succ zero)) [(π1 args) (π2 args)]]))
                        to (P (d/ctor [(e/succ (e/succ zero)) args]))))
             ;; (dsig :type (λ [t] (dpi t (λ [y] desc-x))))
             (λ [args] (c/convert!
                        (case-pi (π1 args) (π2 args))
                        from (P (d/ctor [(e/succ (e/succ (e/succ zero))) [(π1 args) (π2 args)]]))
                        to (P (d/ctor [(e/succ (e/succ (e/succ zero))) args]))))
             ;; (dprod desc-x desc-x)
             (λ [args] (c/convert!
                        (case-prod (π1 args) (π2 args))
                        from (P (d/ctor [(e/succ (e/succ (e/succ (e/succ zero)))) [(π1 args) (π2 args)]]))
                        to (P (d/ctor [(e/succ (e/succ (e/succ (e/succ zero)))) args]))))
             ;; (dsig :labels (λ [t] (dk (struct-desc t))))
             (λ [args] (c/convert!
                        (case-sig-struct (π1 args) (π2 args))
                        from (P (d/ctor [(e/succ (e/succ (e/succ (e/succ (e/succ zero))))) [(π1 args) (π2 args)]]))
                        to (P (d/ctor [(e/succ (e/succ (e/succ (e/succ (e/succ zero))))) args]))))
             ;; (dsig :labels (λ [t] (dk (struct-desc t))))
             (λ [args] (c/convert!
                        (case-pi-struct (π1 args) (π2 args))
                        from (P (d/ctor [(e/succ (e/succ (e/succ (e/succ (e/succ (e/succ zero)))))) [(π1 args) (π2 args)]]))
                        to (P (d/ctor [(e/succ (e/succ (e/succ (e/succ (e/succ (e/succ zero)))))) args]))))
             nil])
    (π2 (d/inner D)))
   from (P (d/ctor [(π1 (d/inner D)) (π2 (d/inner D))]))
   to (P D)))

(defterm [args-type
          [muD :type]
          [D d/descD]
          [P (=> (d/interp D muD) :type)]
          :type]
  ((r/fix d/descD
         (λ [D] (=> (=> (d/interp D muD) :type) :type))
         D
         (λ [D]
            (case-desc D
                       (λ [D]
                          (r/rec d/descD
                                 (λ [D] (=> (=> (d/interp D muD) :type) :type))
                                 (=> (=> (d/interp D muD) :type) :type)))
                       ;; desc-k K
                       (fun [K] (r/ret (λ [P] (Π [k K] (P k)))))
                       ;; desc-x
                       (r/ret (λ [P] (Π [x muD] (P x))))
                       ;; desc-sig
                       (fun [S T] (r/ret (λ [P] b/bottom))) ;; NYI
                       ;; desc-pi
                       (fun [S T] (r/ret (λ [P] b/bottom))) ;; NYI:
                       ;; (Σ [f (Π [s S] (interp (T s) muD))] (P f))))
                       ;; desc-prod
                       (fun [A B] (r/rec-call A (λ [ka]
                                  (r/rec-call B (λ [kb]
                                  (r/ret (λ [P] 
                                  (ka (λ [a] (kb (λ [b] (P [a b]))))))))))))
                       ;; desc-sig-struct
                       (fun [ls cs] (r/ret (λ [P] b/bottom))) ;; NYI
                       ;; desc-pi-struct
                       (fun [ls cs] (r/ret (λ [P] b/bottom)));; NYI
                       ))) P))

(defterm [args-term
          [muD :type]
          [D d/descD]
          [P (=> (d/interp D muD) :type)]
          [f (args-type muD D P)]
          [xs (d/interp D muD)]
          (P xs)]
  ((r/fix d/descD
          (λ [D] (Π [P (=> (d/interp D muD) :type)]
                    (=> (args-type muD D P) 
                        (Π [xs (d/interp D muD)] 
                           (P xs)))))
          D
          (λ [D] 
            (case-desc D 
                       (λ [D]
                          (r/rec d/descD
                                 (λ [D] (Π [P (=> (d/interp D muD) :type)]
                                           (=> (args-type muD D P) 
                                               (Π [xs (d/interp D muD)]
                                                  (P xs)))))
                                 (Π [P (=> (d/interp D muD) :type)]
                                    (=> (args-type muD D P)
                                        (Π [xs (d/interp D muD)]
                                           (P xs))))))
                       ;; desc-k K
                       (fun [K] (r/ret (λ [P p k] (p k))))
                       ;; desc-x
                       (r/ret (λ [P p xs] (p xs)))
                       ;; desc-sig
                       (fun [S T] (r/ret (λ [P bot xs] (b/exfalso (λ [_] (P xs)) bot)))) ;; NYI
                       ;; desc-pi
                       (fun [S T] (r/ret (λ [P bot xs] (b/exfalso (λ [_] (P xs)) bot)))) ;; NYI
                       ;; desc-prod
                       (fun [A B] (r/rec-call B (λ [pb]
                                  (r/rec-call A (λ [pa]
                                  (r/ret (λ [P0 at ab]
                                            (c/convert!
                                             (pb
                                              (λ [b] (P0 [(π1 ab) b]))
                                              (pa
                                               (λ [a] (args-type muD B (λ [b] (P0 [a b]))))
                                               at
                                               (π1 ab))
                                              (π2 ab))
                                             from (P0 [(π1 ab) (π2 ab)])
                                             to (P0 ab)))))))))
                       ;; desc-sig-struct
                       (fun [ls cs] (r/ret (λ [P bot xs] (b/exfalso (λ [_] (P xs)) bot)))) ;; NYI
                       ;; desc-pi-struct
                       (fun [ls cs] (r/ret (λ [P bot xs] (b/exfalso (λ [_] (P xs)) bot))));; NYI
                       ))) P f xs))


(defterm [pattern-type
          [ls :labels]
          [cs (d/struct-desc ls)]
          [P (=> (d/mu (d/desc-sig-struct ls cs)) :type)]
          [e (e/enum ls)]
          :type]
  (args-type (d/mu (d/desc-sig-struct ls cs))
             (d/switch-desc e with cs)
             (λ [xs] (P (d/ctor [e xs])))))

(defterm [pattern-term
          [ls :labels]
          [cs (d/struct-desc ls)]
          [P (=> (d/mu (d/desc-sig-struct ls cs)) :type)]
          [e (e/enum ls)]
          [p (pattern-type ls cs P e)]
          [es (d/interp (d/switch-desc e with cs)
                        (d/mu (d/desc-sig-struct ls cs)))]
          (P (d/ctor [e es]))]
  (args-term
   (d/mu (d/desc-sig-struct ls cs))
   (d/switch-desc e with cs)
   (λ [xs] (P (d/ctor [e xs])))
   p
   es))

(defterm [patterns-type
          [ls :labels]
          [cs (d/struct-desc ls)]
          [P (=> (d/mu (d/desc-sig-struct ls cs)) :type)]
          :type]
  (e/struct ls as x return (pattern-type ls cs P x)))

(defterm [patterns-term
          [ls :labels]
          [cs (d/struct-desc ls)]
          [P (=> (d/mu (d/desc-sig-struct ls cs)) :type)]
          [ps (patterns-type ls cs P)]
          [e (e/enum ls)]
          [es (d/interp (d/switch-desc e with cs)
                        (d/mu (d/desc-sig-struct ls cs)))]
          (P (d/ctor [e es]))]
  (pattern-term ls cs P e (e/switch e as x return (pattern-type ls cs P x) with ps) es))

(defterm [gcase
          [ls :labels]
          [cs (d/struct-desc ls)]
          [P (=> (d/mu (d/desc-sig-struct ls cs)) :type)]
          [x (d/mu (d/desc-sig-struct ls cs))]
          [cases (patterns-type ls cs P)]
          (P x)]
  (c/convert!
   (patterns-term ls cs P cases (π1 (d/inner x)) (π2 (d/inner x)))
     from (P (d/ctor [(π1 (d/inner x)) (π2 (d/inner x))]))
     to (P x)))

