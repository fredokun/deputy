(ns deputy.stdlib.boolean
  (:require
   [deputy.syntax :refer :all]
   [deputy.core :refer [defterm]]
   [deputy.stdlib.defdata :refer [defdata]]))


(defdata Bool
   "The Booleans."
   []
   (ttrue)
   (ffalse))

(defterm [ifte [b Bool] [P (=> Bool :type)] [e1 (P ttrue)] [e2 (P ffalse)] (P b)]
  (Bool-case ?hole1 P (synth e1 ?hole2) e2))

