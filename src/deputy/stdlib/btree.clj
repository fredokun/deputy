(ns deputy.stdlib.btree
  (:require
   [deputy.syntax :refer :all]
   [deputy.core :refer [defterm]]
   [deputy.extensions.fix :as r]
   [deputy.stdlib.list :as list]
   [deputy.stdlib.defdata :refer [defdata]]))

(defdata Tree
  "Binary trees"
  [A :type, B :type]
  (leaf [a A])
  (node [l <rec>] [b B] [r <rec>]))

(defterm [ind
          [A :type]
          [B :type]
          [t (Tree A B)]
          [P (=> (Tree A B) :type)]
          [bc (Π [a A] (P (leaf A B a)))]
          [ih (Π [ls (Tree A B)]
                 (Π [b B]
                    (Π [rs (Tree A B)]
                       (=> (P ls)
                           (P rs)
                           (P (node A B ls b rs))))))]
          (P t)]
  (r/fix (Tree A B)
         P
         t
         (fun [t]
              (Tree-case A B t
                         (λ [t] (r/rec (Tree A B) P (P t)))
                         (λ [a] (r/ret (bc a)))
                         (fun [ls b rs]
                              (r/rec-call ls (fun [Pls]
                              (r/rec-call rs (fun [Prs]
                              (r/ret (ih ls b rs Pls Prs)))))))))))

(defterm [flatten
          [A :type]
          [B :type]
          [t (Tree A B)]
          (list/List A)]
  (r/fix (Tree A B)
         (λ [_] (list/List A))
         t
         (fun [t]
              (Tree-case A B t
                         (λ [t] (r/rec (Tree A B) (λ [_] (list/List A)) (list/List A)))
                         (λ [a] (r/ret (list/lcons A a (list/lnil A))))
                         (λ [ls _ rs]
                            (r/rec-call ls (fun [fls]
                            (r/rec-call rs (fun [frs]
                            (r/ret (list/append A fls frs)))))))))))
