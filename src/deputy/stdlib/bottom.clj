(ns deputy.stdlib.bottom
  (:require
   [deputy.syntax :refer :all]
   [deputy.extensions.enum :as e]
   [deputy.core :refer [defterm]]))

(defterm [bottom :type]
  (e/enum nil))

(defterm [exfalso
          [P (=> bottom :type)]
          [e bottom]
          (P e)]
  (e/switch e as x return (P x) with nil))
