(ns deputy.stdlib.defdata
  "Simpler creation of inductives through description"
  (:require
   [deputy.syntax :refer :all]
   [deputy.utils :as u :refer [ko-expr? throw-ko ok>]]
   [deputy.extensions.labels :refer [vls]]
   [deputy.extensions.enum :as e :refer [switch struct enum index succ]]
   [deputy.extensions.desc :as d]
   [deputy.extensions.convert :as c]
   [deputy.stdlib.generics :as g]
   [deputy.core :refer [defterm]]))

;;; The general syntax of the defdata form is as follows :

(comment

  (defdata T
    "<docstring>"
    [a1 A1, a2 A2, ... , aN AN]
    (c0 [x1-0 X1-0]  [x2-0 X2-0] ...  [x1-M XM-0])
    ...
    (cQ [x1-Q X1-Q]  [x2-Q X2-Q] ...  [xR-Q XR-Q]))

)

(declare parse-params
         parse-constructors)

(defn parse-defdata [name args]
  (ok> 
   ;; type name
   (when-not (symbol? name)
     [:ko "Type name of defdata should be a symbol" {:name name}])
   (when (empty? args)
     [:ko "Definition of defdata is empty" {:defdata name}])
   ;; docstring
   (if (string? (first args))
     [(first args) (rest args)]
     ["" args]) :as [docstring args]
   ;; parameters
   (when-not (vector? (first args))
     [:ko "Missing parameters vector" {:defdata name}])
   [(first args) (rest args)] :as [params args]
   (parse-params params) :as params
   ;; constructors
   (parse-constructors args) :as constructors

   {:name name
    :dostring docstring
    :params params
    :constructors constructors}))

(defn parse-params [ps]
  (loop [ps ps, pname nil, res []]
    (if (seq ps)
      (cond
        ;; a vector parameter
        (and (nil? pname)
             (vector? (first ps))
             (= (count (first ps)) 2)
             (symbol? (ffirst ps)))
        (recur (rest ps) pname (conj res (first ps)))
        ;; a parameter name
        (and (nil? pname)
             (symbol? (first ps)))
        (recur (rest ps) (first ps) res)
        ;; a type expression (at least a tentative one)
        (symbol? pname)
        (recur (rest ps) nil (conj res [pname (first ps)]))
        :else [:ko "Wrong parameter definition" {:param (first ps)}])
      ;; end of sequence
      (if (symbol? pname)
        [:ko "Expecting a type for parameter" {:param pname}]
        res))))

(declare parse-constructor constr)

(defn parse-constructors [cs]
  (loop [cs cs, res []]
    (if (seq cs)
      (let [constr (parse-constructor (first cs))]
        (if (ko-expr? constr)
          constr
          (recur (rest cs) (conj res constr))))
      ;; no more constructors
      res)))

(defn parse-constructor [constr]
  (ok> (when-not (sequential? constr)
         [:ko "Wrong constructor definition" {:constructor constr}])
       (when-not (symbol? (first constr))
         [:ko "Constructor name must be a symbol" {:name (first constr)}])
       [(first constr) (rest constr)] :as [name params]
       (parse-params params) :as params

       {:name name
        :params params}))

(declare expand-constructors
         expand-codes
         expand-desc
         expand-type
         expand-smarts
         expand-case)

(defmacro defdata [name & args]
  (let [ddef (parse-defdata name args)]
    (when (ko-expr? ddef)
      (throw-ko ddef))
    (concat (list 'do
                  (expand-constructors ddef)
                  (expand-codes ddef)
                  (expand-desc ddef)
                  (expand-type ddef))
            (expand-smarts ddef)
            (list (expand-case ddef))
            (list [:defined (list 'quote name)]))))

(defn expand-constructors [ddef]
  (let [tyname (symbol (str (:name ddef) "-constructors"))
        lbls (map #(keyword (:name %)) (:constructors ddef))]
    `(defterm [~tyname :labels]
       (vls ~@lbls))))

(declare expand-cstr-codes)

(defn expand-codes [ddef]
  (let [tyname (symbol (str (:name ddef) "-codes"))
        tyconst (symbol (str (:name ddef) "-constructors"))
        header (vec (concat [tyname]
                            (:params ddef)
                            (list `(d/struct-desc ~tyconst))))
        body (u/vecbuild (map #(expand-cstr-codes (map second (:params %))) (:constructors ddef)))]
    `(defterm ~header ~body)))


(defn expand-cstr-codes [ts]
  (if (seq ts)
    (let [rst (expand-cstr-codes (rest ts))]
      (list `d/dprod (if (= (first ts) '<rec>)
                       `d/dx
                       `(d/dk ~(first ts)))
            rst))
    `(d/dk :unit)))

(defn expand-desc [ddef]
  (let [tyname (symbol (str (:name ddef) "-desc"))
        tycstr (symbol (str (:name ddef) "-constructors"))
        tycodes (symbol (str (:name ddef) "-codes"))
        header (vec (concat [tyname]
                            (:params ddef)
                            [`d/descD]))
        tycodes-params (map first (:params ddef))
        tycodes-call (if (seq tycodes-params)
                       (cons tycodes tycodes-params)
                       tycodes)]
    (list `defterm header (list `d/dsig-struct tycstr tycodes-call))))


(defn expand-type [ddef]
  (let [header (vec (concat [(:name ddef)]
                            (:params ddef)
                            [:type]))
        ty-desc (symbol (str (:name ddef) "-desc"))]
    (list `defterm header (list `d/mu (if (seq (:params ddef))
                                        (cons ty-desc (map first (:params ddef)))
                                        ty-desc)))))
    

(defn expand-smarts [ddef]
  (let [tycall (if (seq (:params ddef))
                 (cons (:name ddef) (map first (:params ddef)))
                 (:name ddef))] 
    (loop [cs (:constructors ddef), num 'zero, res []]
      (if (seq cs)
        (let [cstr (first cs)
              cparams (map (fn [[x ty]]
                             [x (if (= ty '<rec>) tycall ty)]) (:params cstr))
              header (vec (concat (list (:name cstr))
                                  (:params ddef)
                                  cparams
                                  (list tycall)))
              content (u/vecbuild (cons num (map first (:params cstr))))
              body (list `d/ctor content)]
          (recur (rest cs) (list `e/succ num) (conj res (list `defterm header body))))
        ;; no more constructor
        res))))
 

(declare expand-case-params
         expand-case-constr)

(defn expand-case [ddef]
  (let [tyname (symbol (str (:name ddef) "-case"))
        rec (if (seq (:params ddef))
              (cons (:name ddef) (map first (:params ddef)))
              (:name ddef))
        header (vec (concat (list tyname)
                            (:params ddef)
                            (list ['t rec])
                            (list ['P (list '=> rec :type)])
                            (expand-case-params ddef)
                            (list (list 'P 't))))
        cstrs (u/vecbuild (map (partial expand-case-constr (:params ddef)) (u/zip (range) (:constructors ddef))))
        codes (let [csym (symbol (str (:name ddef) "-codes"))]
                (if (seq (:params ddef))
                  (cons csym (map first (:params ddef)))
                  csym))]
    (list `defterm header 
          (list `g/gcase 
                (symbol (str (:name ddef) "-constructors"))
                codes
                'P 't
                cstrs))))

(defn expand-case-params [ddef]
  (let [rec (if (seq (:params ddef))
              (cons (:name ddef) (map first (:params ddef)))
              (:name ddef))]
    (loop [cs (:constructors ddef), pnames [], pvals []]
      (if (seq cs)
        (let [{cname :name, cparams :params} (first cs)
              pname (symbol (str "case-" (name cname)))
              pval (loop [ps (reverse cparams), 
                          res (list 'P (let [args (concat (map first (:params ddef))
                                                          (map first cparams))]
                                         (if (seq args)
                                           (cons (symbol (name cname)) args)
                                           (symbol (name cname)))))]
                     (if (seq ps)
                       (let [[x ty] (first ps)
                             ty' (if (= ty '<rec>) rec ty)]
                         (recur (rest ps) (list 'Π [x ty'] res)))
                       res))]
          (recur (rest cs) (conj pnames pname) (conj pvals pval)))
        ;; end
        (map vector pnames pvals)))))


(declare mkindex)

(defn expand-case-constr [params [n constr]]
  (let [{cname :name, cparams :params} constr
        cname (name cname)
        case-name (symbol (str "case-" cname))
        idx (mkindex n)
        case-call (if (seq cparams)
                    (cons case-name (map first cparams))
                    case-name)
        all-params (concat (map first params) (map first cparams))
        cstr-call (if (seq all-params)
                    (cons (symbol cname) all-params)
                    (symbol cname))]
    (list 'λ (vec (concat (map first cparams)
                          (list 'tt)))
          (list `c/convert! case-call 
                'from (list 'P cstr-call)
                'to (list 'P (list `d/ctor [idx (u/vecbuild (map first cparams) 'tt)]))))))
                                                          

(defn mkindex [n]
  (if (zero? n)
    'zero
    (list `e/succ (mkindex (dec n)))))
