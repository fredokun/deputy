(ns deputy.core
  (:require
   [deputy.utils :as u :refer [ko-expr? throw-ko ok> example examples]]
   [deputy.syntax :as s :refer [parse parse-core]]
   [deputy.ast :as a]
   [deputy.certified :as cert]
   [deputy.norm :as n]
   [deputy.typing :as t]))

(def +examples-enabled+ true)

;;; Syntax of defterm

;;; Non-certified definition :
;;; (defterm sym {doc} term)

;;; Parameterless certified definition :
;;; (defterm [sym ty] {doc} term)

;;; Parameterized certified definition :
;;; (defterm [sym [param1 ty1] [param2 ty2] ... [paramN tyN] ty] {doc} term)

(defn parse-defterm [args]
  (ok> (when (empty? args)
         [:ko "Missing arguments for `defterm`" {}])
       (when (= (count args) 1) 
         [:ko "Not enough arguments in `defterm`" {:arguments args}])
       (when (> (count args) 3)
         [:ko "Too many arguments for `defterm`" {:name (first args)
                                                  :arguments (rest args)}])
       (if (symbol? (first args))
         ;; Non-certified definition
         (ok> (if (= (count args) 2)
                nil
                (second args)) :as docstr
              (when (and (= (count args) 3)
                         (not (string? docstr)))
                [:ko "The documentation string is not a string" {:name (first args),
                                                                 :doc docstr}])
              (if (= (count args) 2)
                (second args)
                (nth args 2)) :as term
              [:non-certified {:sym (first args), :doc docstr, :term term}])
         ;; Certified definition
         (ok> (first args) as vhdr
              (when-not (vector? vhdr)
                [:ko "Wrong `defterm` header" {:header vhdr}])
              (when (= (count vhdr) 1)
                [:ko "Missing argument in `defterm` header" {:header vhdr}])
              (first vhdr) :as sym
              (when-not (symbol? sym)
                [:ko "Name of `defterm` must be a symbol" {:name sym}])
              (butlast (rest vhdr)) :as param-list 
              (last (rest vhdr)) :as ret-ty
              (loop [params param-list, ret []]
                (if (seq params)
                  (cond 
                    (not (and (vector? (first params))
                              (= (count (first params)) 2)))
                    [:ko "Wrong parameter definition in `defterm`" {:name sym,
                                                                    :param (first params)}]
                    (not (symbol? (ffirst params)))
                    [:ko "Parameter name is not a symbol in `defterm`" {:name sym
                                                                        :param (first params)}]
                    :else
                    (recur (rest params) (conj ret (first params))))
                  ret)) :as parameters
              (if (= (count args) 2)
                nil
                (second args)) :as docstr
              (when (and (= (count args) 3)
                         (not (string? docstr)))
                [:ko "The documentation string is not a string" {:name sym,
                                                                 :doc docstr}])
              (if (= (count args) 2)
                (second args)
                (nth args 2)) :as term
              [:certified {:sym sym, :params parameters, :ret-ty ret-ty,
                           :doc docstr, :term term}]))))

(examples

 (parse-defterm '())
 => '[:ko "Missing arguments for `defterm`" {}]

 (parse-defterm '(x))
 => '[:ko "Not enough arguments in `defterm`" {:arguments (x)}]

 (parse-defterm '(x 42))
 => '[:non-certified {:sym x, :doc nil, :term 42}]
   
 (parse-defterm '(x "this is `x`" 42))
 => '[:non-certified {:sym x, :doc "this is `x`", :term 42}]

)

(examples

 (parse-defterm '([x nat]))
 => '[:ko "Not enough arguments in `defterm`" {:arguments ([x nat])}]

 (parse-defterm '([x nat] 42))
 => '[:certified {:sym x, :params [], :ret-ty nat, :doc nil, :term 42}]
          
 (parse-defterm '([x y nat nat] 42))
 => '[:ko "Wrong parameter definition in `defterm`" {:name x, :param y}]

 (parse-defterm '([x [y] nat] 42))
 => '[:ko "Wrong parameter definition in `defterm`" {:name x, :param [y]}]

 (parse-defterm '([x [y nat] nat] 42))
 => '[:certified {:sym x, :params [[y nat]], :ret-ty nat, :doc nil, :term 42}]

 (parse-defterm '([x [y nat] [z nat] toto nat] 42))
 => '[:ko "Wrong parameter definition in `defterm`" {:name x, :param toto}]

 (parse-defterm '([x [y nat] [z nat 33] nat] 42))
 => '[:ko "Wrong parameter definition in `defterm`" {:name x, :param [z nat 33]}]

)

(defn defterm-validation [type-ast term-ast]
  ;;(println "[validation] type-ast=" type-ast)
  ;;(println "  ==> term-ast=" term-ast)
  (ok> (t/type-check :type type-ast)
       (n/evaluate type-ast) :as vtype
       (t/type-check vtype term-ast)
       vtype))


(defn defterm-decorate [ref-id params term-ast]
  (if (seq params)
    (let [rest-ast (defterm-decorate ref-id (rest params) (:body (:content term-ast)))]
      (assoc-in term-ast [:content :body] rest-ast))
    (if (get term-ast :node)
      (a/mk-meta term-ast {:ref-id ref-id})
      term-ast))) 

(defmacro defterm 
  "Generic form for defining deputy terms.

  A term definition of the form :

  (defterm <name> <term>)

  will associated the name <name> to term <term>.
  This term is *not* type-checked and thus neither certified.
  It is just a kind of an alias.

  The definition of certified forms *without parameters* 
  has the following syntax.

  (defterm [<name> <type>] <term>)

  This definition is type-checked and thus the <name> is certified
  of having the specified <term> (if no exception is raised)

  Parameterized definitions are introduced with the following variant:

  (defterm [<name> [<param1> <ptype1>] ... [<paramN> <ptypeN>] <ret-type>] 
    <term>)

  It is similar to parameter-less definition :

  (defterm [<name> (=> <ptype1> ... <ptypeN> <ret-type>)]
    (λ [<param1> ... <paramN>] <term>))

  Except that explicit parameters allow much friendler error
messages (in case of e.g. a type error).  

 Remark: these generate top-level definitions, hence meta-data
 and docstrings can be provided as in a Clojure `def`.
  "
  [& args]
  (let [ret (parse-defterm args)]
    (when (ko-expr? ret)
      (throw-ko ret))
    (let [met (or (meta (first args)) {})]
      (if (= (first ret) :non-certified)
        ;; non-certified definition
        (let [[_ {sym :sym, doc :doc, term :term}] ret]
          `(let [term-ast# (binding [s/*resolve-identifiers* true]
                             (parse ~term))]
             (when (ko-expr? term-ast#)
               (throw-ko term-ast#))
             (def ~sym term-ast#)
             (alter-meta! (var ~sym) #(assoc (merge % ~met) :deputy true :doc ~doc))
             [:defined '~sym]))
        ;; certified definition
        (let [[_ {sym :sym, params :params, ret-ty :ret-ty, doc :doc, term :term}] ret
              type (reduce (fn [ty [param param-ty]]
                             (list 'Π [param param-ty] ty)) ret-ty (reverse params))
              ;;_ (println "type=" type)
              term (reduce (fn [t [param _]]
                             (list 'fun [param] t)) term (reverse params))
              ;;_ (println "term=" term)
              ]
          `(binding [s/*holes-enabled* true]
             (let [type-ast# (binding [s/*resolve-identifiers* true]
                               (parse ~type))
                   term-ast# (binding [s/*resolve-identifiers* true]
                               (parse ~term))]
               (when (ko-expr? type-ast#)
                 (throw-ko type-ast#))
               (when (ko-expr? term-ast#)
                 (throw-ko term-ast#))
               (reset! t/+holes-context+ {})
               (let [valid# (defterm-validation type-ast# term-ast#)]
                 (when (ko-expr? valid#)
                   (throw-ko [:ko "The defined term has not the specified type" {:term (quote ~sym)
                                                                                 :type (quote ~type)
                                                                                 :cause valid#}]))
                 (let [vtype# valid#
                       nterm-ast# (defterm-decorate 
                                    (u/qualified-keyword-from-sym (quote ~sym))
                                    (quote ~params)
                                    term-ast#)]
                   (def ~sym (assoc (cert/mk-certified nterm-ast# vtype#)
                                    ;; XXX: the following does not work (unconditional patterns)
                                    :def-id (u/qualified-keyword-from-sym (quote ~sym))))
                   (alter-meta! (var ~sym) #(assoc (merge % ~met) 
                                                   :deputy true 
                                                   :doc ~doc))
                   (let [holes-ctx# @t/+holes-context+]
                     (if-not (empty? holes-ctx#)
                       [:defined '~sym :holes holes-ctx#]
                       [:defined '~sym])))))))))))


(def last-error (atom nil))

(defn extract-root-cause [msg data]
  (if-let [cause (get data :cause)]
    (let [[ko msg data] cause]
      (recur msg data))
    [msg data]))

(defmacro try-defterm
  "A variant of [[defterm]] that does not throw
  exceptions in case of failure.

  In case no error is raised, this form has the same effect
  and return value as [[defterm]].
  If an error is raised, the root cause of the error is
  returned, and the full error trace can be obtained
  by the form `@last-error`

  Important: use this form only for debugging purpose,
  otherwise possibly erronous term definitions will
  be silently accepted."
  [& args]
  `(do (reset! last-error nil)
       (try
         (defterm ~@args)
         (catch clojure.lang.ExceptionInfo e#
           (let [[msg# data#] (extract-root-cause (ex-message e#) (ex-data e#))]
             (reset! last-error [:error (ex-message e#) (ex-data e#)])
             [:error msg# data#])))))


(defn -main
  []
  (let [node (parse (lambda [a b] (+ c a)))]
    (println node)
    (print node)))
