(ns deputy.extensions.desc
  (:require
   [clojure.set :as set]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [example examples ok>]]
   [deputy.syntax :as s :refer [λ defparse]]
   [deputy.norm :as n]
   [deputy.typing :as t]
   [deputy.certified :as cert]
   [deputy.core :refer [defterm]]
   [deputy.extensions.labels :as l :refer [vls]]
   [deputy.extensions.enum :as e :refer [index succ enum]]))

;; (defonce +examples-enabled+ true)


;; ************************************************************************************
;; *********************************** CONSTRUCTORS ***********************************
;; ************************************************************************************

(defn mk-interp
  ([desc type] (mk-interp desc type {}))
  ([desc type met]
   (a/mk-meta {:node :interp
               :desc desc
               :type type}
              met)))
  
(defn mk-mu
  ([desc] (mk-mu desc {}))
  ([desc met]
   (a/mk-meta {:node :mu
               :desc desc}
              met)))

(defn mk-ctor
  ([xs] (mk-ctor xs {}))
  ([xs met]
   (a/mk-meta {:node :ctor
               :xs xs}
              met)))

(defn mk-inner
  ([ctor] (mk-inner ctor {}))
  ([ctor met]
   (a/mk-meta {:node :inner
               :ctor ctor}
              met)))

(defn mk-struct-desc
  ([labels] (mk-struct-desc labels {}))
  ([labels met]
   (a/mk-meta {:node :struct-desc
               :labels labels}
              met)))

(defn mk-switch-desc
  ([idx pattern] (mk-switch-desc idx pattern {}))
  ([idx pattern met]
   (a/mk-meta {:node :switch-desc
               :idx idx
               :pattern pattern}
              met)))

;; ****************************************************************************
;; *********************************** CTOR ***********************************
;; ****************************************************************************


(defparse ctor ::ctor)

(defn parse-desc-ctor
  [parse benv term]
  (ok>
   (parse benv (second term)) :as t
   (mk-ctor t)))

(s/add-parse-pattern [(u/starts-with ::ctor) parse-desc-ctor])

(defmethod @#'a/unparse-impl :ctor unparse-impl-ctor
  [mode node]
  (list 'ctor (a/unparse mode (:xs node))))

(defmethod a/freevars :ctor freevars-ctor
  [node]
  (a/freevars (:xs node)))

(defmethod a/locally-closed-impl? :ctor locally-closed-impl?-ctor
  [idx node]
  (a/locally-closed-impl? idx (:xs node)))

(defmethod a/normal-form-canonical? :ctor normal-form-canonical?-ctor
  [node]
  (a/normal-form-canonical? (:xs node)))

(defmethod a/subst-impl :ctor subst-impl-ctor
  [expr idx node]
  (mk-ctor (a/subst-impl expr idx (:xs node)) (meta node)))

(defmethod a/bind-impl :ctor bind-impl-ctor
  [expr idx node]
  (mk-ctor (a/bind-impl expr idx (:xs node)) (meta node)))

(defmethod n/alpha-equiv-impl [:ctor :ctor] alpha-equiv-impl-ctor-ctor
  [node1 node2]
  (n/alpha-equiv (:xs node1) (:xs node2)))




;; ****************************************************************************
;; *********************************** INTERP *********************************
;; ****************************************************************************


(defparse interp ::interp)

(defn parse-desc-interp
  [parse benv term]
  (ok>
   (parse benv (second term)) :as desc
   (parse benv (nth term 2)) :as type
   (mk-interp desc type)))

(s/add-parse-pattern [(u/starts-with ::interp) parse-desc-interp])

(defmethod @#'a/unparse-impl :interp unparse-impl-interp
  [mode node]
  (list 'interp (a/unparse mode (:desc node)) (a/unparse mode (:type node))))

(defmethod a/freevars :interp freevars-interp
  [node]
  (set/union (a/freevars (:desc node)) (a/freevars (:type node))))

(defmethod a/locally-closed-impl? :interp locally-closed-impl?-interp
  [idx node]
  (and (a/locally-closed-impl? idx (:desc node)) (a/locally-closed-impl? idx (:type node))))

(defmethod a/normal-form-canonical? :interp normal-form-canonical?-interp
  [node]
  (and (a/normal-form-canonical? (:desc node)) (a/normal-form-canonical? (:type node))))

(defmethod a/subst-impl :interp subst-impl-interp
  [expr idx node]
  (mk-interp (a/subst-impl expr idx (:desc node))
             (a/subst-impl expr idx (:type node))
             (meta node)))

(defmethod a/bind-impl :interp bind-impl-interp
  [expr idx node]
  (mk-interp (a/bind-impl expr idx (:desc node))
             (a/bind-impl expr idx (:type node))
             (meta node)))


(defmethod n/alpha-equiv-impl [:interp :interp] alpha-equiv-impl-interp-interp
  [node1 node2]
  (and (n/alpha-equiv (:desc node1) (:desc node2))
       (n/alpha-equiv (:type node1) (:type node2))))




;; ****************************************************************************
;; *********************************** MU *************************************
;; ****************************************************************************

(defparse mu ::mu)
(defparse μ ::mu)

(defn parse-desc-mu
  [parse benv term]
  (ok>
   (parse benv (second term)) :as t
   (mk-mu t)))

(s/add-parse-pattern [(u/starts-with ::mu) parse-desc-mu])

(defmethod @#'a/unparse-impl :mu unparse-impl-mu
  [mode node]
  (list 'μ (a/unparse mode (:desc node))))

(defmethod a/freevars :mu freevars-mu
  [node]
  (a/freevars (:desc node)))

(defmethod a/locally-closed-impl? :mu locally-closed-impl?-mu
  [idx node]
  (a/locally-closed-impl? idx (:desc node)))

(defmethod a/normal-form-canonical? :mu normal-form-canonical?-mu
  [node]
  (a/normal-form-canonical? (:desc node)))

(defmethod a/subst-impl :mu subst-impl-mu
  [expr idx node]
  (mk-mu (a/subst-impl expr idx (:desc node)) (meta node)))

(defmethod a/bind-impl :mu bind-impl-mu
  [expr idx node]
  (mk-mu (a/bind-impl expr idx (:desc node)) (meta node)))

(defmethod n/alpha-equiv-impl [:mu :mu] alpha-equiv-impl-mu-mu
  [node1 node2]
  (n/alpha-equiv (:desc node1) (:desc node2)))






;; ****************************************************************************
;; *********************************** SWITCH-DESC ****************************
;; ****************************************************************************

(defparse switch-desc ::switch-desc)

(defn parse-switch-desc
  [parse benv term]
  (ok>
   (parse benv (second term)) :as idx
   (parse benv (nth term 3)) :as pattern
   (mk-switch-desc idx pattern)))

(s/add-parse-pattern [(u/starts-with ::switch-desc) parse-switch-desc])

(defmethod @#'a/unparse-impl :switch-desc unparse-impl-switch-desc
  [mode node]
  (list 'switch-desc (a/unparse mode (:idx node)) 'with (a/unparse mode (:pattern node))))

(defmethod a/freevars :switch-desc freevars-switch-desc
  [node]
  (set/union (a/freevars (:idx node))
             (a/freevars (:pattern node))))

(defmethod a/locally-closed-impl? :switch-desc locally-closed-impl?-switch-desc
  [idx node]
  (and (a/locally-closed-impl? idx (:idx node))
       (a/locally-closed-impl? idx (:pattern node))))

(defmethod a/normal-form-neutral? :switch-desc normal-form-neutral?-switch-desc
  [node]
  (and (a/normal-form-neutral? (:idx node))
       (a/normal-form-canonical? (:pattern node))))

(defmethod a/subst-impl :switch-desc subst-impl-switch-desc
  [expr idx node]
  (mk-switch-desc (a/subst-impl expr idx (:idx node))
                  (a/subst-impl expr idx (:pattern node))
                  (meta node)))

(defmethod a/bind-impl :switch-desc bind-impl-switch-desc
  [name idx node]
  (mk-switch-desc (a/bind-impl name idx (:idx node))
                  (a/bind-impl name idx (:pattern node))
                  (meta node)))

(defmethod n/alpha-equiv-impl [:switch-desc :switch-desc] alpha-equiv-impl-switch-desc-switch-desc
  [node1 node2]
  (and (n/alpha-equiv (:idx node1) (:idx node2))
       (n/alpha-equiv (:pattern node1) (:pattern node2))))


;; ****************************************************************************
;; *********************************** STRUCT-DESC ****************************
;; ****************************************************************************

(defparse struct-desc ::struct-desc)

(defn parse-struct-desc
  [parse benv term]
  (ok>
   (parse benv (second term)) :as labels
   (mk-struct-desc labels)))

(s/add-parse-pattern [(u/starts-with ::struct-desc) parse-struct-desc])

(defmethod @#'a/unparse-impl :struct-desc unparse-impl-struct-desc
  [mode node]
  (list 'struct-desc (a/unparse mode (:labels node))))



(defmethod a/freevars :struct-desc freevars-struct-desc
  [node]
  (a/freevars (:labels node)))

(defmethod a/locally-closed-impl? :struct-desc locally-closed-impl?-struct-desc
  [idx node]
  (a/locally-closed-impl? idx (:labels node)))

(defmethod a/normal-form-neutral? :struct-desc normal-form-neutral?-struct-desc
  [node]
  (a/normal-form-neutral? (:labels node)))

(defmethod a/subst-impl :struct-desc subst-impl-struct-desc
  [expr idx node]
  (mk-struct-desc (a/subst-impl expr idx (:labels node)) (meta node)))

(defmethod a/bind-impl :struct-desc bind-impl-struct-desc
  [name idx node]
  (mk-struct-desc (a/bind-impl name idx (:labels node)) (meta node)))

(defmethod n/alpha-equiv-impl [:struct-desc :struct-desc] alpha-equiv-impl-struct-desc-struct-desc
  [node1 node2]
  (n/alpha-equiv (:labels node1) (:labels node2)))



;; ****************************************************************************
;; *********************************** INNER **********************************
;; ****************************************************************************

(defparse inner ::inner)

(defn parse-desc-inner
  [parse benv term]
  (ok>
   (parse benv (second term)) :as t
   (mk-inner t)))

(s/add-parse-pattern [(u/starts-with ::inner) parse-desc-inner])

(defmethod @#'a/unparse-impl :inner unparse-impl-inner
  [mode node]
  (list 'inner (a/unparse mode (:ctor node))))

(defmethod a/freevars :inner freevars-inner
  [node]
  (a/freevars (:ctor node)))

(defmethod a/locally-closed-impl? :inner locally-closed-impl?-inner
  [idx node]
  (a/locally-closed-impl? idx (:ctor node)))

(defmethod a/normal-form-neutral? :inner normal-form-neutral?-inner
  [node]
  (a/normal-form-neutral? (:ctor node)))

(defmethod a/subst-impl :inner subst-impl-inner
  [expr idx node]
  (mk-inner (a/subst-impl expr idx (:ctor node)) (meta node)))

(defmethod a/bind-impl :inner bind-impl-inner
  [name idx node]
  (mk-inner (a/bind-impl name idx (:ctor node)) (meta node)))

(defmethod n/alpha-equiv-impl [:inner :inner] alpha-equiv-impl-inner-inner
  [node1 node2]
  (n/alpha-equiv (:ctor node1) (:ctor node2)))





;; ****************************************************************************
;; *********************************** DESC? **********************************
;; ****************************************************************************

(defn desc-k?
  [node]
  (= (:first node) (e/index 0)))

(defn desc-x?
  [node]
  (= (:first node) (e/index 1)))

(defn desc-sig?
  [node]
  (= (:first node) (e/index 2)))

(defn desc-pi?
  [node]
  (= (:first node) (e/index 3)))

(defn desc-prod?
  [node]
  (= (:first node) (e/index 4)))

(defn desc-sig-struct?
  [node]
  (= (:first node) (e/index 5)))

(defn desc-pi-struct?
  [node]
  (= (:first node) (e/index 6)))


;; ****************************************************************************
;; *********************************** EVALUATE *******************************
;; ****************************************************************************



(defmethod n/evaluate-impl :ctor evaluate-impl-ctor
  [node]
  ;; (ctor xs) (normalized)
  (mk-ctor (n/evaluate-impl (:xs node)) (meta node)))


(defmethod n/evaluate-impl :mu evaluate-impl-mu
  [node]
  ;; (mu desc) (normalized)
  (mk-mu (n/evaluate-impl (:desc node)) (meta node)))

(defmethod n/evaluate-impl :inner evaluate-impl-inner
  [node]
  ;; (inner ctor)
  (let [ctor
   ;; ctor (normalized)
        (n/evaluate-impl (:ctor node))]
    (case (u/node-type ctor)
      :ctor (:xs ctor)
      (do (assert (a/normal-form-neutral? ctor))
          (mk-inner ctor (meta node))))))

(defmethod n/evaluate-impl :switch-desc evaluate-impl-switch-desc
  [node]
  ;; (switch-desc idx cs)
  (let [;; idx (normalized)
        idx-norm (n/evaluate-impl (:idx node))]
    (case (u/node-type idx-norm)
      'zero (n/evaluate-impl (a/mk-proj :left (:pattern node)))
      :succ (let [;; (π2 cs)
                  new-cs (n/evaluate-impl (a/mk-proj :right (:pattern node)))
                  ;; n (normalized)
                  n (:idx idx-norm)]
              (n/evaluate-impl (mk-switch-desc n new-cs (meta node))))

      (do (assert (a/normal-form-neutral? idx-norm))
          (let [;; cs (normalized)
                new-cs (n/evaluate-impl (:pattern node))]
            ;; (switch-desc idx cs) (normalized)
            (mk-switch-desc idx-norm new-cs (meta node)))))))

(declare descD)

(defmethod n/evaluate-impl :struct-desc evaluate-impl-struct-desc
  [node]
  ;; (struct-desc labels)
  (let [;; labels (normalized)
        labels-norm (n/evaluate-impl (:labels node))]
    (case (u/node-type labels-norm)
      ;; empty labels
      nil :unit

      ;; cons labels [l ls]
      :tags (let [;; ls (normalized)
                  remaining-labels-norm (:labels labels-norm)
                  ;; (struct-desc ls) (not normalized)
                  remaining-struct (mk-struct-desc remaining-labels-norm (meta node))]
              ;; (Σ desc (struct-desc ls)) (normalized)
              (n/evaluate-impl (a/mk-sig descD (a/mk-bind '_ remaining-struct))))

      (do (assert (a/normal-form-neutral? labels-norm))
          ;; (struct-desc labels) (normalized)
          (mk-struct-desc labels-norm (meta node))))))

(defmethod n/evaluate-impl :interp evaluate-impl-interp
  [node]
  ;; (interp ctor type)

  (let [ctor
        ;; ctor (normalized)
        (n/evaluate-impl (:desc node))
        ty
        ;; type (normalized)
        (n/evaluate-impl (:type node))
        xs
        ;; (ctor xs)
        (:xs ctor)]
    (case (u/node-type ctor)
     :ctor
     (let [xs (:xs ctor)]
       (case (u/node-type xs)
         :pair
         (cond
           ;; (ctor [zero type])
           (desc-k? xs) (n/evaluate-impl (a/mk-proj :right xs))
           
           ;; (ctor [(succ zero) nil])
           (desc-x? xs) ty

           ;; (ctor [(succ (succ zero)) type (fn [x: type] desc)])
           (desc-sig? xs) (let [second
                                ;; [sig-type (fn [x: sig-type] desc)]
                                (n/evaluate-impl (a/mk-proj :right xs))
                                sig-type
                                ;; sig-type (normalized)
                                (n/evaluate-impl (a/mk-proj :left second))
                                desc
                                ;; (fn [x: sig-type] desc) as D -> (interp (D p) type)
                                (mk-interp (a/mk-app (a/mk-proj :right second) (a/mk-bound 's 0))
                                           ty
                                           (meta node))]
                            ;; (Σ [p sig-type] (interp (D p) type)) (normalized)
                            (n/evaluate-impl (a/mk-sig sig-type (a/mk-bind 's desc))))
           
           ;; (ctor [(succ (succ (succ zero))) type (fn [x: type] desc)])
           (desc-pi? xs) (let [second
                               ;; [pi-type (fn [x: pi-type] desc)]
                               (n/evaluate-impl (a/mk-proj :right xs))
                               pi-type
                               ;; pi-type (normalized)
                               (n/evaluate-impl (a/mk-proj :left second))
                               desc
                               ;; (fn [x: pi-type] desc) as D -> (interp (D p) type)
                               (mk-interp (a/mk-app (a/mk-proj :right second) (a/mk-bound 'p 0))
                                          ty
                                          (meta node))]
                           ;; (Π [p pi-type] (interp (D p) type)) (normalized)
                           (n/evaluate-impl (a/mk-pi pi-type (a/mk-bind '_ desc))))
           
           ;; (ctor [(succ (succ (succ (succ zero)))) desc1 desc2])
           (desc-prod? xs) (let [second
                                 ;; [desc1 desc2] (normalized)
                                 (n/evaluate-impl (a/mk-proj :right xs))
                                 desc1
                                 ;; (interp desc1 type) (normalized)
                                 (n/evaluate-impl (mk-interp (a/mk-proj :left second) ty))
                                 desc2
                                 ;; (interp desc2 type) (normalized)
                                 (n/evaluate-impl (mk-interp (a/mk-proj :right second) ty))]
                             ;; (Σ [_ (interp desc1 type)] (interp desc2 type))
                             (a/mk-sig desc1 (a/mk-bind '_ desc2)))

           ;; (ctor [(succ (succ (succ (succ (succ zero))))) labels struct])
           (desc-sig-struct? xs) (let [second
                                       ;; [labels struct] (normalized)
                                       (n/evaluate-impl (a/mk-proj :right xs))
                                       labels
                                       ;; labels (normalized)
                                       (n/evaluate-impl (a/mk-proj :left second))
                                       struct
                                       ;; struct (normalized)
                                       (n/evaluate-impl (a/mk-proj :right second))]
                                   ;; (Σ [e (enum labels)] (interp (switch-desc e struct) type))
                                   (s/sig [e (enum (clj labels))] (interp (switch-desc e with (clj struct)) (clj ty))))
           
           ;; (ctor [(succ (succ (succ (succ (succ (succ zero)))))) labels struct])
           (desc-pi-struct? xs) (let [second
                                      ;; [labels struct] (normalized)
                                      (n/evaluate-impl (a/mk-proj :right xs))
                                      labels
                                      ;; labels (normalized)
                                      (n/evaluate-impl (a/mk-proj :left second))
                                      struct
                                      ;; struct (normalized)
                                      (n/evaluate-impl (a/mk-proj :right second))]
                                  ;; (Π [e (enum labels)] (interp (switch-desc e struct) type))
                                  (s/pi [e (enum (clj labels))] (interp (switch-desc e with (clj struct)) (clj ty))))

           ;; if (:first xs) is in neutral form
           :else  (do (assert (a/normal-form-neutral? (:first xs)))
                      ;; (interp ctor type) (normalized)
                      (mk-interp ctor ty (meta node))))

         ;; if xs is in neutral form
         (do (assert (a/normal-form-neutral? xs))
                      ;; (interp ctor type) (normalized)
             (mk-interp ctor ty (meta node)))))

     ;; if ctor is in neutral form
     (do (assert (a/normal-form-neutral? ctor))
         ;; (interp ctor type) (normalized)
         (mk-interp ctor ty (meta node))))))


;; ****************************************************************************
;; *********************************** DESC-CONSTRUCTORS **********************
;; ****************************************************************************

(defparse desc-k ::desc-k)
(defparse dk ::desc-k)

(defn parse-desc-k
  [parse benv term]
  (ok> (parse benv (second term)) :as arg
       (mk-ctor (a/mk-pair (e/index 0) arg))))

(def ^{:deputy true
       :ref-id ::desc-x}
  desc-x (cert/mk-certified (mk-ctor (a/mk-pair (e/index 1) nil))
                            (a/mk-ref #'descD)))

(def ^{:deputy true} dx desc-x)


;;(defterm desc-x (clj (a/mk-certified (mk-ctor (a/mk-pair (e/index 1) nil))
;;                                    (a/mk-ref descD))))
;;(defterm dx (clj desc-x))

(defparse desc-sig ::desc-sig)
(defparse dsig ::desc-sig)

(defn parse-desc-sig
  [parse benv term]
  (ok> (parse benv (second term)) :as arg1
       (parse benv (nth term 2)) :as arg2
       (mk-ctor (a/mk-pair (e/index 2) (a/mk-pair arg1 arg2)))))

(defparse desc-pi ::desc-pi)
(defparse dpi ::desc-pi)

(defn parse-desc-pi
  [parse benv term]
  (ok> (parse benv (second term)) :as arg1
       (parse benv (nth term 2)) :as arg2
       (mk-ctor (a/mk-pair (e/index 3) (a/mk-pair arg1 arg2)))))


(defparse desc-prod ::desc-prod)
(defparse dprod ::desc-prod)

(defn parse-desc-prod
  [parse benv term]
  (ok> (parse benv (second term)) :as arg1
       (parse benv (nth term 2)) :as arg2
       (mk-ctor (a/mk-pair (e/index 4) (a/mk-pair arg1 arg2)))))

(defparse desc-sig-struct ::desc-sig-struct)
(defparse dsig-struct ::desc-sig-struct)

(defn parse-desc-sig-struct
  [parse benv term]
  (ok> (parse benv (second term)) :as arg1
       (parse benv (nth term 2)) :as arg2
       (ctor [(e/index 5) (clj arg1) (clj arg2)])))

(defparse desc-pi-struct ::desc-pi-struct)
(defparse dpi-struct ::desc-pi-struct)

(defn parse-desc-pi-struct
  [parse benv term]
  (ok> (parse benv (second term)) :as arg1
       (parse benv (nth term 2)) :as arg2
  (ctor [(e/index 6) (clj arg1) (clj arg2)])))


(s/add-parse-pattern [(u/starts-with ::desc-k) parse-desc-k]
                     [(u/starts-with ::desc-sig) parse-desc-sig]
                     [(u/starts-with ::desc-pi) parse-desc-pi]
                     [(u/starts-with ::desc-prod) parse-desc-prod]
                     [(u/starts-with ::desc-sig-struct) parse-desc-sig-struct]
                     [(u/starts-with ::desc-pi-struct) parse-desc-pi-struct])

;; ****************************************************************************
;; *********************************** DESCD **********************************
;; ****************************************************************************

(def descD-ctor-def
  (dsig-struct
   (vls :k :x :sig :pi :prod :sig-struct :pi-struct)
   [(dk :type)
    (dk :unit)
    (dsig :type (λ [t] (dpi t (λ [y] desc-x))))
    (dsig :type (λ [t] (dpi t (λ [y] desc-x))))
    (dprod desc-x desc-x)
    (dsig :labels (λ [t] (dk (struct-desc t))))
    (dsig :labels (λ [t] (dk (struct-desc t))))
    nil]))

(defterm descD-ctor (clj (cert/mk-certified descD-ctor-def
                                            (mk-mu descD-ctor-def)
                                            {:ref-id ::descD-ctor})))


(defterm descD (clj (cert/mk-certified (mk-mu descD-ctor-def) :type
                                       {:ref-id ::descD})))


;; ****************************************************************************
;; *********************************** TYPE CHECK AND SYNTH *******************
;; ****************************************************************************

(defmethod t/type-synth-impl :struct-desc type-synth-impl-struct-desc
  [ctx term]
  (ok>
   (:labels term) :as t
   (t/type-check ctx :labels t)
   [:ko> "Type Synthesis Error : bad labels." {:labels (a/unparse t)}]

   :type))


(t/set-computation! :struct-desc)

(defmethod t/type-synth-impl :switch-desc type-synth-impl-switch-desc
  [ctx term]
  (ok>
   (:idx term) :as ne
   (t/type-synth-impl ctx ne) :as netype
   [:ko> "Type Synthesis Error : Cannot synthesize type of left hand-side (operator) of a switch-desc."
    {:operator (a/unparse ne)}]

   (when-not (= (:node netype) :enum)
     [:ko "Left hand-side (operator) of application must be an enum."
      {:operator (a/unparse ne)
       :type (a/unparse netype)}])

   (:labels netype) :as vl

   (:pattern term) :as cs
   (n/evaluate (mk-struct-desc vl)) :as st
   (t/type-check ctx st cs)
   [:ko> "Type Synthesis Error : bad cases" {:cases (a/unparse cs)
                                             :expected (a/unparse struct)}]

   descD))


(t/set-computation! :switch-desc)


(defmethod @#'t/type-check-impl [:mu :ctor] type-check-impl-mu-ctor
  [ctx vtype term]
  (ok> (:desc vtype) :as desc
       (:xs term) :as xs

       (n/evaluate (mk-interp desc vtype)) :as dtype
       (t/type-check ctx dtype xs)
       [:ko> "Type Checking Error : Invalid arguments." {:type (a/unparse vtype)
                                                         :interp (a/unparse dtype)
                                                         :ctor (a/unparse term)}]
       true))


(defmethod t/type-synth-impl :interp type-synth-impl-interp
  [ctx term]
  (ok> (:desc term) :as desc
       (:type term) :as type
       (t/type-check ctx descD desc)
       [:ko "Type Synthesis Error : Invalid description" {:desc (a/unparse desc)}]
       (t/type-check ctx :type type)
       [:ko "Type Synthesis Error : Invalid recursive argument" {:rec (a/unparse desc)}]
       :type))

(t/set-computation! :interp)

(defmethod @#'t/type-check-impl [:type :mu] type-check-impl-type-mu
  [ctx vtype term]
  (ok> (:desc term) :as d
       (t/type-check ctx descD d)
       [:ko> "Type Checking Error : Invalid description." {:desc (a/unparse d)}]))

(defmethod t/type-synth-impl :inner type-synth-impl-inner
  [ctx term]
  (ok> (t/type-synth-impl ctx (:ctor term)) :as mu
       (when-not (= (:node mu) :mu)
         [:ko "Type Synthesis Error : type of `ctor` has to be `(mu D)` where `D` is of type `:desc`."
          {:ctor (a/unparse (:ctor term))
           :type (a/unparse mu)}])
       (n/evaluate (mk-interp (:desc mu) mu))))

(t/set-computation! :inner)


(examples
 (t/type-check descD descD-ctor)
 => true)

(examples
 (t/type-check descD (desc-k :unit))
 => true

 (t/type-check descD (desc-k :type))
 => true

 (t/type-check descD (desc-prod (desc-k :type) desc-x))
 => true)

