
(ns deputy.extensions.lets
  "This namespace introduces a `let` form
as an extension to the deputy kernel.

The syntax is :

```
(let [[x1 ty1] e1
      [x2 ty2] e2
      ...
      [xN tyN] eN]
  e)
```

The semantics is the same as common `let` forms.
"
  (:require
   [clojure.set :as set]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [ok> ko-expr?]]
   [deputy.syntax :as s]
   [deputy.norm :as n]
   [deputy.typing :as t]))

(defn mk-lets
  ([params body] (mk-lets params body {}))
  ([params body met]
   (a/mk-meta {:node :let
               :params params
               :body body}
              met)))

(declare parse-lets-params)

(defn parse-lets [parse benv term]
  (ok> (when (not= (count term) 3)
         [:ko> "Wrong arity for `let` form, expecting 3." {:term term
                                                           :arity (count term)}])
       term :as [_ params body]
       (when-not (vector? params)
         [:ko> "`let` form variable definitions must be a vector." {:vars params}])
       (parse-lets-params parse benv params) :as [params benv']
       (parse benv' body) :as body
       (mk-lets params body)))

(defn parse-lets-params [parse benv params]
  (loop [params params, benv benv, pparams []]
    (if (seq params)
      (let [res
            (ok> (when-not (and (vector? (first params))
                                (= (count (first params)) 2))
                   [:ko> "Wrong variable definition in `let` form." {:var (first params)}])
                 (first params) :as [x ty]
                 (when-not (symbol? x)
                   [:ko> "Variable name in `let` must be a symbol." {:var (first params)}])
                 (parse benv ty) :as ty
                 (when-not (seq (rest params))
                   [:ko> "Missing let-variable expression." {:var (first params)}])
                 (parse benv (second params)) :as term
                 (conj benv x) :as benv'
                 [x ty term benv'])]
        (if (ko-expr? res)
          res
          (let [[x ty term benv'] res]
            (recur (rest (rest params)) benv' (conj pparams [[x ty] term])))))
      ;; no more param
      [pparams benv])))

(s/defparse lets ::let)
       
(s/add-parse-pattern [(u/starts-with 'let) parse-lets]
                     [(u/starts-with 'lets) parse-lets]
                     [(u/starts-with ::let) parse-lets])


(defmethod @#'a/unparse-impl :let unparse-impl-let
  [mode node]
  (let [uparams (loop [params (:params node), res []]
                  (if (seq params)
                    (let [[[x ty] t] (first params)]
                      (recur (rest params)
                             (-> res
                                 (conj [x (a/unparse mode ty)])
                                 (conj (a/unparse mode t)))))
                    res))]
    (list 'let uparams (a/unparse mode (:body node)))))

(defmethod a/freevars :let freevars-let
  [node]
  (reduce (fn [fvs [[x ty] t]]
            (set/union fvs (a/freevars ty) (a/freevars t)))
          (a/freevars (:body node)) (:params node)))


(defmethod a/locally-closed-impl? :let locally-closed-impl?-let
  [idx node]
  (let [idx' (loop [params (:params node), idx idx]
               (if (seq params)
                 (let [[[_ ty] t] (first params)]
                   (if (and (a/locally-closed-impl? idx ty)
                            (a/locally-closed-impl? idx ty))
                     (recur (rest params) (inc idx))
                     false))
                 idx))]
    (if idx'
      (a/locally-closed-impl? idx' (:body node))
      false)))


(defn subst-let-params [expr idx params]
  ;;(println "[subst-let-params] expr = " (a/unparse #{:index} expr) (str "(idx=" idx ")"))
  (loop [params params, idx idx, nparams []]
    (if (seq params)
      (let [[[x ty] t] (first params)
            ;;_ (println "  • param  =" [[x (a/unparse #{:index} ty)] (a/unparse #{:index} t)])
            [ty' t'] [(a/subst-impl expr idx ty) (a/subst-impl expr idx t)]
            ;;_ (println "  • param' =" [[x (a/unparse #{:index} ty')] (a/unparse #{:index} t')])
            ]
        (recur (rest params) 
               (inc idx) 
               (conj nparams [[x ty'] t'])))
      [idx nparams])))
    
(defmethod a/subst-impl :let subst-impl-let
  [expr idx node]
  ;;(println "[subst-impl/let]" (a/unparse node) (str "[" (a/unparse expr) "/" idx "]"))
  (let [[idx' nparams] (subst-let-params expr idx (:params node))]
    (let [res (mk-lets nparams
                       (a/subst-impl expr idx' (:body node))
                       (meta node))]
      ;;(println "  ==> " (a/unparse #{:index} res))
      res)))

(defn bind-let-params [name idx params]
  (loop [params params, idx idx, nparams []]
    (if (seq params)
      (let [[[x ty] t] (first params)]
        (recur (rest params) 
               (inc idx) 
               (conj nparams 
                     [[x (a/bind-impl name idx ty)] (a/bind-impl name idx t)])))
      [idx nparams])))
  
(defmethod a/bind-impl :let bind-impl-let
  [name idx node]
  (let [[idx' nparams] (bind-let-params name idx (:params node))]
    (mk-lets nparams
             (a/bind-impl name idx' (:body node))
             (meta node))))


(defmethod n/evaluate-impl :let evaluate-impl-let
  [node]
  (if (seq (:params node))
    (let [[[x ty] term] (first (:params node))]
      (n/evaluate-impl (a/subst (n/evaluate term)
                                (mk-lets (rest (:params node)) (:body node) (meta node)))))
      ;; no more parameter
      (n/evaluate (:body node))))

(t/set-computation! :let)

(declare type-check-let-params)

(defmethod t/type-synth-impl :let type-synth-impl-let
  [ctx term]
  ;;(println "[type-synth/let] term =" (a/unparse #_#{:index} term))
  (if (seq (:params term))
    (let [[[x ty] t] (first (:params term))]
      (ok> 
       ;;(println "  t = " t)
       ;;(println "  ty = " (a/unparse #_#{:index} ty))
       (t/type-check ctx :type ty)
       [:ko> "Type error for `let` parameter type: not a `:type`." {:param x
                                                                    :param-type ty}]
       (n/evaluate ty) :as vtype
       (t/type-check ctx vtype t)
       [:ko> "Type error for `let` parameter." {:param x
                                                    :type (a/unparse ty)
                                                    :term (a/unparse t)}]
       (t/type-synth-impl ctx (a/subst (a/mk-annot ty (n/evaluate t) (meta t))
                                       (mk-lets (rest (:params term)) (:body term) (meta term))))))
    ;; no more params
    (t/type-synth-impl ctx (:body term))))



