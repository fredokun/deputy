(ns deputy.extensions.boolean
  (:refer-clojure :exclude [if]) 
  (:require
   [clojure.set :as set]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [example examples ok>]]
   [deputy.syntax :as s]
   [deputy.norm :as n]
   [deputy.typing :as t]))

(defn mk-if
  ([cnd tret tthen telse] (mk-if cnd tret tthen telse {}))
  ([cnd tret tthen telse met]
   (a/mk-meta {:node :if
               :condition cnd
               :return tret
               :then tthen
               :else telse}
              met)))

(defn parse-if-impl [t]
  (ok> (when (not= (count t) 4)
         [:ko> "Wrong arity for if expression, expecting 4." {:term t
                                                              :arity (count t)}])
       t :as [_ u tthen telse]
       (when (or (not (vector? u))
                 (not= (count u) 5))
         [:ko> "Wrong condition vector for if expression." {:condition u}])
       u :as [cnd _ x _ Tret]
       (when (not (symbol? x))
         [:ko> "Condition binder must be a symbol." {:binder x}])
       [:ok cnd x Tret tthen telse]))


(defn parse-if
  [parse benv term]
  (ok> (parse-if-impl term) :as [_ cnd x Tret tthen telse]
       (parse benv cnd) :as cnd
       (conj benv x) :as benv'
       (parse benv' Tret) :as Tret
       (parse benv tthen) :as tthen
       (parse benv telse) :as telse
       (mk-if cnd (a/mk-bind x Tret) tthen telse)))

;;; XXX : this does not work locally because `if seems parsed
;;;       as a primitive in clojure ...
(s/defparse if ::if)

(s/add-parse-pattern [true? s/parse-const]
                     [false? s/parse-const]
                     [#(= % :bool) s/parse-const]
                     ;; XXX : but this works, sort of...
                     [(u/starts-with 'if) parse-if]
                     ;; and this too...
                     [(u/starts-with ::if) parse-if])


(defmethod @#'a/unparse-impl :if unparse-impl-if
  [mode node]
  (let [cnd (a/unparse mode (:condition node))
        tret (a/unparse mode (:body (:return node)))
        tthen (a/unparse mode (:then node))
        telse (a/unparse mode (:else node))]
    (if (= mode :index)
      (list 'if [cnd tret] tthen telse)
      (list 'if [cnd :as (:name (:return node))
                 :return tret] tthen telse))))

(defmethod a/freevars :if freevars-if
  [node]
  (set/union (a/freevars (:condition node))
             (a/freevars (:body (:return node)))
             (a/freevars (:then node))
             (a/freevars (:else node))))

(defmethod a/locally-closed-impl? :if locally-closed-impl?-if
  [idx node]
  (and (a/locally-closed-impl? idx (:condition node))
       (a/locally-closed-impl? idx (:return node))
       (a/locally-closed-impl? idx (:then node))
       (a/locally-closed-impl? idx (:else node))))

(defmethod a/normal-form-neutral? :if normal-form-neutral?-if
  [node]
  (and (a/normal-form-neutral? (:condition node))
       (a/normal-form-canonical? (:return node))
       (a/normal-form-canonical? (:then node))
       (a/normal-form-canonical? (:else node))))

(defmethod a/subst-impl :if subst-impl-if
  [expr idx node]
  (mk-if (a/subst-impl expr idx (:condition node))
         (a/subst-impl expr idx (:return node))
         (a/subst-impl expr idx (:then node))
         (a/subst-impl expr idx (:else node))
         (meta node)))


(defmethod a/bind-impl :if bind-impl-if
  [name idx node]
  (mk-if (a/bind-impl name idx (:condition node))
         (a/bind-impl name idx (:return node))
         (a/bind-impl name idx (:then node))
         (a/bind-impl name idx (:else node))
         (meta node)))

(defmethod n/evaluate-impl :if evaluate-impl-if
  [node]
  (let [cond-val (n/evaluate-impl (:condition node))]
    (cond
      (true? cond-val) (n/evaluate-impl (:then node))
      (false? cond-val) (n/evaluate-impl (:else node))
      :else
      (mk-if cond-val (:return node)
             (n/evaluate-impl (:then node))
             (n/evaluate-impl (:else node))
             (meta node)))))

(defmethod n/alpha-equiv-impl [:if :if] alpha-equiv-impl-if-if
  [node1 node2]
  (and (n/alpha-equiv (:condition node1) (:condition node2))
       ;; (n/alpha-equiv (:return node1) (:return node2)) ;; <<- not needed ?
       (n/alpha-equiv (:then node1) (:then node2))
       (n/alpha-equiv (:else node1) (:else node2))))

(t/set-correct! [:type :bool] [:bool true] [:bool false])

(t/set-computation! :if)

(defmethod t/type-synth-impl :if type-synth-impl-if
  [ctx term]
  (ok> (:condition term) :as c

       (t/type-synth-impl ctx c) :as ctype
       [:ko> "Type Synthesis Error : Cannot synthesize type of condition of an if."
        {:condition (a/unparse c)}]

       (when-not (= ctype :bool)
         [:ko> "Type Synthesis Error : Condition of `if` must be a boolean"
          {:condition (a/unparse c)
           :type (a/unparse ctype)}])

       (:then term) :as tthen
       (:else term) :as telse

       (s/gen-free-name (:name (:return term)) ctx) :as name
       (a/mk-free name) :as x
       (a/subst x (:body (:return term))) :as T
       (assoc ctx name :bool) :as new-ctx

       (t/type-check new-ctx :type T)
       [:ko> "Type Synthesis Error : Return of `if` is not a type." {:motive (a/unparse T)}]

       ;; old:
       ;;(n/evaluate (a/mk-lam (a/mk-bind (:bind term) (:return term)))) :as vT

       ;; old:
       ;;(vT true) :as vTtrue
       ;; new:
       (n/evaluate (a/subst true (:body (:return term)))) :as vTtrue

       (t/type-check ctx vTtrue tthen)
       [:ko> "Type Synthesis Error : Wrong type for then branch of `if`."
        {:then-term (a/unparse tthen)
         :then-type (a/unparse vTtrue)}]

       ;; old:
       ;; (vT false) :as vTfalse
       ;;new:
       (n/evaluate (a/subst false (:body (:return term)))) :as vTfalse

       (t/type-check ctx vTfalse telse)
       [:ko> "Type Synthesis Error : Wrong type for else branch of `if`."
        {:else-term (a/unparse telse)
         :else-type (a/unparse vTfalse)}]
       (n/evaluate (a/subst c (:body (:return term))))))








