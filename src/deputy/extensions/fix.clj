(ns deputy.extensions.fix
  (:require
   [clojure.set :as set]
   [clojure.test :refer [is]]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [example examples ok> seq1?]]
   [deputy.syntax :as s :refer [Π λ fun app]]
   [deputy.norm :as n]
   [deputy.typing :as t]))

;; (def +examples-enabled+ true)

;; ****************************************************************************
;; *********************************** REC ***********************************
;; ****************************************************************************

(s/defparse rec ::rec)

(defn mk-rec
  [dom cod ret]
  (a/mk-meta {:node :rec :domain dom :codomain cod :return ret}
           {:deputy true}))

(defn parse-rec
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as dom
   (parse benv (nth term 2)) :as cod
   (parse benv (nth term 3)) :as ret
   (mk-rec dom cod ret)))

(s/add-parse-pattern [(u/starts-with ::rec) parse-rec])

(defmethod @#'a/unparse-impl :rec unparse-impl-rec
  [mode node]
  (list 'rec
        (a/unparse mode (:domain node))
        (a/unparse mode (:codomain node))
        (a/unparse mode (:return node))))

(examples
 (a/unparse (s/parse (rec a b c)))
 => '(rec a b c))

(defmethod a/freevars :rec freevars-rec
  [node]
  (set/union (a/freevars (:domain node))
             (a/freevars (:codomain node))
             (a/freevars (:return node))))

(defmethod a/locally-closed-impl? :rec locally-closed-impl?-rec
  [idx node]
  (and (a/locally-closed-impl? idx (:domain node))
       (a/locally-closed-impl? idx (:codomain node))
       (a/locally-closed-impl? idx (:return node))))

(defmethod a/normal-form-canonical? :rec normal-form-canonical?-rec
  [node]
  (and (a/normal-form-canonical? (:domain node))
       (a/normal-form-canonical? (:codomain node))
       (a/normal-form-canonical? (:return node))))

(defmethod a/subst-impl :rec subst-impl-rec
  [expr idx node]
  (mk-rec (a/subst-impl expr idx (:domain node))
          (a/subst-impl expr idx (:codomain node))
          (a/subst-impl expr idx (:return node))))

(defmethod a/bind-impl :rec bind-impl-rec
  [name idx node]
  (mk-rec (a/bind-impl name idx (:domain node))
          (a/bind-impl name idx (:codomain node))
          (a/bind-impl name idx (:return node))))

(defmethod n/evaluate-impl :rec evaluate-impl-rec
  [node]
  (let [dom (n/evaluate-impl (:domain node))
        cod (n/evaluate-impl (:codomain node))
        ret (n/evaluate-impl (:return node))]
    (mk-rec dom cod ret)))

(defmethod n/alpha-equiv-impl [:rec :rec] alpha-equiv-impl-rec-rec
  [node1 node2]
  (and (n/alpha-equiv (:domain node1) (:domain node2))
       (n/alpha-equiv (:codomain node1) (:codomain node2))
       (n/alpha-equiv (:return node1) (:return node2))))

(defmethod @#'t/type-check-impl [:type :rec] type-check-impl-type-rec
  [ctx vtype term]
  (ok>
   (:domain term) :as dom
   (t/type-check ctx :type dom)
   (t/type-check ctx (n/evaluate (a/mk-pi dom  (a/mk-bind '_ :type))) (:codomain term))
   (t/type-check ctx :type (:return term))))

;; ****************************************************************************
;; *********************************** RET ***********************************
;; ****************************************************************************

(s/defparse ret ::ret)

(defn mk-ret
  [v]
  (a/mk-meta {:node :ret :value v}
           {:deputy true}))

(defn parse-ret
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as v
   (mk-ret v)))

(s/add-parse-pattern [(u/starts-with ::ret) parse-ret])

(defmethod @#'a/unparse-impl :ret unparse-impl-ret
  [mode node]
  (list 'ret
        (a/unparse mode (:value node))))

(examples
 (a/unparse (s/parse (ret a)))
 => '(ret a))

(defmethod a/freevars :ret freevars-ret
  [node]
  (a/freevars (:value node)))

(defmethod a/locally-closed-impl? :ret locally-closed-impl?-ret
  [idx node]
  (a/locally-closed-impl? idx (:value node)))

(defmethod a/normal-form-canonical? :ret normal-form-canonical?-ret
 [node]
  (a/normal-form-canonical? (:ret node)))

(defmethod a/subst-impl :ret subst-impl-ret
  [expr idx node]
  (mk-ret (a/subst-impl expr idx (:value node))))

(defmethod a/bind-impl :ret bind-impl-ret
  [name idx node]
  (mk-ret (a/bind-impl name idx (:value node))))

(defmethod n/evaluate-impl :ret evaluate-impl-ret
  [node]
  (mk-ret (n/evaluate-impl (:value node))))

(defmethod n/alpha-equiv-impl [:ret :ret] alpha-equiv-impl-ret-ret
  [node1 node2]
  (n/alpha-equiv (:value node1) (:value node2)))


(defmethod @#'t/type-check-impl [:rec :ret] type-check-impl-rec-ret
  [ctx vtype term]
  (ok>
   (t/type-check ctx (:return vtype) (:value term))
   [:ko "Type Checking Error : Term `term` does not have the type `vtype`." {:vtype (a/unparse vtype), :term (a/unparse term)}]))

;; ****************************************************************************
;; *********************************** RECUR***********************************
;; ****************************************************************************

(s/defparse rec-call ::rec-call)

(defn mk-rec-call
  [as k]
  (a/mk-meta {:node :rec-call :args as :kont k}
           {:deputy true}))

(defn parse-rec-call
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as as
   (parse benv (nth term 2)) :as k
   (mk-rec-call as k)))

(s/add-parse-pattern [(u/starts-with ::rec-call) parse-rec-call])

(defmethod @#'a/unparse-impl :rec-call unparse-impl-rec-call
  [mode node]
  (list 'rec-call
        (a/unparse mode (:args node))
        (a/unparse mode (:kont node))))

(examples
 (a/unparse (s/parse (rec-call a b)))
 => '(rec-call a b))

(defmethod a/freevars :rec-call freevars-rec-call
  [node]
  (set/union (a/freevars (:args node))
             (a/freevars (:kont node))))

(defmethod a/locally-closed-impl? :rec-call locally-closed-impl?-rec-call
  [idx node]
  (and (a/locally-closed-impl? idx (:args node))
       (a/locally-closed-impl? idx (:kont node))))

(defmethod a/normal-form-canonical? :rec-call normal-form-canonical?-rec-call
  [node]
  (and (a/normal-form-canonical? (:args node))
       (a/normal-form-canonical? (:kont node))))

(defmethod a/subst-impl :rec-call subst-impl-rec-call
  [expr idx node]
  (mk-rec-call (a/subst-impl expr idx (:args node))
            (a/subst-impl expr idx (:kont node))))

(defmethod a/bind-impl :rec-call bind-impl-rec-call
  [name idx node]
  (mk-rec-call (a/bind-impl name idx (:args node))
            (a/bind-impl name idx (:kont node))))

(defmethod n/evaluate-impl :rec-call evaluate-impl-rec-call
  [node]
  (mk-rec-call (n/evaluate-impl (:args node)) (n/evaluate-impl (:kont node))))

(defmethod n/alpha-equiv-impl [:rec-call :rec-call] alpha-equiv-impl-rec-call-rec-call
  [node1 node2]
  (and (n/alpha-equiv (:args node1) (:args node2))
       (n/alpha-equiv (:kont node1) (:kont node2))))

(defmethod @#'t/type-check-impl [:rec :rec-call] type-check-impl-rec-rec-call
  [ctx vtype term]
  (ok>
   (t/type-check ctx (:domain vtype) (:args term))
   (t/type-check ctx (n/evaluate (a/mk-pi (a/mk-app (:codomain vtype) (:args term)) (a/mk-bind '_  (mk-rec (:domain vtype) (:codomain vtype) (:return vtype))))) (:kont term))
   [:ko "Type Checking Error : Term `term` does not have the type `vtype`." {:vtype (a/unparse vtype), :term (a/unparse term)}]))

;; ****************************************************************************
;; *********************************** RED ***********************************
;; ****************************************************************************

(s/defparse red ::red)

(defn mk-red
  [dom cod ret src rec]
  (a/mk-meta {:node :red :domain dom :codomain cod :return ret :code src :rec rec }
           {:deputy true}))

(defn parse-red
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as dom
   (parse benv (nth term 2)) :as cod
   (parse benv (nth term 3)) :as ret
   (parse benv (nth term 4)) :as src
   (parse benv (nth term 5)) :as rec
   (mk-red dom cod ret src rec)))

(s/add-parse-pattern [(u/starts-with ::red) parse-red])

(defmethod @#'a/unparse-impl :red unparse-impl-red
  [mode node]
  (list 'red
        (a/unparse mode (:domain node))
        (a/unparse mode (:codomain node))
        (a/unparse mode (:return node))
        (a/unparse mode (:code node))
        (a/unparse mode (:rec node))))

(examples
 (a/unparse (s/parse (red a b c d e)))
 => '(red a b c d e))


(defmethod a/freevars :red freevars-red
  [node]
  (set/union (a/freevars (:domain node))
             (a/freevars (:codomain node))
             (a/freevars (:return node))
             (a/freevars (:code node))
             (a/freevars (:rec node))))

(defmethod a/locally-closed-impl? :red locally-closed-impl?-red
  [idx node]
  (and (a/locally-closed-impl? idx (:domain node))
       (a/locally-closed-impl? idx (:codomain node))
       (a/locally-closed-impl? idx (:return node))
       (a/locally-closed-impl? idx (:code node))
       (a/locally-closed-impl? idx (:rec node))))

(defmethod a/normal-form-neutral? :red normal-form-neutral?-red
  [node]
  (and (a/normal-form-canonical? (:domain node))
       (a/normal-form-canonical? (:codomain node))
       (a/normal-form-canonical? (:return node))
       (a/normal-form-canonical? (:code node))
       (a/normal-form-canonical? (:rec node))))

(defmethod a/subst-impl :red subst-impl-red
  [expr idx node]
  (mk-red (a/subst-impl expr idx (:domain node))
           (a/subst-impl expr idx (:codomain node))
           (a/subst-impl expr idx (:return node))
           (a/subst-impl expr idx (:code node))
           (a/subst-impl expr idx (:rec node))))

(defmethod a/bind-impl :red bind-impl-red
  [name idx node]
  (mk-red (a/bind-impl name idx (:domain node))
           (a/bind-impl name idx (:codomain node))
           (a/bind-impl name idx (:return node))
           (a/bind-impl name idx (:code node))
           (a/bind-impl name idx (:rec node))))

(defmethod n/evaluate-impl :red evaluate-impl-red
  [node]
  (loop [i (n/evaluate-impl (:code node))]
    (cond
      (= (:node i) :ret)
      (:value i)
      (= (:node i) :rec-call)
      (recur (n/evaluate-impl (a/mk-app (:kont i) (a/mk-app (:rec node) (:args i)))))
      :else
      (mk-red (n/evaluate-impl (:domain node))
              (n/evaluate-impl (:codomain node))
              (n/evaluate-impl (:return node))
              i
              (n/evaluate-impl (:rec node))))))

(defmethod n/alpha-equiv-impl [:red :red] alpha-equiv-impl-red-red
  [node1 node2]
  (and (n/alpha-equiv (:domain node1) (:domain node2))
       (n/alpha-equiv (:codomain node1) (:codomain node2))
       (n/alpha-equiv (:return node1) (:return node2))
       (n/alpha-equiv (:code node1) (:code node2))
       (n/alpha-equiv (:rec node1) (:rec node2))))

(defmethod t/type-synth-impl :red type-synth-impl-red
  [ctx term]
  (ok>
   (t/type-check ctx :type (:domain term))
   [:ko> "Type Synthesis Error : bad domain." {:labels (a/unparse (:domain term))}]

   (n/evaluate (a/mk-pi (:domain term) (a/mk-bind '_ :type))) :as vpi-dom-type
   (t/type-check ctx vpi-dom-type (:codomain term))
   [:ko> "Type Synthesis Error : bad codomain." {:labels (a/unparse (:domain term))}]

   (t/type-check ctx :type (:return term))
   [:ko> "Type Synthesis Error : bad return type." {:labels (a/unparse (:domain term))}]

   (n/evaluate (mk-rec (:domain term) (:codomain term) (:return term))) :as vrec
   (t/type-check ctx vrec (:code term))
   [:ko> "Type Synthesis Error : bad code." {:labels (a/unparse (:code term))}]

   (n/evaluate (a/mk-pi (:domain term) (a/mk-bind 'x (a/mk-app (:codomain term) (a/mk-bound 'x 0))))) :as vpi-dom-cod
   (t/type-check ctx vpi-dom-cod (:rec term))
   [:ko> "Type Synthesis Error : bad recursor." {:labels (a/unparse (:rec term))}]

   (n/evaluate (:return term))))

;; ****************************************************************************
;; *********************************** FIX ***********************************
;; ****************************************************************************

(s/defparse fix ::fix)

(defn mk-fix
  [dom cod init rec]
  (a/mk-meta {:node :fix :domain dom :codomain cod :init init :rec rec}
           {:deputy true}))

(defn parse-fix
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as dom
   (parse benv (nth term 2)) :as cod
   (parse benv (nth term 3)) :as init
   (parse benv (nth term 4)) :as rec
   (mk-fix dom cod init rec)))

(s/add-parse-pattern [(u/starts-with ::fix) parse-fix])

(defmethod @#'a/unparse-impl :fix unparse-impl-fix
  [mode node]
  (list 'fix
        (a/unparse mode (:domain node))
        (a/unparse mode (:codomain node))
        (a/unparse mode (:init node))
        (a/unparse mode (:rec node))))

(defmethod a/freevars :fix freevars-fix
  [node]
  (set/union (a/freevars (:domain node))
             (a/freevars (:codomain node))
             (a/freevars (:init node))
             (a/freevars (:rec node))))

(defmethod a/locally-closed-impl? :fix locally-closed-impl?-fix
  [idx node]
  (and (a/locally-closed-impl? idx (:domain node))
       (a/locally-closed-impl? idx (:codomain node))
       (a/locally-closed-impl? idx (:init node))
       (a/locally-closed-impl? idx (:rec node))))

(defmethod a/normal-form-neutral? :fix normal-form-neutral?-fix
  [node]
  (and (a/normal-form-canonical? (:domain node))
       (a/normal-form-canonical? (:codomain node))
       (a/normal-form-canonical? (:init node))
       (a/normal-form-canonical? (:rec node))))

(defmethod a/subst-impl :fix subst-impl-fix
  [expr idx node]
  (mk-fix (a/subst-impl expr idx (:domain node))
           (a/subst-impl expr idx (:codomain node))
           (a/subst-impl expr idx (:init node))
           (a/subst-impl expr idx (:rec node))))

(defmethod a/bind-impl :fix bind-impl-fix
  [name idx node]
  (mk-fix (a/bind-impl name idx (:domain node))
           (a/bind-impl name idx (:codomain node))
           (a/bind-impl name idx (:init node))
           (a/bind-impl name idx (:rec node))))

(defmethod n/alpha-equiv-impl [:fix :fix] alpha-equiv-impl-fix-fix
  [node1 node2]
  (and (n/alpha-equiv (:domain node1) (:domain node2))
       (n/alpha-equiv (:codomain node1) (:codomain node2))
       (n/alpha-equiv (:init node1) (:init node2))
       (n/alpha-equiv (:rec node1) (:rec node2))))

(defmethod n/evaluate-impl :fix evaluate-impl-fix
  [node]
  (let [i (n/evaluate-impl (:init node))]
    (cond
      (a/normal-form-neutral? i)
      (mk-fix (n/evaluate-impl (:domain node))
              (n/evaluate-impl (:codomain node))
              i
              (n/evaluate-impl (:rec node)))
      :else
      (n/evaluate-impl (mk-red (:domain node) (:codomain node)
                               (a/mk-app (:codomain node) (:init node))
                               (a/mk-app (:rec node) (:init node))
                               (a/mk-lam (a/mk-bind 'x (mk-fix (:domain node)
                                                               (:codomain node)
                                                               (a/mk-bound 'x 0)
                                                               (:rec node)))))))))

(defmethod t/type-synth-impl :fix type-synth-impl-fix
  [ctx term]
  (ok>
   (t/type-check ctx :type (:domain term))
   [:ko> "Type Synthesis Error : bad domain." {:labels (a/unparse (:domain term))}]

   (n/evaluate (a/mk-pi (:domain term) (a/mk-bind '_ :type))) :as vpi-dom-type
   (t/type-check ctx vpi-dom-type (:codomain term))
   [:ko> "Type Synthesis Error : bad codomain." {:labels (a/unparse (:domain term))}]

   (n/evaluate (:domain term)) :as vdom
   (t/type-check ctx vdom (:init term))
   [:ko> "Type Synthesis Error : bad initial value." {:labels (a/unparse (:init term))}]

   (n/evaluate (a/mk-pi (:domain term) (a/mk-bind 'x (mk-rec (:domain term) (:codomain term) (a/mk-app (:codomain term) (a/mk-bound 'x 0)))))) :as vpi-dom-rec
   (t/type-check ctx vpi-dom-rec (:rec term))
   [:ko> "Type Synthesis Error : bad recursor." {:labels (a/unparse (:rec term))}]

   (n/evaluate (a/mk-app (:codomain term) (:init term)))))


(t/set-computation! :fix)
