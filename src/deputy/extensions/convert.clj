(ns deputy.extensions.convert
  (:require 
   [clojure.set :as sets]
   [deputy.syntax :as s]
   [deputy.ast :as a]
   [deputy.utils :as u]
   [deputy.norm :as n]
   [deputy.typing :as t]))


(s/defparse convert! ::convert!)

(defn mk-convert!
  ([term from-ty to-ty] (mk-convert! term from-ty to-ty {}))
  ([term from-ty to-ty met]
   (a/mk-meta {:node ::convert!
               :from from-ty
               :to to-ty
               :term term}
              met)))

;;; Syntax:
;;; (convert! <from-ty>

(defn parse-convert!
  [parse benv term]
  (u/ok> (when (not= (count term) 6)
           [:ko "Wrong number of arguments for `convert!`" {:expected 6
                                                            :nbargs (count term)}])
         term :as [_ cterm fkw from-ty tkw to-ty]
         (parse benv cterm) :as cterm-ast
         [:ko> "Cannot parse `convert!` term" {:convert-term cterm}]
         (when-not (#{:from 'from} fkw)
           [:ko "Missing `:from` keyword in `convert!` form" {:from-kw fkw}])
         (parse benv from-ty) :as from-ty-ast
         [:ko> "Cannot parse from-type in `convert!` form" {:from-type from-ty}]
         (when-not (#{:to 'to} tkw)
         [:ko "Missing `:to` keyword in `convert!` form" {:to-kw tkw}])
         (parse benv to-ty) :as to-ty-ast
         [:ko> "Cannot parse to-type in `convert!` form" {:to-type to-ty}]
         (mk-convert! cterm-ast from-ty-ast to-ty-ast)))

(s/add-parse-pattern [(u/starts-with ::convert!) parse-convert!])

;;; AST methods

(defmethod @#'a/unparse-impl ::convert! unparse-impl-convert!
  [mode node]
  (list 'convert!
        (a/unparse mode (:term node))
        :from
        (a/unparse mode (:from node))
        :to
        (a/unparse mode (:to node))))

(defmethod a/freevars ::convert! freevars-convert!
  [node]
  (sets/union (a/freevars (:term node))
              (a/freevars (:from node))
              (a/freevars (:to node))))

(defmethod a/locally-closed-impl? ::convert! locally-closed-impl?-convert!
  [idx node]
  (and (a/locally-closed-impl? idx (:term node))
       (a/locally-closed-impl? idx (:from node))
       (a/locally-closed-impl? idx (:to node))))

(defmethod a/normal-form-neutral? ::convert! normal-form-neutral?-convert!
  [node]
  (a/normal-form-neutral? (:term node)))

(defmethod a/normal-form-canonical? ::convert! normal-form-canonical?-convert!
  [node]
  (a/normal-form-canonical? (:term node)))

(defmethod a/subst-impl ::convert! subst-impl-convert!
  [expr idx node]
  (mk-convert!  (a/subst-impl expr idx (:term node))
                (a/subst-impl expr idx (:from node))
                (a/subst-impl expr idx (:to node))
                (meta node)))

(defmethod a/bind-impl ::convert! bind-impl-convert!
  [name idx node]
  (mk-convert! (a/bind-impl name idx (:term node))
               (a/bind-impl name idx (:from node))
               (a/bind-impl name idx (:to node))
               (meta node)))

;;; evaluation

(defmethod n/evaluate-impl ::convert! evaluate-impl-convert!
  [node]
  (n/evaluate-impl (:term node)))

(t/set-computation! ::convert!)

(defmethod t/type-synth-impl ::convert! type-synth-impl-convert!
  [ctx term]
  (u/ok> (t/type-check ctx :type (:from term))
         [:ko> "From type of `convert!` term is not a type" {:from-type (:from term)}]
         (t/type-check ctx :type (:to term))
         [:ko> "To type of `convert!` term is not a type" {:to-type (:to term)}]
         (n/evaluate (:from term)) :as from-vty
         (n/evaluate (:to term)) :as to-vty
         (t/type-check ctx from-vty (:term term))
         [:ko> "Term of `convert!` has not the specified from-type" {:term (:term term)
                                                                     :from-type (:from term)}]
         ;; and we return the to-type, unchecked (hence the bang!)
         to-vty))




