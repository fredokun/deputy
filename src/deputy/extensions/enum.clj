(ns deputy.extensions.enum
  (:require
   [clojure.set :as set]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [ok>]]
   [deputy.syntax :as s]
   [deputy.norm :as n]
   [deputy.typing :as t]))

;; ****************************************************************************
;; *********************************** ENUM ***********************************
;; ****************************************************************************

(defn mk-enum
  ([labels] (mk-enum labels {}))
  ([labels met]
  (a/mk-meta {:node :enum
              :labels labels}
             met)))

(defn parse-enum
  [parse benv term]
  (ok>
   (parse benv (second term)) :as labels
   (mk-enum labels)))

(s/defparse enum ::enum)

(s/add-parse-pattern [(u/starts-with ::enum) parse-enum])

(defmethod @#'a/unparse-impl :enum unparse-impl-enum
  [mode node]
  (list 'enum (a/unparse mode (:labels node))))

(defmethod a/freevars :enum freevars-enum
  [node]
  (a/freevars (:labels node)))

(defmethod a/locally-closed-impl? :enum locally-closed-impl?-enum
  [idx node]
  (a/locally-closed-impl? idx (:labels node)))


(defmethod a/normal-form-canonical? :enum normal-form-canonical?-enum
  [node]
  (a/normal-form-canonical? (:labels node)))

(defmethod a/subst-impl :enum subst-impl-enum
  [expr idx node]
  (mk-enum (a/subst-impl expr idx (:labels node)) (meta node)))

(defmethod a/bind-impl :enum bind-impl-enum
  [name idx node]
  (mk-enum (a/bind-impl name idx (:labels node)) (meta node)))


(defmethod n/evaluate-impl :enum evaluate-impl-enum
  [node]
  (let [labels (n/evaluate-impl (:labels node))]
    (mk-enum labels (meta node))))

(defmethod n/alpha-equiv-impl [:enum :enum] alpha-equiv-impl-enum-enum
  [node1 node2]
  (n/alpha-equiv (:labels node1) (:labels node2)))

(defmethod @#'t/type-check-impl [:type :enum] type-check-impl-type-enum
  [ctx _ term]
  (ok>
   (t/type-check ctx :labels (:labels term))
   [:ko> "Type Check Error : invalid argument for enum." {:term (a/unparse term)}]))

;; ****************************************************************************
;; *********************************** INDEX **********************************
;; ****************************************************************************

(s/defparse index ::index)
(s/defparse idx ::index)
(s/defparse succ ::succ)

(defn mk-succ
  ([idx] (mk-succ idx {}))
  ([idx met]
   (a/mk-meta {:node :succ
               :idx idx}
              met)))

(defn parse-idx
  [_ _ term]
  (loop [n (second term)
         succ 'zero]
    (if-not (zero? n)
      (recur (dec n) (mk-succ succ))
      succ)))

(defn parse-succ
  [parse benv term]
  (ok>
   (parse benv (second term)) :as idx
   (mk-succ idx)))


(s/add-parse-pattern [(u/starts-with ::index) parse-idx]
                     [(u/is? 'zero) s/parse-const]
                     [(u/starts-with ::succ) parse-succ])


(defmethod @#'a/unparse-impl :succ unparse-impl-succ
  [mode node]
  (list 'succ (a/unparse mode (:idx node))))


(defmethod a/freevars :succ freevars-succ
  [node]
  (a/freevars (:idx node)))

(defmethod a/locally-closed-impl? :succ locally-closed-impl?-succ
  [idx node]
  (a/locally-closed-impl? idx (:idx node)))


(defmethod a/normal-form-neutral? :succ normal-form-neutral?-succ
  [node]
  (a/normal-form-neutral? (:idx node)))


(defmethod a/normal-form-canonical? :succ normal-form-canonical?-succ
  [node]
  (a/normal-form-canonical? (:idx node)))

(defmethod a/subst-impl :succ subst-impl-succ
  [expr idx node]
  (mk-succ (a/subst-impl expr idx (:idx node)) (meta node)))

(defmethod a/bind-impl :succ bind-impl-succ
  [name idx node]
  (mk-succ (a/bind-impl name idx (:idx node)) (meta node)))

(defmethod n/alpha-equiv-impl :succ alpha-equiv-impl-succ
  [node1 node2]
  (n/alpha-equiv (:idx node1) (:idx node2)))

(defmethod n/evaluate-impl :succ evaluate-impl-succ
  [node]
  (mk-succ (n/evaluate-impl (:idx node)) (meta node)))


;; ****************************************************************************
;; *********************************** INDEX ENUM *****************************
;; ****************************************************************************



(defmethod @#'t/type-check-impl [:enum 'zero] type-check-impl-enum-zero
  [_ vtype term]
  (if (= (:node (:labels vtype)) :tags)
    true
    [:ko "Type Checking Error : Term `term` does not have the type `vtype`."
     {:vtype (a/unparse vtype), :term (a/unparse term)}]))

(defmethod @#'t/type-check-impl [:enum :succ] type-check-impl-enum-succ
  [ctx vtype term]
  (if (= (:node (:labels vtype)) :tags)
    (t/type-check ctx (mk-enum (:labels (:labels vtype))) (:idx term))
    [:ko "Type Checking Error : Term `term` does not have the type `vtype`."
     {:vtype (a/unparse vtype), :term (a/unparse term)}]))


;; ****************************************************************************
;; *********************************** STRUCT *********************************
;; ****************************************************************************

(s/defparse struct ::struct)

(defn mk-struct
  ([labels return] (mk-struct labels return {}))
  ([labels return met]
   (a/mk-meta {:node :struct
               :labels labels
               :return return}
              met)))

(defn parse-struct
  [parse benv term]
  (ok>
   (when-not (= (count term) 6)
     [:ko "Wrong arity for `struct` form" {:term term}])
   (parse benv (second term)) :as labels
   (when-not (#{'as :as} (nth term 2))
     [:ko "Missing `as` or `:as` keyword in `struct` form." {:term term}])
   (nth term 3) :as name
   (when-not (symbol? name)
     [:ko "Name of `struct` return should be a symbol." {:term term, :name name}])
   (when-not (#{'return :return 'ret :ret} (nth term 4))
     [:ko "Missing `return` (`:return`, `ret` or `:ret`) keyword in `struct` form." {:term term}])
   (parse (cons name benv) (nth term 5)) :as return
   (mk-struct labels (a/mk-bind name return))))

(s/add-parse-pattern [(u/starts-with ::struct) parse-struct])

(defmethod @#'a/unparse-impl :struct unparse-impl-struct
  [mode node]
  (let [labels (a/unparse mode (:labels node))
        return (a/unparse mode (:body (:return node)))]
    (if (= mode :index)
      (list 'struct labels 'return return)
      (list 'struct labels 'as (:name (:return node)) 'return return))))

(defmethod a/freevars :struct freevars-struct
  [node]
  (set/union (a/freevars (:labels node))
             (a/freevars (:body (:return node)))))

(defmethod a/locally-closed-impl? :struct locally-closed-impl?-struct
  [idx node]
  (and (a/locally-closed-impl? idx (:labels node))
       (a/locally-closed-impl? idx (:return node))))

(defmethod a/normal-form-neutral? :struct normal-form-neutral?-struct
  [node]
  (and (a/normal-form-neutral? (:labels node))
       (a/normal-form-canonical? (:return node))))

(defmethod a/subst-impl :struct subst-impl-struct
  [expr idx node]
  (mk-struct (a/subst-impl expr idx (:labels node))
             (a/subst-impl expr idx (:return node))
             (meta node)))

(defmethod a/bind-impl :struct bind-impl-struct
  [name idx node]
  (mk-struct (a/bind-impl name idx (:labels node))
             (a/bind-impl name idx (:return node))
             (meta node)))

(defmethod n/evaluate-impl :struct evaluate-impl-struct
  [node]
  ;; (struct labels B)
  (let [labels-norm (n/evaluate-impl (:labels node))]
    (case (u/node-type labels-norm)
      ;; empty labels
      nil :unit

      ;; cons labels [l ls]
      :tags (let
             ;; A (normalized)
             [first-norm (n/evaluate-impl (a/subst 'zero (:body (:return node))))
              ;; ls (normalized)
              remaining-labels-norm (:labels labels-norm)
              ;; B [(succ x) / x] (not normalized)
              name (s/gen-free-name (:name (:return node)) (a/freevars (:body (:return node))))
              remaining-types (a/mk-bind name (a/bind name (n/evaluate-impl (a/subst (mk-succ (a/mk-free name))
                                                                                     (:body (:return node))))))
              ;; (struct ls B [(succ x) / x]) (not normalized)
              remaining-struct (mk-struct remaining-labels-norm
                                          remaining-types
                                          (meta node))]
              ;; (Σ A (struct ls B [(succ x) / x])) (normalized)
              (n/evaluate-impl (a/mk-sig first-norm (a/mk-bind '_ remaining-struct))))
      ;; else
      (do
        (assert (a/normal-form-neutral? labels-norm))
        (let
        ;; B (normalized)
         [name (s/gen-free-name (:name (:return node)) (a/freevars (:body (:return node))))
          return-norm (a/mk-bind name (a/bind name (n/evaluate-impl (a/subst (a/mk-free name) (:body (:return node))))))]
         ;; (struct labels B) (normalized)
          (mk-struct labels-norm return-norm (meta node)))))))

(defmethod n/alpha-equiv-impl :struct alpha-equiv-impl-struct
  [node1 node2]
  (and (n/alpha-equiv (:labels node1) (:labels node2))
       (n/alpha-equiv (:return node1) (:return node2))))

(defmethod t/type-synth-impl :struct type-synth-impl-struct
  [ctx term]
  (ok>
   (:labels term) :as t
   (t/type-check ctx :labels t)
   [:ko> "Type Synthesis Error : bad labels." {:labels (a/unparse t)}]

   (s/gen-free-name (:name (:return term)) ctx) :as name
   (a/mk-free name) :as x
   (assoc ctx name (mk-enum t)) :as new-ctx

   (:body (:return term)) :as T
   (t/type-check new-ctx :type (a/subst (a/mk-free name) T))
   [:ko> "Type Synthesis Error : bad motive." {:labels (a/unparse T)}]
   :type))

(t/set-computation! :struct)

;; ****************************************************************************
;; *********************************** SWITCH *********************************
;; ****************************************************************************

(s/defparse switch ::switch)

(defn mk-switch
  ([idx name return pattern] (mk-switch idx (a/mk-bind name return) pattern))
  ([idx return pattern]
   (a/mk-meta {:node :switch
            :idx idx
            :return return
            :pattern pattern} {:deputy true})))

(defn parse-switch
  [parse benv term]
  (ok>
   (when-not (= (count term) 8)
     [:ko "Wrong arity for `switch` form" {:term term}])
   (parse benv (second term)) :as idx
   (when-not (#{'as :as} (nth term 2))
     [:ko "Missing `as` or `:as` keyword in `switch` form." {:term term}])
   (nth term 3) :as name
   (when-not (symbol? name)
     [:ko "Name of `switch` return should be a symbol." {:term term, :name name}])
   (when-not (#{'return :return 'ret :ret} (nth term 4))
     [:ko "Missing `return` (`:return`, `ret` or `:ret`) keyword in `switch` form." {:term term}])
   (parse (cons name benv) (nth term 5)) :as return
   (when-not (#{'with :with} (nth term 6))
     [:ko "Missing `with` or `:with` keyword in `switch` form." {:term term}])
   (parse benv (nth term 7)) :as pattern
   (mk-switch idx name return pattern)))

(s/add-parse-pattern [(u/starts-with ::switch) parse-switch])

(defmethod @#'a/unparse-impl :switch unparse-impl-switch
  [mode node]
  (let [idx (a/unparse mode (:idx node))
        return (a/unparse mode (:body (:return node)))
        with (a/unparse mode (:pattern node))]
    (if (= mode :index)
      (list 'switch idx 'return return 'with with)
      (list 'switch idx 'as (:name (:return node)) 'return return 'with with))))


(defmethod a/freevars :switch freevars-switch
  [node]
  (set/union (a/freevars (:idx node))
             (a/freevars (:body (:return node)))
             (a/freevars (:pattern node))))

(defmethod a/locally-closed-impl? :switch locally-closed-impl?-switch
  [idx node]
  (and (a/locally-closed-impl? idx (:idx node))
       (a/locally-closed-impl? idx (:return node))
       (a/locally-closed-impl? idx (:pattern node))))

(defmethod a/normal-form-neutral? :switch normal-form-neutral?-switch
  [node]
  (and (a/normal-form-neutral? (:idx node))
       (a/normal-form-canonical? (:return node))
       (a/normal-form-canonical? (:pattern node))))

(defmethod a/subst-impl :switch subst-impl-switch
  [expr idx node]
  (mk-switch (a/subst-impl expr idx (:idx node))
             (a/subst-impl expr idx (:return node))
             (a/subst-impl expr idx (:pattern node))))

(defmethod a/bind-impl :switch bind-impl-switch
  [name idx node]
  (mk-switch (a/bind-impl name idx (:idx node))
           (a/bind-impl name idx (:return node))
           (a/bind-impl name idx (:pattern node))))

(defmethod n/evaluate-impl :switch evaluate-impl-switch
  [node]
  ;; (switch idx B cs)
  (let
   [idx-norm (n/evaluate-impl (:idx node))]
    (case (u/node-type idx-norm)
      ;; 0
      ;; (π1 cs) (normalized)
      'zero (n/evaluate-impl (a/mk-proj :left (:pattern node)))

      ;; (succ n)
      :succ (let
             [name (s/gen-free-name (:name (:return node)) (a/freevars (:body (:return node))))
              ;; B[(succ x) / x] (normalized)
              remaining-types (a/mk-bind name (a/bind name (n/evaluate-impl
                                                            (a/subst (mk-succ (a/mk-free name)) (:body (:return node))))))
              ;; (π2 cs)
              new-cs (n/evaluate-impl (a/mk-proj :right (:pattern node)))
              ;; n (normalized)
              n (:idx idx-norm)]
              ;; (switch n B[(succ x) / x] (π2 cs)) (normalized)
              (n/evaluate-impl (mk-switch n remaining-types new-cs)))
      ;; else
      (do
        (assert (a/normal-form-neutral? idx-norm))
        (let
          ;; B (normalized)
         [name (s/gen-free-name (:name (:return node)) (a/freevars (:body (:return node))))
          return-norm (a/mk-bind name (a/bind name (n/evaluate-impl (a/subst (a/mk-free name) (:body (:return node))))))
          ;; cs (normalized)
          new-cs (n/evaluate-impl (:pattern node))]
          ;; (switch idx B cs) (normalized)
          (mk-switch idx-norm return-norm new-cs))))))


(defmethod n/alpha-equiv-impl :switch alpha-equiv-impl-switch
  [node1 node2]
  (and (n/alpha-equiv (:idx node1) (:idx node2))
       (n/alpha-equiv (:return node1) (:return node2))
       (n/alpha-equiv (:pattern node1) (:pattern node2))))

(defmethod t/type-synth-impl :switch type-synth-impl-switch
  [ctx term]
  (ok>
   (:idx term) :as ne
   (t/type-synth-impl ctx ne) :as netype
   [:ko> "Type Synthesis Error : Cannot synthesize type of left hand-side (operator) of a switch."
    {:operator (a/unparse ne)}]

   (when-not (= (:node netype) :enum)
     [:ko "Type Synthesis Error : Left hand-side (operator) of application must be an enum."
      {:operator (a/unparse ne)
       :type (a/unparse netype)}])

   (:labels netype) :as vl

   (s/gen-free-name (:name (:return term)) ctx) :as name
   (a/mk-free name) :as x
   (assoc ctx name netype) :as new-ctx

   (:body (:return term)) :as T

   (t/type-check new-ctx :type (a/subst (a/mk-free name) T))
   [:ko> "Type Synthesis Error : bad motive." {:labels (a/unparse T)}]

   (:pattern term) :as cs
   (n/evaluate (mk-struct vl (a/mk-bind (:name (:return term)) T))) :as struct
   (t/type-check ctx struct cs)
   [:ko> "Type Synthesis Error : bad switchs" {:switchs (a/unparse cs)
                                             :expected (a/unparse struct)}]

   (n/evaluate (a/subst (n/evaluate ne) T))))

(t/set-computation! :switch)
