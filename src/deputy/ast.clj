(ns deputy.ast
  "Abstract Syntax Tree (AST) representation
  and related operations"
  (:require [clojure.set :as set]
            [deputy.utils :as u]
            [deputy.unparse :as up]))

;; ************************************************************************************
;; *********************************** CONSTRUCTORS ***********************************
;; ************************************************************************************

(defn mk-meta
  "Decorate the AST node with `met`adata"
  [node met]
  ;; (println "[mk-meta]: node = " node) 
  (if (u/has-meta? node)
    (let [nmet (merge (or (meta node) {:deputy true}) met)] 
      (with-meta node nmet))
    node))

(defn mk-const
  "Build a constant AST node.
Remark: a constant node cannot be associated to metadata."
  [value] value)

(defn mk-annot
  ([type term] (mk-annot type term {}))
  ([type term met] 
   (mk-meta {:node :annotation
             :type type
             :term term}
            met)))

(defn mk-app
  ([rator rand] (mk-app rator rand {}))
  ([rator rand met]
  (mk-meta {:node :application
            :rator rator
            :rand rand}
           met)))

(defn mk-bound
  ([name level] (mk-bound name level {}))
  ([name level met]
   (mk-meta {:node :bound-var
             :name name
             :level level}
           met)))

(defn mk-free
  ([name] (mk-free name {}))
  ([name met]
   (mk-meta {:node :free-var
             :name name}
            met)))

(defn mk-ref
  ([ref] (mk-ref ref {}))
  ([ref met]
   (mk-meta {:node ::ref
             :ref ref}
            met)))

(defn unref [node]
  (assert (= (:node node) :deputy.ast/ref))
  @(:ref node))

(defn mk-hole
  ([hole-name params] (mk-hole hole-name params {}))
  ([hole-name params met]
   (mk-meta {:node ::hole
             :name hole-name
             :params params}
            met)))

(defn mk-const
  "Build a constant AST node.
Remark : constant nodes cannot be associated to metadata."
  [value]
  value)

(defn mk-bind
  ([name body] (mk-bind name body {}))
  ([name body met]
   (mk-meta {:node :bind
             :name name
             :body body}
           met)))

(defn mk-lam
  ([content] (mk-lam content {}))
  ([content met]
  (mk-meta {:node :lambda
            :content content}
           met)))

(defn mk-pair
  ([fst scd] (mk-pair fst scd {}))
  ([fst scd met]
   (mk-meta {:node :pair
             :first fst
             :second scd}
            met)))

(defn mk-sig
  ([fst snd] (mk-sig fst snd {}))
  ([fst snd met ] 
   (mk-meta {:node :sig :first fst :second snd}
            met)))

(defn mk-proj
  ([left-right node] (mk-proj left-right node {}))
  ([left-right node met]
   (mk-meta {:node :proj :take left-right :pair node}
            met)))

(defn mk-pi
  ([dom cod] (mk-pi dom cod {}))
  ([dom cod met] 
   (mk-meta {:node :pi :domain dom :codomain cod}
            met)))

;; ************************************************************************************
;; *********************************** METHODS ****************************************
;; ************************************************************************************

(defmulti freevars
  "Returns the set of free variables used in the term `node`."
  (fn [node] (:node node)))

(defmulti ^:private unparse-impl
  "Returns a syntax that the parser can read, or a better syntax for debugging, using the default mode or a more specific mode."
  (fn [mode node] (:node node)))

(defn unparse
  "Unparse as a term the provided AST `node`, with
  optional unparsing `mode` option (or options set)"
  ([node] (unparse #{} node))
  ([mode node] 
   (if (keyword? mode)
     (recur #{mode} node)
     (if-let [id (get (meta node) :ref-id)]
       (up/apply-unparse-patterns id (unparse-impl mode node))
       (if-let [rid (get (meta node) :deputy.certified/nf-def-id)]
         rid
         (unparse-impl mode node))))))

(defmulti locally-closed-impl?
  "Checks if the term node is locally-closed at the index `idx`."
  (fn [idx node] (:node node)))

(defn locally-closed?
  "Checks if the term node is locally-closed at the index `idx`.
   Uses `0` by default."
  ([node] (locally-closed-impl? 0 node))
  ([idx node] (locally-closed-impl? idx node)))

(defmulti normal-form-neutral?
  "Checks if the term `node` is in normal neutral form."
  (fn [node] (:node node)))

(defmulti normal-form-canonical?
  "Checks if the term `node` is in normal canonical form."
  (fn [node] (:node node)))

(defn normal-form?
  "Checks if the term `node` is in beta-normal form."
  [node]
  {:pre [(true? (locally-closed? node) "Term must be locally-closed.")]}
  (normal-form-canonical? node))

(defmulti subst-impl
  "Substitutes in term `node` all occurrences of bound variable (i.e. `#{idx}`) by
  (locally-closed) term `expr` at level `idx` (depth of binder)."
  (fn [expr idx node] (:node node)))

(defn subst
  "Substitutes in term `node` all occurrences of bound variable (i.e. `#{idx}`) by
  (locally-closed) term `expr` at level `idx` (depth of binder).
   Uses `0` as default depth."
  ([expr node] (subst expr 0 node))
  ([expr idx node] (subst-impl expr idx node)))

(defmulti bind-impl
  "Bind free variable `name` in term `node` to index `#{idx}`."
  (fn [name idx node] (:node node)))

(defn bind
  "Bind free variable `name` in term `node` to index `#{idx}`.
   Uses `0` by default."
  ([name term]
   ;;  {:pre  [(true? (locally-closed? term)
   ;;              "input term must be locally-closed")]
   ;;   :post [(true? (not (contains? (freevars %) name))
   ;;              "input variable cannot be free in the resulting term")
   ;;          (true? (locally-closed? 1 %)
   ;;              "resulting term is locally-closed above index 0")]}
   (bind name 0 term))
  ([name idx term] (bind-impl name idx term)))


;; *******************************************************************************
;; *********************************** DEFAULT ***********************************
;; *******************************************************************************


(defmethod freevars :default freevars-default
  [_]
  #{})

(defmethod unparse-impl :default unparse-impl-default
  [_ node]
  (throw (ex-info "Cannot unparse the `term`." {:term node})))

(defmethod locally-closed-impl? :default locally-closed-impl?-default
  [_ _]
  true)

(defmethod normal-form-neutral? :default normal-form-neutral?-default
  [_]
  false)

(defmethod normal-form-canonical? :default normal-form-canonical?-default
  [node]
  (normal-form-neutral? node))

(defmethod subst-impl :default subst-impl-default
  [_ _ node]
  node)

(defmethod bind-impl :default bind-impl-default
  [_ _ node]
  node)



;; **********************************************************************************
;; *********************************** ANNOTATION ***********************************
;; **********************************************************************************


(defmethod unparse-impl :annotation unparse-impl-annotation
  [mode node]
  (list 'the
        (unparse mode (:type node))
        (unparse mode (:term node))))

(defmethod freevars :annotation freevars-annotation
  [node]
  (set/union (freevars (:type node))
             (freevars (:term node))))

(defmethod locally-closed-impl? :annotation locally-closed-impl?-annotation
  [idx node]
  (and (locally-closed-impl? idx (:type node))
       (locally-closed-impl? idx (:term node))))

(defmethod subst-impl :annotation subst-impl-annotation
  [expr idx node]
  (mk-annot (subst-impl expr idx (:type node))
            (subst-impl expr idx (:term node))
            (meta node)))

(defmethod bind-impl :annotation bind-impl-annotation
  [name idx node]
  (mk-annot (bind-impl name idx (:type node))
            (bind-impl name idx (:term node))
            (meta node)))

;; ***********************************************************************************
;; *********************************** APPLICATION ***********************************
;; ***********************************************************************************


(defmethod unparse-impl :application unparse-impl-application
  [mode node]
  (list (unparse mode (:rator node))
        (unparse mode (:rand node))))

(defmethod freevars :application freevars-application
  [node]
  (set/union (freevars (:rator node))
             (freevars (:rand node))))

(defmethod locally-closed-impl? :application locally-closed-impl?-application
  [idx node]
  (and (locally-closed-impl? idx (:rator node))
       (locally-closed-impl? idx (:rand node))))

(defmethod normal-form-neutral? :application  normal-form-neutral?-application
  [node]
  (and (normal-form-neutral? (:rator node))
       (normal-form-canonical? (:rand node))))

(defmethod subst-impl :application subst-impl-application
  [expr idx node]
  (mk-app (subst-impl expr idx (:rator node))
          (subst-impl expr idx (:rand node))
          (meta node)))

(defmethod bind-impl :application bind-impl-application
  [name idx node]
  (mk-app (bind-impl name idx (:rator node))
          (bind-impl name idx (:rand node))
          (meta node)))


;; *********************************************************************************
;; *********************************** BOUND VAR ***********************************
;; *********************************************************************************


(defmethod unparse-impl :bound-var unparse-impl-bound-var
  [mode node]
  (if (contains? mode :index)
    #{(:level node)}
    (:name node)))

(defmethod locally-closed-impl? :bound-var locally-closed-impl?-bound-var
  [idx node]
  (< (:level node) idx))

(defmethod normal-form-neutral? :bound-var normal-form-neutral?-bound-var
  [_]
  true)

(defmethod subst-impl :bound-var subst-impl-bound-var
  [expr idx node]
  (if (= (:level node) idx)
    expr
    node))


;; ********************************************************************************
;; *********************************** FREE VAR ***********************************
;; ********************************************************************************


(defmethod unparse-impl :free-var unparse-impl-free-var
  [_ node]
  (:name node))

(defmethod freevars :free-var freevars-free-var
  [node]
  #{(:name node)})

(defmethod normal-form-neutral? :free-var normal-form-neutral?-free-var
  [_]
  true)

(defmethod bind-impl :free-var bind-impl-free-var
  [name idx node]
  (if (= (:name node) name)
    (mk-bound name idx (meta node))
    node))

;; *******************************************************************************
;; ************************************ HOLES ************************************
;; *******************************************************************************


(defmethod freevars ::hole freevars-hole
  [node]
  #{})
  
(defmethod unparse-impl ::hole unparse-impl-hole
  [_ node]
  (if (seq (:params node))
    (list* (:name node) (:params node))
    (:name node)))

(defmethod locally-closed-impl? ::hole locally-closed-impl?-hole
  [_ _]
  true)

(defmethod normal-form-neutral? ::hole normal-form-neutral?-hole
  [node]
  true)

(defmethod normal-form-canonical? ::hole normal-form-canonical?-hole
  [node]
  true)

(defmethod subst-impl ::hole subst-impl-hole
  [_ _ node]
  node)

(defmethod bind-impl ::hole bind-impl-hole
  [_ _ node]
  node)

;; *******************************************************************************
;; ********************************* REFERENCES **********************************
;; *******************************************************************************

(defmethod freevars ::ref freevars-ref
  [node]
  ;; XXX: referenced terms should be closed (?)
  (freevars (unref node)))

(defmethod unparse-impl ::ref unparse-impl-ref
  [mode node]
  ;; TODO: allow opaque references (?)
  ;; in this case, we need to reinject the identifier
  #_(let [m (meta (:ref node))]
      (symbol (name (ns-name (:ns m))) (name (:name m))))
  (unparse mode (unref node)))

(defmethod locally-closed-impl? ::ref locally-closed-impl?-ref
  [idx node]
  ;; XXX: referenced terms should be closed (?)
  (locally-closed-impl? idx (unref node)))

(defmethod normal-form-neutral? ::ref normal-form-neutral?-ref
  [node]
  (normal-form-neutral? (unref node)))

(defmethod normal-form-canonical? ::ref normal-form-canonical?-ref
  [node]
  (normal-form-canonical? (unref node)))

(defmethod subst-impl ::ref subst-impl-ref
  [expr idx node]
  ;; XXX: do *not* substitute within references (no freevars)
  ;;(subst-impl expr idx (unref node))
  node)

(defmethod bind-impl ::ref bind-impl-ref
  [name idx node]
  ;; XXX: do *not* bind references (no freevars)
  ;;(bind-impl name idx (unref node))
  node)

;; ********************************************************************************
;; *********************************** CONSTANT ***********************************
;; ********************************************************************************


(defmethod unparse-impl nil  unparse-impl-nil
  [_ node]
  node)

(defmethod normal-form-canonical? nil normal-form-canonical?-nil
  [_]
  true)

;; ******************************************************************************
;; *********************************** LAMBDA ***********************************
;; ******************************************************************************


(defmethod unparse-impl :lambda unparse-impl-lambda
  [mode node]
  (let [{name :name body :body} (:content node)]
    (if (contains? mode :index)
      (list 'λ (unparse mode body))
      (list 'λ [name] (unparse mode body)))))

(defmethod freevars :lambda  freevars-lambda
  [node]
  (freevars (:content node)))

(defmethod locally-closed-impl? :lambda locally-closed-impl?-lambda
  [idx node]
  (locally-closed-impl? idx (:content node)))

(defmethod normal-form-canonical? :lambda normal-form-canonical?-lambda
  [node]
  (normal-form-canonical? (:content node)))

(defmethod subst-impl :lambda subst-impl-lambda
  [expr idx node]
  (mk-lam (subst-impl expr idx (:content node)) (meta node)))

(defmethod bind-impl :lambda bind-impl-lambda
  [name idx node]
  (mk-lam (bind-impl name idx (:content node)) (meta node)))

;; ******************************************************************************
;; *********************************** BIND *************************************
;; ******************************************************************************

(defmethod unparse-impl :bind unparse-impl-bind
  [_ node]
  (throw (ex-info "Cannot unparse `bind` form directly." {:node node})))

(defmethod freevars :bind freevars-bind
  [node]
  (freevars (:body node)))

(defmethod locally-closed-impl? :bind locally-closed-impl?-bind
  [idx node]
  (locally-closed-impl? (inc idx) (:body node)))

(defmethod normal-form-canonical? :bind normal-form-canonical?-bind
  [node]
  (normal-form-canonical? (:body node)))

(defmethod subst-impl :bind subst-impl-bind
  [expr idx node]
  (mk-bind (:name node)
           (subst-impl expr (inc idx) (:body node))
           (meta node)))

(defmethod bind-impl :bind bind-impl-bind
  [name idx node]
  (mk-bind (:name node)
           (bind-impl name (inc idx) (:body node))
           (meta node)))


;; ****************************************************************************
;; *********************************** PAIR ***********************************
;; ****************************************************************************

(defmethod unparse-impl :pair unparse-impl-pair
  [mode node]
  [(unparse mode (:first node))
   (unparse mode (:second node))])

(defmethod freevars :pair freevars-pair
  [node]
  (set/union (freevars (:first node))
             (freevars (:second node))))

(defmethod locally-closed-impl? :pair locally-closed-impl?-pair
  [idx node]
  (and (locally-closed-impl? idx (:first node))
       (locally-closed-impl? idx (:second node))))

(defmethod normal-form-canonical? :pair normal-form-canonical?-pair
  [node]
  (and (normal-form-canonical? (:first node))
       (normal-form-canonical? (:second node))))

(defmethod subst-impl :pair subst-impl-pair
  [expr idx node]
  (mk-pair (subst-impl expr idx (:first node))
           (subst-impl expr idx (:second node))
           (meta node)))

(defmethod bind-impl :pair bind-impl-pair
  [name idx node]
  (mk-pair (bind-impl name idx (:first node))
           (bind-impl name idx (:second node))
           (meta node)))



;; ***************************************************************************
;; *********************************** SIG ***********************************
;; ***************************************************************************


(defmethod unparse-impl :sig unparse-impl-sig
  [mode node]
  (let [{first :first, second :second} node
        {name :name, body :body} second]
    (if (contains? mode :index)
      (list 'Σ (unparse mode first) (unparse mode body))
      (list 'Σ [name (unparse mode first)] (unparse mode body)))))

(defmethod freevars :sig freevars-sig
  [node]
  (set/union (freevars (:first node))
             (freevars (:second node))))

(defmethod locally-closed-impl? :sig locally-closed-impl?-sig
  [idx node]
  (and (locally-closed-impl? idx (:first node))
       (locally-closed-impl? idx (:second node))))

(defmethod normal-form-canonical? :sig normal-form-canonical?-sig
  [node]
  (and (normal-form-canonical? (:first node))
       (normal-form-canonical? (:second node))))

(defmethod subst-impl :sig subst-impl-sig
  [expr idx node]
  (mk-sig
   (subst-impl expr idx (:first node))
   (subst-impl expr idx (:second node))
   (meta node)))

(defmethod bind-impl :sig bind-impl-sig
  [name idx node]
  (mk-sig
   (bind-impl name idx (:first node))
   (bind-impl name idx (:second node))
   (meta node)))

; ****************************************************************************
;; *********************************** PROJ ***********************************
;; ****************************************************************************

(defmethod unparse-impl :proj unparse-impl-proj
  [mode node]
  (list
   (case (:take node)
     :left 'π1
     :right 'π2)
   (unparse mode (:pair node))))

(defmethod freevars :proj freevars-proj
  [node]
  (freevars (:pair node)))

(defmethod locally-closed-impl? :proj locally-closed-impl?-proj
  [idx node]
  (locally-closed-impl? idx (:pair node)))

(defmethod normal-form-neutral? :proj normal-form-neutral?-proj
  [node]
  (normal-form-neutral? (:pair node)))

(defmethod subst-impl :proj subst-impl-proj
  [expr idx node]
  (mk-proj (:take node)
           (subst-impl expr idx (:pair node))
           (meta node)))

(defmethod bind-impl :proj bind-impl-proj
  [name idx node]
  (mk-proj (:take node)
           (bind-impl name idx (:pair node))
           (meta node)))

;; **************************************************************************
;; *********************************** PI ***********************************
;; **************************************************************************

(defmethod unparse-impl :pi unparse-impl-pi
  [mode node]
  (let [{domain :domain, codomain :codomain} node
        {name :name, body :body} codomain]
    (list 'Π
          (if (contains? mode :index)
            (unparse mode domain)
            [name (unparse mode domain)])
          (unparse mode body))))

(defmethod freevars :pi freevars-pi
  [node]
  (set/union (freevars (:domain node))
             (freevars (:codomain node))))

(defmethod locally-closed-impl? :pi locally-closed-impl?-pi
  [idx node]
  (and (locally-closed-impl? idx (:domain node))
       (locally-closed-impl? idx (:codomain node))))

(defmethod normal-form-canonical? :pi normal-form-canonical?-pi
  [node]
  (and (normal-form-canonical? (:domain node))
       (normal-form-canonical? (:codomain node))))

(defmethod subst-impl :pi subst-impl-pi
  [expr idx node]
  (mk-pi (subst-impl expr idx (:domain node))
         (subst-impl expr idx (:codomain node))
         (meta node)))

(defmethod bind-impl :pi bind-impl-pi
  [name idx node]
  (mk-pi (bind-impl name idx (:domain node))
         (bind-impl name idx (:codomain node))
         (meta node)))


