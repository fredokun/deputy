(ns deputy.syntax
  (:require
   [clojure.walk :as w]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [example examples ok>]]))

(def ^:private +examples-enabled+ false)

(def ^:dynamic *resolve-identifiers* 
  "This dynamic variable controls the interpretation of
symbols not bound in terms. If `false` any uninterpreted
symbol becomes an occurrence of a free variable. If `true` the clojure resolver
is used to fetch a clojure var bound to a deputy term. If no
such var exists, or it is not bound to a deputy term (which is known
through metadata) then a parse error is returned."
  true)

(def ^:dynamic *allow-free-variables* false)

(def ^:dynamic *holes-enabled*
  "This dynamic variable controls the support for term holes."
  false)

;;; Free (fresh) name generation

(defn mk-name [prefix lvl]
  (case lvl
    0 (symbol prefix)
    1 (symbol (str prefix "'"))
    2 (symbol (str prefix "''"))
    (symbol (str prefix "-" lvl))))

(defn gen-fresh [name lvl forbidden]
  (let [sname (mk-name name lvl)]
    (if (contains? forbidden sname)
      (recur name (inc lvl) forbidden)
      sname)))

(defn gen-free-name
  "Generate a symbol, prefixed by `name`,
  and guaranteed fresh wrt. the set of names `forbidden`
  (plus the reserved symbols)."
  [name forbidden]
  (gen-fresh name 0 forbidden))

(def parse-list (atom #{}))

(defn find-parser [term]
  (loop [parsers @parse-list]
    (if (seq parsers)
      (let [[reco? parser] (first parsers)]
        (if (reco? term)
          parser
          (recur (rest parsers))))
      ;; not found
      nil)))

(declare application? parse-application
         variable? parse-variable
         hole? parse-hole)

(defn parse-core
  "Parse a term and create an AST"
  ([term] (parse-core '() term))
  ([benv term]
   (if-let [parser (find-parser term)]
     (parser parse-core benv term)
     ;; default for sequence is application
     (cond
       (hole? term) (parse-hole parse-core benv term)
       (variable? term) (parse-variable parse-core benv term)
       (application? term) (parse-application parse-core benv term)
       (keyword? term) term ;; keyword are auto-parsed
       :else [:ko "Cannot parse term." {:term term}]))))

(defn add-parse-pattern
  [& patterns]
  (loop [[pattern & patterns] patterns]
    (when pattern
      (let [[pattern? parse-pattern] pattern]
        (swap! parse-list #(conj % [pattern? parse-pattern]))
        (recur patterns)))))


(defn deputy? [t]
  (:deputy (meta t)))

(add-parse-pattern [deputy? (fn [parse benv term] term)])

;;;; **************** Macro-fu ******************

(def clj? (u/starts-with 'clj))

(def parsed? (u/starts-with 'deputy.syntax/parse-core))

(defn expand-and-quote [term]
  (cond
    (clj? term) (second term)
    (vector? term) (list* 'vector (apply vector (map expand-and-quote term)))
    (symbol? term) (let [v (resolve term)
                         m (if v (meta v) nil)]
                     (if (and v m (:deputy m)
                              (= (u/node-type @v) :certified))
                       (list 'quote (u/qualified-symbol-from-var v))
                       (list 'quote term)))
    (seq? term) (let [expanded-term (w/macroexpand-all term)]
                  (cond
                    (and (seq? expanded-term) (parsed? expanded-term)) (second expanded-term)
                    :else (cons 'list (map expand-and-quote expanded-term))))
    :else term))

(defmacro defparse [extsym intsym]
  `(defmacro ^{:deputy true} ~extsym [& rest#]
     (list 'parse-core (list* 'list '~intsym (map expand-and-quote rest#)))))

(defmacro parse [term]
  (list `parse-core (expand-and-quote term)))


;;;; ****************** Holes ********************

(defn hole? [t]
  (cond 
    (symbol? t)
    (let [id (name t)]
      (and (>= (count id) 2)
           (= (first id) \?)))
    (sequential? t)
    (and (>= (count t) 1)
         (hole? (first t)))
    :else
    false))

(defn parse-hole
  [_ _ term]
  (if *holes-enabled*
    (cond 
      (symbol? term) (a/mk-hole term [])
      (sequential? term)
      (let [[hole-name & ps] term
            params (vec ps)]
        (ok> (loop [params params]
               (if (seq params)
                 (if-not (symbol? (first params))
                   [:ko "Hole parameter is not a symbol." {:hole hole-name
                                                           :param (first params)}]
                   (recur (rest params)))
                 :ok))
             (a/mk-hole hole-name params)))
      :else
      (throw (ex-info "Unexpected hole term (please report)" {:term term})))
    ;; else, holes are not enabled
    [:ko "Holes not enabled" {:hole term}]))

;;;; **************** Constants ******************

(defn parse-const
  [_ _ term]
  (a/mk-const term))

(add-parse-pattern [(u/is? :type) parse-const]
                   [(u/is? :unit) parse-const]
                   [nil? parse-const])

;;;; **************** Abstractions ******************

(defparse λ ::lambda)
(defparse lambda ::lambda)
(defparse fun ::lambda)

(defn unfold-lambdas
  [params body]
  (if (seq params)
    (list ::lambda [(first params)] (unfold-lambdas (rest params) body))
    body))

(defn parse-lambda-content [t]
  (ok> (when (not= (count t) 3)
         [:ko> "Wrong arity for lambda-abstraction, expecting 3."
          {:term t
           :arity (count t)}])
       (second t) :as params
       (when (not (and (vector? params)
                       (pos? (count params))
                       (every? symbol? params)))
         [:ko> "Lambda parameters should be a non-empty vector of variable identifiers."
          {:params params}])
       (when (not (= (count (into #{} params)) (count params)))
         [:ko> "Lambda parameters should be all distinct." {:params params}])
       [:ok params (nth t 2)]))

(defn parse-lambda
  [parse benv term]
  (ok> (parse-lambda-content term) :as [_ params body]
       (if (> (count params) 1)
         (unfold-lambdas (rest params) body)
         body) :as chained-lambdas
       (parse (cons (first params) benv) chained-lambdas) :as parsed-chained-lambdas
       (a/mk-lam (a/mk-bind (first params) parsed-chained-lambdas))))

(add-parse-pattern [(u/starts-with ::lambda) parse-lambda])

;;;; **************** Products  ******************

(defmacro =>
  ([a b] `(Π [~'_ ~a] ~b))
  ([a b & rest] (list 'deputy.syntax/=> a (cons 'deputy.syntax/=> (cons b rest)))))

(defparse pi ::pi)
(defparse Π ::pi)

(defn parse-pi-content [t]
  (ok> (when (not= (count t) 3)
         [:ko> "Wrong arity for Pi-type, expecting 3." {:term t
                                                        :arity (count t)}])
       (second t) :as binding
       (when (not (and (vector? binding)
                       (>= (count binding) 2)))
         [:ko> "Malformed Pi-type binding: missing binding elements." {:binding binding}])
       (butlast binding) :as vars
       (last binding) :as typ
       (when (not (every? symbol? vars))
         [:ko> "Pi-type variables should be identifiers." {:vars vars}])
       (when (not (= (count (into #{} vars)) (count vars)))
         [:ko> "Pi-type variables should be all distinct." {:vars vars}])
       [:ok {:vars vars
             :dom typ
             :cod (nth t 2)}]))

(defn unfold-pis
  [vars dom cod]
  (if (seq vars)
    (list ::pi [(first vars) dom] (unfold-pis (rest vars) dom cod))
    cod))

(defn parse-pi
  [parse benv term]
  (ok> (parse-pi-content term) :as [_ p]
       (let [cod (if (> (count (:vars p)) 1)
                   (unfold-pis (rest (:vars p)) (:dom p) (:cod p))
                   (:cod p))]
         (ok> (parse benv (:dom p)) :as dom
              (parse (cons (first (:vars p)) benv) cod) :as cod
              [(first (:vars p)) dom cod])) :as [name dom cod]
       (a/mk-pi dom (a/mk-bind name cod))))

(add-parse-pattern [(u/starts-with ::pi) parse-pi])

;;;; **************** Annotations  ******************

(defparse the ::annot)

(defn parse-annotation
  [parse benv term]
  (ok> (when-not (= (count term) 3)
         [:ko> "Annotation needs exactly two arguments." {:term term}])
       term :as [_ typ e]
       (parse benv typ) :as typ
       (parse benv e) :as e
       (a/mk-annot typ e)))

(add-parse-pattern [(u/starts-with ::annot) parse-annotation])

;;;; **************** Applications  ******************

(defn application? [t]
  (and (sequential? t)
       (not (vector? t))
       (>= (count t) 2)))

(defparse app ::application)

(defn front&last [s]
  (loop [s s, front []]
    (if (seq s)
      (if (seq (rest s))
        (recur (rest s) (conj front (first s)))
        [front (first s)])
      nil)))

(examples
 (front&last '(a b c d)) => '[[a b c] d]
 (front&last '(a b c)) => '[[a b] c]
 (front&last '(a b)) => '[[a] b]
 (front&last '(a)) => '[[] a]
 (front&last '()) => nil)

(defn stack-app [args]
  (if (<= (count args) 2)
    args
    (let [[front lst] (front&last args)]
      (list (stack-app (seq front)) lst))))

(examples
 (stack-app '(a b c d)) => '(((a b) c) d)
 (stack-app '(a b c)) => '((a b) c)
 (stack-app '(a b)) => '(a b)
 (stack-app '(a)) => '(a)
 (stack-app '()) => '())

(defn parse-application
  [parse benv term]
  (if (= (count term) 2)
    (ok> (parse benv (first term)) :as rator
         (parse benv (second term)) :as rand
         (a/mk-app rator rand))
    ;; more than 2 arguments
    (parse-application parse benv (stack-app term))))

(add-parse-pattern [(u/starts-with ::application) #(parse-application %1 %2 (rest %3))])

;;;; **************** Variables  ******************

(defparse dvar ::var)

(defn variable?
  [t]
  (symbol? t))

(defn search-boundvar
  [benv x]
  (loop [benv benv, lvl 0]
    (if (seq benv)
      (if (= (first benv) x)
        lvl
        (recur (rest benv) (inc lvl)))
      nil)))

(examples
 (search-boundvar '(x y z) 'x) => 0
 (search-boundvar '(x y z) 'z) => 2
 (search-boundvar '(x y z) 't) => nil)


(defn parse-variable
  [_ benv term]
  ;;(println "parse variable:" term)
  ;;(println "  ==> resolve=" (resolve term))
  (if-let [lvl (search-boundvar benv term)]
    (a/mk-bound term lvl)
    (if-let [ref (if *resolve-identifiers*
                   (resolve term)
                   nil)]
      (let [m (meta ref)]
        (if (get m :deputy)
          (a/mk-ref ref)
          [:ko "This does not reference a deputy term." {:term term, :ref ref}]))
      ;; otherwise just create a free occurrence of a variable (if permitted)
      (if *allow-free-variables*
        (do ;;(println "var=" term)
            (a/mk-free term))
        [:ko "Unbound symbol" {:symbol term}]))))




(add-parse-pattern [(u/starts-with ::var) #(parse-variable %1 %2 (second %3))])

;;;; **************** Pairs  ******************

(defn pair? [t]
  (and (vector? t)
       (>= (count t) 2)))

(defparse pair ::pair)

(defn unfold-pair [p]
  (if (> (count p) 2)
    [(first p) (unfold-pair (subvec p 1))]
    p))

(example
 (unfold-pair '[A B C D]) => '[A [B [C D]]])

(defn parse-pair
  [parse benv term]
  (ok> (if (> (count term) 2)
         (unfold-pair term)
         term) :as [A B]
       (parse benv A) :as fst
       [:ko> "Cannot parse term of pair" {:pair term :term A}]
       (parse benv B) :as snd
       (a/mk-pair fst snd)))

(add-parse-pattern [pair? parse-pair]
                   [(u/starts-with ::pair) #(parse-pair %1 %2 (into [] (rest %3)))])

;;;; **************** Sigmas (pair types) ******************

(defparse Σ ::sigma)

(defparse sig ::sigma)

(defn unfold-pair-type [p]
  (if (> (count p) 2)
    (list ::sigma (first p) (unfold-pair-type (rest p)))
    (list* ::sigma p)))

(defn parse-binding [t]
  (if (not (vector? t))
    ['_ t]
    (ok> (when (not= (count t) 2)
           [:ko> "Wrong binding: expecting `[var type]` form." {:term t}])
         (when-not (symbol? (first t))
           [:ko> "Binding variable must be an idenfier." {:binding t}])
         t)))

(defn parse-sigma
  [parse benv term]
  (ok> (if (> (count term) 3)
         (unfold-pair-type (rest term))
         term) :as [_ A B]
       (parse-binding A) :as [v A]
       (parse benv A) :as fst
       [:ko> "Cannot parse first element of pair type" {:pair-type term, :term A}]
       (parse (cons v benv) B) :as scd
       [:ko> "Cannot parse second element of pair type" {:pair-type term, :term B}]
       (a/mk-sig fst (a/mk-bind v scd))))

(add-parse-pattern [(u/starts-with ::sigma) parse-sigma])

;;;; **************** Projections ******************

(defparse p1 ::proj1)
(defparse π1 ::proj1)

(defparse p2 ::proj2)
(defparse π2 ::proj2)

(defn parse-proj-left
  [parse benv term]
  (ok> (when-not (= (count term) 2)
         [:ko> "Wrong left projection: expecting 1 argument" {:term term}])
       (parse benv (second term)) :as arg
       (a/mk-proj :left arg)))

(defn parse-proj-right
  [parse benv term]
  (ok> (when-not (= (count term) 2)
         [:ko> "Wrong right projection: expecting 1 argument" {:term term}])
       (parse benv (second term)) :as arg
       (a/mk-proj :right arg)))

(add-parse-pattern [(u/starts-with ::proj1) parse-proj-left]
                   [(u/starts-with ::proj2) parse-proj-right])


;; ;;;; **************** clj-eval hook ******************

;; (defparse clj ::clj)

;; (defn parse-clj
;;   [parse benv term]
;;   (ok> (when-not (= (count term) 2)
;;          [:ko> "Wrong clj form: expecting 1 argument" {:term term}])
;;        (let [res (eval (second term))]
;;          (parse benv res))))

;; (add-parse-pattern [(u/starts-with ::clj) parse-clj])

;; (example
;;  (fun [x] (clj (if false 42 'x)))
;;  => '{:node :lambda, :name x, :body {:node :bound-var, :name x, :level 0}})


;; ********************************************************************************
;; ************************** ANNOTATIONS / CONSTANTS *****************************
;; ********************************************************************************


(examples
 (a/mk-annot (a/mk-const :unit) (a/mk-const nil))
 => '{:node :annotation, :type :unit, :term nil}

 (the :unit nil)
 => '{:node :annotation, :type :unit, :term nil})


;; ********************************************************************************
;; **************** LAMBDA / APPLICATION / BOUND VAR / FREE VAR *******************
;; ********************************************************************************


(examples
 (parse (lambda [x y z] x))
 => '{:node :lambda
      :name x
      :body
      {:node :lambda
       :name y
       :body
       {:node :lambda
        :name z
        :body
        {:node :bound-var
         :name x
         :level 2}}}}

 (parse (lambda [x y z] (x y z)))
 => '{:node :lambda
      :name x
      :body {:node :lambda
             :name y
             :body {:node :lambda
                    :name z
                    :body {:node :application
                           :rator {:node :application
                                   :rator {:node :bound-var
                                           :name x
                                           :level 2}
                                   :rand {:node :bound-var
                                          :name y
                                          :level 1}}
                           :rand {:node :bound-var
                                  :name z
                                  :level 0}}}}}

 (parse (lambda [x] t))
 => '{:node :lambda
      :name x
      :body {:node :free-var
             :name t}}
 (parse ((fun [x] x) y))
 => '{:node :application
      :rator {:node :lambda
              :name x
              :body {:node :bound-var
                     :name x
                     :level 0}}
      :rand {:node :free-var
             :name y}})


;; ********************************************************************************
;; ********************************* PI / IF **************************************
;; ********************************************************************************

(examples
 (parse (Π [x :unit] :unit))
 => '{:node :pi, :name x, :domain :unit, :codomain :unit}


 (parse (Π [x :type] x))
 => '{:node :pi, :name x, :domain :type, :codomain {:node :bound-var, :name x, :level 0}})

;; ********************************************************************************
;; ***************************** SIG / PAIR / PROJ ********************************
;; ********************************************************************************


(examples
 (parse (sig [a A] [b a] C a))
 => '{:node :sig, :name a, :first {:node :free-var, :name A}, :second {:node :sig, :name b, :first {:node :bound-var, :name a, :level 0}, :second {:node :sig, :name _, :first {:node :free-var, :name C}, :second {:node :bound-var, :name a, :level 2}}}}

 (parse [A B C D])
 => '{:node :pair, :first {:node :free-var, :name A}, :second {:node :pair, :first {:node :free-var, :name B}, :second {:node :pair, :first {:node :free-var, :name C}, :second {:node :free-var, :name D}}}}

 (parse (π1 (π2 (π2 [A B C D]))))
 => '{:node :proj, :take :left, :pair {:node :proj, :take :right, :pair {:node :proj, :take :right, :pair {:node :pair, :first {:node :free-var, :name A}, :second {:node :pair, :first {:node :free-var, :name B}, :second {:node :pair, :first {:node :free-var, :name C}, :second {:node :free-var, :name D}}}}}}})


;; ********************************************************************************
;; *********************************** UNPARSE ************************************
;; ********************************************************************************


(examples
 (a/unparse (parse (λ [x] (x y))))
 => '(λ [x] (x y))

 (a/unparse (parse (fun [x y z] (x y z))))
 => '(λ [x] (λ [y] (λ [z] ((x y) z))))

 (a/unparse (parse (sig [a A] [b a] C a)))
 => '(Σ [a A] (Σ [b a] (Σ [_ C] a)))

 ;; FIXME? we used to support this, before the advent of macro-based AST
 ;; (a/unparse (parse '(Π x x y)))
 ;; => '(Π [_ x] (Π [_ x] y))

 (a/unparse (parse (π1 (π2 (π2 (π2 [A B C D]))))))
 => '(π1 (π2 (π2 (π2 [A [B [C D]]])))))


;; ********************************************************************************
;; *********************************** GET FREE VARS ******************************
;; ********************************************************************************


(examples
 (a/freevars (parse (λ [x] y)))
 => '#{y})

(examples
 (a/freevars (parse (λ [x] (x y)))) => '#{y}
 (a/freevars (parse (Π [_ x] (Π [_ x] y)))) => '#{x y}
 (a/freevars (parse [x y])) => '#{x y})


;; ********************************************************************************
;; *********************************** LOCALLY CLOSED *****************************
;; ********************************************************************************


(examples

 (a/locally-closed? (parse (fun [x] (x y)))) => true
 (a/locally-closed? (parse (fun [x] (x y)))) => true
 (a/locally-closed? (parse (fun [x] x))) => true
 (a/locally-closed? (parse ((fun [x] x) y))) => true
 (a/locally-closed? (parse true)) => true
 (a/locally-closed? (parse false)) => true
 (a/locally-closed? (parse type)) => true
 (a/locally-closed? (parse bool)) => true
 (a/locally-closed? (parse [false true])) => true
 (a/locally-closed? (parse (fun [x] ((fun [z] z) y)))) => true)


;; ********************************************************************************
;; *********************************** NORMAL *************************************
;; ********************************************************************************


(examples
 (a/normal-form-neutral? (a/mk-bound 'x 0)) => true
 (a/normal-form? (parse (fun [x] (x y)))) => true
 (a/normal-form? (parse (fun [x] x))) => true
 (a/normal-form? (parse ((fun [x] x) y))) => false
 (a/normal-form? (parse true)) => true
 (a/normal-form? (parse false)) => true
 (a/normal-form? (parse :type)) => true
 (a/normal-form? (parse :bool)) => true
 (a/normal-form? (parse [false true])) => true
 (a/normal-form-neutral? (parse (fun [z] z))) => false
 (a/normal-form-canonical? (parse ((fun [z] z) y))) => false
 (a/normal-form? (parse (fun [x] (app (fun [z] z) y)))) => false)


;; ********************************************************************************
;; *********************************** SUBSTITUTION *******************************
;; ********************************************************************************


(examples
 (a/unparse :index (a/subst (parse (fun [z] (z d))) (:body (parse (fun [x] (fun [y] (x y c)))))))
 => '(λ (((λ (#{0} d)) #{0}) c))


 (a/unparse :index (a/subst (parse nil) (:body (parse (fun [x] (π1 (Σ x x)))))))
 => '(π1 (Σ nil nil)))



;; ********************************************************************************
;; *********************************** BINDS **************************************
;; ********************************************************************************


(examples
 (a/unparse :index (a/bind 'x12 (parse (fun [x] (π1 (Σ [y x12]  y))))))
 => '(λ (π1 (Σ #{1} #{0})))

 (a/unparse :index (a/bind 'x (parse (fun [y] (π1 (Σ [z x] z))))))
 => '(λ (π1 (Σ #{1} #{0}))))


