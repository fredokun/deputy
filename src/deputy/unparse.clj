(ns deputy.unparse
  "Unparsing utilities,
  especially unparse patterns management.")

(defn- variable?
  "The argument `t` is a term variable if
  it is a symbol whose name starts with a question mark."
  [t]
  (and (symbol? t)
       (= (first (name t)) \?)))

(defn vec-term?
  "The argument `t` is a *vector term* if
  it is a Clojure vector."
  [t] (vector? t))

(defn seq-term?
  "The argument `t` is a *sequential term* if
  it is `sequential?` for Clojure (and not a vector)."
  [t] (sequential? t))

(defn assoc-term?
  "The argument `t` is an *associative term* if
  it is `associative?`for Clojure."
  [t] (associative? t))

(defn constant?
  "The argument `t` is a constant if it is non-`nil`,
 and it is not a variable, a sequential or an associative term."
  [t]
  (and (not (nil? t))
       (not (variable? t))
       (not (vec-term? t))
       (not (assoc-term? t))
       (not (seq-term? t))))

(defn match
  "Matches pattern `p` against term `t`.
  Returns a substitution, a map from
  variables to matched subterms, or `nil` if
  the matching fails."
  {:todo "Write a tail-recursive version (use a zipper ?)."}
  ([p t] (match p t {}))
  ([p t s]
   (if (nil? s)
     s
     (cond
       (variable? p) (if-let [u (s p)]
                       (when (= t u)
                         s)
                       (assoc s p t))
       (vec-term? p) (if (vec-term? t)
                       (if-let [p' (seq p)]
                         (when-let [t' (seq t)]
                           (recur p' t' s))
                         (if (seq t)
                           nil
                           s)))
       (seq-term? p) (when (seq-term? t)
                       (if (seq p)
                         (when (seq t)
                           (recur (rest p) (rest t) (match (first p) (first t) s)))
                         (if (seq t)
                           nil
                           s)))
       (assoc-term? p) (when (assoc-term? t)
                         (if (seq p)
                           (when (seq t)
                             (let [[kp tp] (first p)]
                               (when-let [tt (get t kp)]
                                 (recur (dissoc p kp) (dissoc t kp) (match tp tt s)))))
                           (if (seq t)
                             nil
                             s)))
       :else (when (= p t)
               s)))))


(def +unparse-patterns+ (atom {}))

(defn register-unparse-pattern!
  ([id unparse-fn]
   (register-unparse-pattern! id ::any unparse-fn))
  ([id patt unparse-fn]
   (swap! +unparse-patterns+ 
          #(assoc % id {:pattern patt, :unparse-fn unparse-fn}))))

(defn apply-unparse-patterns [id term]
  (if-let [{patt :pattern, unparse-fn :unparse-fn} (get @+unparse-patterns+ id)]
    (if (= patt ::any)
      (unparse-fn term)
      (if-let [subst (match patt term)]
        (unparse-fn subst term)
        ;; the pattern does not match
        id))
    ;; no parse pattern found
    id))
